//
//  User.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Rider.h"
#import "userDailyStat.h"


@interface User : Rider

@property (nonatomic, retain) NSNumber * fbId;
@property (nonatomic, retain) NSString * first;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * last;
@end

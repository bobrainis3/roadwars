//
//  RWProfileTableViewController.h
//  roadwars
//
//  Created by Christian Allen on 11/16/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWTableViewController.h"

@class User;
@class FBProfilePictureView;
@class DBFBProfilePictureView;

@interface RWProfileTableViewController : RWTableViewController

@property (nonatomic, weak) IBOutlet DBFBProfilePictureView *profilePicture;
@property (nonatomic, weak) IBOutlet UIView *profilePictureView;
@property (nonatomic, weak) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet UILabel *coinTotal;
@property (nonatomic, weak) IBOutlet UILabel *flagTotal;
@property (nonatomic, weak) IBOutlet UILabel *attackLabel;
@property (nonatomic, weak) IBOutlet UILabel *attackButton;
@property (nonatomic, weak) IBOutlet UIView *attackAnywhereView;
@property (nonatomic, weak) IBOutlet UILabel *droveTodayLabel;
@property (nonatomic, weak) IBOutlet UILabel *droveWeekLabel;
@property (nonatomic, weak) IBOutlet UILabel *attacksTodayLabel;
@property (nonatomic, weak) IBOutlet UILabel *attacksWeekLabel;
@property (nonatomic, weak) IBOutlet UILabel *attacksPercentWonLabel;
@property (nonatomic, weak) IBOutlet UILabel *attacksWeeklyPercentWonLabel;
@property (nonatomic, weak) IBOutlet UILabel *todayCoinEarnedLabel;
@property (nonatomic, weak) IBOutlet UILabel *todayRoadsCapturedLabel;

@property (nonatomic, strong) User *user;
@property (nonatomic, assign) BOOL bTopDogIsMe;
@property (nonatomic, assign) int indexPathForAttack;
@property (nonatomic, assign) float sortAlpha;
@property (nonatomic, weak) IBOutlet UIButton *turfView;
@property (nonatomic, weak) IBOutlet UIButton *skinnyView;
@property (nonatomic, weak) IBOutlet UIImageView *emptyImageView;



- (IBAction)getTurf:(id)sender;
- (IBAction)getSkinny:(id)sender;


@end

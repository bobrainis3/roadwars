//
//  UIImage+Decompress.h
//  MWPhotoBrowser
//

#import <Foundation/Foundation.h>

@interface UIImage (Decompress)
- (void)decompress;
@end

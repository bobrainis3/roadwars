//
//  RWSansBoldLabel.m
//  RW
//
//  Created by Christian Allen on 9/18/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWSansBoldLabel.h"
#import "Constants.h"

@implementation RWSansBoldLabel

- (void)awakeFromNib {  // CA: simply allows us to use the PT Sans Bold font in interface builder
    [super awakeFromNib];
    self.font = [UIFont fontWithName:kFontNameSansBold size:self.font.pointSize];
}

@end

//
//  Town.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Area.h"

@class Country, Road, StateProvince;

@interface Town : Area

@property (nonatomic, retain) StateProvince *stateProvince;
@property (nonatomic, retain) Country *country;
@property (nonatomic, retain) NSSet *roads;
@end

@interface Town (CoreDataGeneratedAccessors)

- (void)addRoadsObject:(Road *)value;
- (void)removeRoadsObject:(Road *)value;
- (void)addRoads:(NSSet *)values;
- (void)removeRoads:(NSSet *)values;

@end

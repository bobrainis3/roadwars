//
//  RWBlackGradientView.m
//  RW
//
//  Created by bob rainis
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWBlackGradientView.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation RWBlackGradientView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CALayer *layer = self.layer;
    layer.cornerRadius = 9.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 2.0f;
    layer.borderColor = [UIColor blackColor].CGColor;
    
    CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.layer.bounds;
    _gradientLayer.colors = [NSArray arrayWithObjects:
                             (id)kColorNighttimeViewTop.CGColor,
                             (id)kColorNighttimeViewMiddle.CGColor,
                             (id)kColorNighttimeViewBottom.CGColor,
                             nil];
    _gradientLayer.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:0.5f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
    [self.layer insertSublayer:_gradientLayer atIndex:0];
    
   
}

@end

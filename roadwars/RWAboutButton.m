//
//  RWAboutButton.h.m
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//

#import "RWAboutButton.h"
#import "Constants.h"

@implementation RWAboutButton

- (void)awakeFromNib {
    // Initialization code
    
    self.contentMode = UIViewContentModeCenter;
    
    [self setTitleColor:kColorGold forState:UIControlStateNormal];
    [[self layer] setBorderWidth:2.0f];
    [[self layer] setBorderColor:kColorGrungeButtonTopBorder.CGColor];
    [self layer].cornerRadius = 8.0f;
    [self layer].masksToBounds = YES;
    [self layer].borderWidth = 1.0f;
    self.titleLabel.font = [UIFont fontWithName:kFontNameGrunge size:20];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)kColorGrungeButtonTopGradient.CGColor, (id)kColorGrungeButtonBottomGradient.CGColor, nil];
    [self.layer insertSublayer:gradient atIndex:0];
    
    
}

@end

//
//  RWShinyButton.h
//  RW
//
//  Created by Christian Allen on 9/26/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWShinyButton : UIButton

@end

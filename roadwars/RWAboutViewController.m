//
//  RWAboutViewController
//  roadwars
//
//  Created by Bob Rainis 1/07/13.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWAboutViewController.h"
#import "Constants.h"
#import "MWPhotoBrowser.h"

#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVSpeechSynthesis.h>

@interface RWAboutViewController ()

@property (nonatomic, retain) AVSpeechSynthesizer *r;

@end

@implementation RWAboutViewController
@synthesize versionLabel=_versionLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *compileDate = [NSString stringWithUTF8String:__DATE__];
    NSString *version = NSLocalizedString(@"V", nil);
    self.versionLabel.text=[NSString stringWithFormat:@"Road Wars %@%@,  %@",version,[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], compileDate];
    
    NSLog(@"%@",[AVSpeechSynthesisVoice speechVoices]);
    NSLog(@"%f %f %f",AVSpeechUtteranceMaximumSpeechRate,AVSpeechUtteranceMinimumSpeechRate,AVSpeechUtteranceDefaultSpeechRate);
    self.r = [[AVSpeechSynthesizer alloc] init];
    self.r.delegate = self;
    AVSpeechUtterance *utt = [AVSpeechUtterance speechUtteranceWithString:@"Drive your car and Road Wars does all the work to capture roads. Earn coin by safe driving and no phone usage. Attack friends when not driving, so you can take over their territory."];
    utt.rate = 0.2;
    [self.r speakUtterance:utt];
  
}


- (IBAction)doFacebook:(id)sender{
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSString *url = [serverParameters objectForKey:@"facebookUrl"];
    //NSLog(@"do facebook <%@>", url);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    
}

- (IBAction)doTwitter:(id)sender{
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSString *url = [serverParameters objectForKey:@"twitterUrl"];

    //NSLog(@"do twitter <%@>", url);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    
}

#pragma mark
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%s",__func__);
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%s",__func__);
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didPauseSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%s",__func__);
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didContinueSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%s",__func__);
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didCancelSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%s",__func__);
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer willSpeakRangeOfSpeechString:(NSRange)characterRange utterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%s",__func__);
}


@end

//
//  CurrentUser.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "CurrentUser.h"


@implementation CurrentUser

@dynamic bonusTime;
@dynamic attackLevel;
@dynamic attackRandomizedDiceOrder;
@dynamic numBonusRecieved;
@end

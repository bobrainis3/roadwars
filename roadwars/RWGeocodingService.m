//
//  RWGeocodingService.m
//  roadwars
//
//  Created by Bob Rainis on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWGeocodingService.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "Reachability.h"
#import "AFNetworking.h"

@implementation RWGeocodingService
@synthesize geoCoder=_geoCoder;
@synthesize mqReach=_mqReach;

BOOL bMapquestRequestDone;
BOOL bReverseGeoDone;

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)

- (id) init {
	self = [super init];
	if (self != nil) {
      	// reverse geocoder
        self.geoCoder = [[CLGeocoder alloc] init];
        [self addObserver:kReachabilityChangedNotification withSelector:@selector(onNetworkChange:)];
        self.mqReach = [Reachability reachabilityForInternetConnection];
        [self.mqReach startNotifier];
        bMapquestRequestDone=bReverseGeoDone=YES;
        
	}
	return self;
}

- (void)dealloc {
    [self removeObserver:kReachabilityChangedNotification];
	self.geoCoder=nil;
    
}

// Reverse geocode our location
// send off request to MQ for speedlimit, and toolkit reverse geocoder for
// state, town and postal code. 
-(void) reverseGeocode: (CLLocation *)updatedLocation{
    
    if (!self.mqReach.isReachable){
        NSLog (@"Not Reachable <%@>", updatedLocation);
        return;
    }
    
    /*
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSString *url = [serverParameters objectForKey:@"mapquestUrl"];
    NSLog(@"mapQuestUrl <%@>%d", url,url.length);
   
    NSString *params;
    if (url.length>0)
        params = [NSString stringWithFormat:@"%@&unit=m&from=%5.8f,%5.8f&to=%5.8f,%5.8f",url, (updatedLocation.coordinate.latitude),(updatedLocation.coordinate.longitude),(updatedLocation.coordinate.latitude), (updatedLocation.coordinate.longitude )];
    else
        params = [NSString stringWithFormat:@"%@&unit=m&from=%5.8f,%5.8f&to=%5.8f,%5.8f",kMapQuestPath, (updatedLocation.coordinate.latitude),(updatedLocation.coordinate.longitude),(updatedLocation.coordinate.latitude), (updatedLocation.coordinate.longitude )];
    
    
   //NSLog (@"%@", s);
    
    // check MQ for data, use speed limit from MQ
    dispatch_async(kBgQueue, ^{
        NSURL *url = [NSURL URLWithString:params];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
           // NSLog(@"MQ response is <%@>", JSON);
            [self fetchedMQData:JSON];
         } failure:nil];
        
        [operation start];
        
    });
    */
    dispatch_async(kBgQueue, ^{
        // toolkit reverse geo to get town, state and zip
        [self.geoCoder reverseGeocodeLocation:updatedLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            //NSLog (@"Placemarks %@ error <%@>", placemarks, error);
            if ([placemarks count]){
                //CLPlacemark *placemark = [placemarks objectAtIndex:0];
                //NSLog (@"PlaceMark is %@", [placemarks objectAtIndex:0]);
                //NSLog(@"Placemark array: %@",placemark.addressDictionary );
                
                [self postNotification:kOnLocationUpdate withObject:[placemarks objectAtIndex:0]];
                [self postNotification:kOnSpeedlimitUpdate withObject:nil];

            }
        }];
    });
    
}

#pragma mark -  speedlimits and road name from MQ
- (void)fetchedMQData:(NSDictionary *)json {
 
    if (json == nil) {
        //NSLog(@"Response from MQ is nil");
        // TODO: should we do something more here?
        //return;
    } else{
        
       // NSLog(@"Good results from MQ");
        
    }
    //NSLog (@"MQ speed limit info -- %@", json);
    [self postNotification:kOnSpeedlimitUpdate withObject:json];
    
}

-(void)onNetworkChange:(NSNotification *)notif {
    NetworkStatus remoteHostStatus;
    if ( self.mqReach == notif.object ) {
        remoteHostStatus = [self.mqReach currentReachabilityStatus];
        NSLog(@"RWGeocoding:onNetworkChange - network is %@", ((remoteHostStatus == NotReachable) ? @"suspended" : @"resumed"));
        
    }
    else{
        Reachability *reach = (Reachability*)notif.object;
        remoteHostStatus = [reach currentReachabilityStatus];
        
        NSLog(@"RWGeocoding:onNetworkChange but not MQ - network is %@", ((remoteHostStatus == NotReachable) ? @"suspended" : @"resumed"));
    }
     
    [self postNotification:(remoteHostStatus == NotReachable)?kOnInternetNotReachable:kOnInternetReachable withObject:nil];

}

@end

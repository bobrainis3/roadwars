//
//  RWMapAnnotations.m
//  roadwars
//
//  Created by Bob Rainis on 7/1/13.
//  Copyright (c) 2013 BLADE LLC. All rights reserved.
//

#import "RWMapAnnotations.h"


@implementation MapAnnotations
@synthesize coordinate;
@synthesize tag;
@synthesize title;
@synthesize subtitle;


-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTag:(NSUInteger)t withTitle:(NSString *)tl withSubtitle:	(NSString *)s
{
	if(self = [super init])
	{
		coordinate = c;
		tag = t;
		title = tl;
		subtitle = s;
	}
	return self;
    
}



@end

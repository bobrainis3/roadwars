//
//  RWSkinnyViewController.h
//  roadwars
//
//  Created by Bob Rainis.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTableViewController.h"
#import "User.h"


@interface RWSkinnyViewController : RWTableViewController

- (IBAction)doDeleteSkinny:(id)sender;
@end

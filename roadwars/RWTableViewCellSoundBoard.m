//
//  RWTableViewCellSoundBoard.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWTableViewCellSoundBoard.h"

@implementation RWTableViewCellSoundBoard

@synthesize muteButton = _muteButton;
@synthesize aButton = _aButton;
@synthesize bButton = _bButton;
@synthesize cButton = _cButton;

@end

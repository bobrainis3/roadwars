//
//  RWServices.h
//  roadwars
//
//  Created by Bob Rainis on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RWLocationService;
@class RWGeocodingService;
@class RWAccelerometerService;
@class RWBonusTimerService;
@class RWFacebookService;
@class RWAPIOperationQueue;
@class RWAudioService;
@class RWDrivingTimerService;

@interface RWServices : NSObject

@property (nonatomic, strong) RWLocationService *locationService;
@property (nonatomic, strong) RWGeocodingService *geocodingService;
@property (nonatomic, strong) RWAccelerometerService *accelerometerService;
@property (nonatomic, strong) RWBonusTimerService *bonusTimerService;
@property (nonatomic, strong) RWDrivingTimerService *drivingTimerService;
@property (nonatomic, strong) RWFacebookService *facebookService;
@property (nonatomic, strong) RWAPIOperationQueue *apiQueue;
@property (nonatomic, strong) RWAudioService *audioService;


// convenience and access
+ (RWServices *)sharedServices;
+ (RWLocationService *)locationService;
+ (void)startMotionDetection;
+ (RWGeocodingService *)geocodingService;
+ (RWAccelerometerService *)accelerometerService;
+ (RWBonusTimerService *)bonusTimerService;
+ (RWDrivingTimerService *)drivingTimerService;
+ (RWFacebookService *)facebookService;
+ (RWAPIOperationQueue *)apiQueue;
+ (RWAudioService *)audioService;

+ (void)startLocationService;
+ (void)stopLocationService;
+ (void)startAccelerometerService;
+ (void)stopAccelerometerService;
+ (void)startBonusTimerService;
+ (void)stopBonusTimerService;
+ (void)storeObject:(NSObject *)root withKey:(NSString *)key;
+ (NSObject *)loadObjectOfClass:(Class)aClass fromKey:(NSString *)key withDefault:(NSObject *)defaultObj;

@end

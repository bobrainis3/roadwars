//
//  userDailyStat.m
//  roadwars
//
//  Created by Bob Rainis on 4/22/13.
//  Copyright (c) 2013 BLADE LLC. All rights reserved.
//

#import "userDailyStat.h"
#import "User.h"


@implementation userDailyStat

@dynamic coinUpDriving;
@dynamic coinDownDriving;
@dynamic coinUpTolls;
@dynamic coinDownTolls;
@dynamic roadsUpDriving;
@dynamic roadsDownDriving;
@dynamic roadsUpAttack;
@dynamic roadsDownAttack;
@dynamic driveTime;
@dynamic totalAttack;
@dynamic user;

@end

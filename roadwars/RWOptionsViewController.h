//
//  RWOptionsViewController.h
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWViewController.h"

@interface RWOptionsViewController : RWViewController

@property (nonatomic, weak) IBOutlet UISwitch *optionAudio;

- (IBAction)doOptionAudio:(id)sender;

@end

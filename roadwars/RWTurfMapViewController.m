//
//  RWTurfMapViewController.b
//  roadwars
//
//  Created by Bob Rainis
//

#import "RWTurfMapViewController.h"
#import <MapKit/MapKit.h>
#import "RWMapAnnotations.h"
#import "RWContext.h"
#import "Constants.h"
#import "CurrentUser.h"

@interface RWTurfMapViewController ()

@end

@implementation RWTurfMapViewController
@synthesize mapView=_mapView;
@synthesize turfAnnotations=_turfAnnotations;
@synthesize allTurf=_allTurf;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
 
    
    int i=1;
    self.turfAnnotations = [[NSMutableArray alloc] init];
    
    for (NSDictionary *turf in self.allTurf){
        // NSLog (@"turf <%@>", turf);
        
        CLLocationCoordinate2D coords =
        CLLocationCoordinate2DMake([[turf objectForKey:@"lat"]doubleValue],[[turf objectForKey:@"lng"]doubleValue]);
        
        CLLocationCoordinate2D center;
        center.latitude = coords.latitude;
        center.longitude = coords.longitude;
        
        //declare span of map (height and width in degrees)
        MKCoordinateSpan span;
        span.latitudeDelta = (self.allTurf.count>1)?1:.01;
        span.longitudeDelta = (self.allTurf.count>1)?1:.01;
        
        //add center and span to a region,
        //adjust the region to fit in the mapview
        //and assign to mapview region
        MKCoordinateRegion region;
        region.center = center;
        region.span = span;
        self.mapView.region = [self.mapView regionThatFits:region];
        //create annotations and add to the turfAnnotations array
        NSUInteger tag;
        CLLocationCoordinate2D annotationCenter;
        MapAnnotations *annotation;
        
        tag = i++;
        annotationCenter.latitude = coords.latitude;
        annotationCenter.longitude =  coords.longitude;
        
        NSString *runnerUp;
        NSNumber *fbId;
        
        if (self.allTurf.count==1){
            if ([[turf objectForKey:@"runnerUp"]isKindOfClass:[NSNull class]]){
                runnerUp=@"No one else drove road";
                //NSLog(@"No one else drove road");
            }
            
            else {
                fbId = [[turf objectForKey:@"runnerUp"]objectForKey:@"fbId"];
                if (fbId!=nil){
                    User *roadOwner = (User *)[RWContext getEntityOfType:kModelUser where:[NSPredicate predicateWithFormat:@"self.fbId == %@", fbId]];
                    //NSLog (@"owner is <%@>", roadOwner.name);
                    if (roadOwner.name == nil)// not one of my friends
                        runnerUp=@"Owner of road is not a friend";
                    else{
                        NSString *friendName = [[turf objectForKey:@"runnerUp"]objectForKey:@"name"];
                        runnerUp=[NSString stringWithFormat:@"%@ is runner up", friendName];
                        //NSLog (@"%@ is runner up", friendName);
                        
                    }
                }
            }
            
            NSNumber *lifetimeTake = [turf objectForKey:@"lifetimeTake"];
            annotation = [[MapAnnotations alloc] initWithCoordinate:annotationCenter withTag:tag withTitle:[NSString stringWithFormat:@"Lifetime Take %@ Coin", lifetimeTake]  withSubtitle:runnerUp];
            
        }
        else{
            annotation = [[MapAnnotations alloc] initWithCoordinate:annotationCenter withTag:tag withTitle:[turf objectForKey:@"name"]  withSubtitle:[NSString stringWithFormat:@"%@, %@",[[turf objectForKey:@"city"]objectForKey:@"name"],[[[turf objectForKey:@"city"]objectForKey:@"stateProvince"]objectForKey:@"name"]]];
            
        }
        
          [self.turfAnnotations addObject:annotation];
        
       
        
    }
    //add annotations array to the mapView
    [self.mapView addAnnotations:self.turfAnnotations];
}

//this is the required method implementation for MKMapView annotations
- (MKAnnotationView *) mapView:(MKMapView *)thisMapView
             viewForAnnotation:(MapAnnotations *)annotation
{
	
	//the annotation view objects act like cells in a tableview.  When off screen,
	//they are added to a queue waiting to be reused.  This code mirrors that for
	//getting a table cell.  First check if the queue has available annotation views
	//of the right type, identified by the identifier string.  If nil is returned,
	//then allocate a new annotation view.
	
	static NSString *turfAnnotations = @"turfAnnotations";
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[thisMapView
                                                                  dequeueReusableAnnotationViewWithIdentifier:turfAnnotations];
	if(annotationView == nil)
		annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:turfAnnotations];
	
    annotationView.pinColor = MKPinAnnotationColorGreen;
    
    //pin drops when it first appears
    annotationView.animatesDrop=TRUE;
    
    //tapping the pin produces a gray box which shows title and subtitle
    annotationView.canShowCallout = YES;
    
    return annotationView;
}

// this shows the annotation automatically
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    id myAnnotation = [mapView.annotations objectAtIndex:0];
    NSLog (@"mapView.annotations.count %d",mapView.annotations.count);
    if (mapView.annotations.count==1)
        [mapView selectAnnotation:myAnnotation animated:YES];
}
@end

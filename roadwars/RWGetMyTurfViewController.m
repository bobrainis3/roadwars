//
//  RWGetMyTurfViewController.m
//  roadwars
//
//  Created by Bob Rainis.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWGetMyTurfViewController.h"
#import "RWTurfMapViewController.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWTableViewCell.h"
#import "RWAPI.h"
#import "UIView+UIView_.h"

@interface RWGetMyTurfViewController ()

@end

@implementation RWGetMyTurfViewController
@synthesize myTurf=_myTurf;
@synthesize user=_user;

- (void)initializeData {
    [super initializeData];
   
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(160, 240);
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [RWAPI getTurf: ^(NSDictionary * turf) {
        // NSLog (@"results from getTurf <%@>",turf);
        NSMutableArray *area = [[turf objectForKey:@"data"]objectForKey:@"turf"];
        //NSLog (@"area array %@", area);
        //NSLog(@"size %d", area.count);
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"ownerAccount"  ascending:NO];
        [area sortUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        self.myTurf = [area copy];
        [self.tableView reloadData];
        [spinner stopAnimating];
    } failure:^(NSError *error) {
        // sigh... what to do?
    }];
   
 }


- (void)dealloc {
    self.myTurf=nil;
    self.user=nil;
    self.tableView.delegate=nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
   
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog (@"self.myTurf.count <%d>", self.myTurf.count);
    return self.myTurf.count;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog (@" selected turf <%@>", [self.myTurf objectAtIndex:indexPath.row]);
    [self performSegueWithIdentifier:kSegueTurfMap sender:[self.myTurf objectAtIndex:indexPath.row]];
    

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:kSegueTurfMap] ) {
        // NSLog (@"kSegueTurfMap");
        NSDictionary *turf = (NSDictionary *)sender;
        RWTurfMapViewController *mapController = [segue destinationViewController];
        mapController.allTurf= [[NSMutableArray alloc]initWithObjects:turf, nil];
    } else if ( [[segue identifier] isEqualToString:kSegueAllTurfMap] ) {
        //  NSLog (@"kSegueAllTurfMap");
        NSMutableArray *allTurf = (NSMutableArray *)sender;
        RWTurfMapViewController *mapController = [segue destinationViewController];
        mapController.allTurf=allTurf;
    }

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RWTableViewCell *cell = nil;
    
    cell = [self dequeueOrNew:kTableCellTurf];
    [cell setBackgroundImageForRow:indexPath.row ofTotalRows:self.myTurf.count];
    
    [self configureCell:cell withObject:[self.myTurf objectAtIndex:indexPath.row]];
    
    if ( cell == nil ) NSLog(@"RWMyTurfViewController::cellForRow... - returning a null cell :(");
    return cell;
}

- (void)configureCell:(RWTableViewCell *)cell withObject:(NSObject *)obj {
    NSDictionary *turf = (NSDictionary *)obj;
   // NSLog (@"turf is <%@>", turf);
    
    cell.titleLabel.text = [turf objectForKey:@"name"];
    cell.descriptionLabel.text= [NSString stringWithFormat:@"%@, %@",[[turf objectForKey:@"city"]objectForKey:@"name"],[[[turf objectForKey:@"city"]objectForKey:@"stateProvince"]objectForKey:@"name"]];
    NSNumber *num=[turf objectForKey:@"ownerAccount"];
    cell.rightLabel.text = num.stringValue;
    num=[turf objectForKey:@"dailyTake"];
    cell.rightDescriptionLabel.text = [NSString stringWithFormat:@"%.2f", num.floatValue];
}

- (IBAction)getAllTurf:(id)sender{
    //NSLog (@"All turf");
    [self performSegueWithIdentifier:kSegueAllTurfMap sender:self.myTurf];
    
}

@end

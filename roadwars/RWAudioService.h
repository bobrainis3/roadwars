//
//  RWAudioService.m
//  roadwars
//
//  Created by Bob Rainis on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>


#define kSoundEventBigBonus @"Bonus - Big"
#define kSoundEventSmallBonus @"Bonus - Small"
#define kSoundEventPaulComfortingTic @"Bonus - Tiny"
#define kSoundEventBusted @"Speeding - Busted"
#define kSoundEventDanger @"Speeding - Danger"
#define kSoundEventSpeedLimitDown @"Speed Limit - Down"
#define kSoundEventSpeedLimitUp  @"Speed Limit - Up"

#define kSoundEventRoadCapturedFromFriend @"Road From Friend"
#define kSoundEventRoadCaptured @"Road Capture"
#define kSoundEventLoseBonusTime @"Lose Bonus Time"

#define kSoundCoinBig1 @"coin-bonus-big"
#define kSoundCoinBig2 @"coin-1"
#define kSoundCoinSmall1 @"coin-bonus-small"
#define kSoundBusted1  @"busted"
#define kSoundBusted2  @"busted-1"
#define kSoundBusted3  @"brake-1"
#define kSoundDanger1  @"speeding-high"
#define kSoundDanger2  @"speed-2"
#define kSoundDanger3  @"speed-3"
#define kSoundTicHeavy1 @"speed-limit-down"
#define kSoundTicLight1  @"speed-limit-up"
#define kSoundTicLight2 @"button-tick-1"
#define kSoundSwerve1 @"trunk-rumble"
#define kSoundCaptured1 @"turf-earned"
#define kSoundCaptured2 @"capture-1"
#define kSoundMute @"sfx-mute"
#define kSoundTickUp @"speed-limit-up.m4a"
#define kSoundTickDown @"speed-limit-down.m4a"


@interface RWAudioService : NSObject <AVAudioPlayerDelegate>

@property (nonatomic, strong) NSMutableDictionary *sfxSoundsDictionary;
@property (nonatomic, strong) AVAudioPlayer* audioPlayer;

// from game, only play same sound every N seconds (server variable)
@property (nonatomic, assign) BOOL bCanPlaySound;
@property (nonatomic, strong) NSString *lastSoundEvent;

- (void) playSoundEvent:(NSString *)soundEvent :(NSInteger)fromWhere; // fromWhere...from soundboard, play whenever
- (void) playSound:(NSString *)sound;
- (void) updateSound:(NSString *)sound forSoundEvent:(NSString *)soundEvent;

@end
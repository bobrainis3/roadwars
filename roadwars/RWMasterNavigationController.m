//
//  RWMasterNavigationController.m
//  roadwars
//
//  Created by Christian Allen on 11/6/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWMasterNavigationController.h"
#import "RWDriveViewController.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWContext.h"
#import "CurrentUser.h"
#import "RWServices.h"
#import "RWLocationService.h"
#import "RWFacebookService.h"
#import "UIView+UIView_.h"
#import <FacebookSDK/FBSession.h>
#import <FacebookSDK/FBGraphUser.h>

@interface RWMasterNavigationController ()

@end

@implementation RWMasterNavigationController

@synthesize settingsView = _settingsView;
@synthesize drivingViewController = _drivingViewController;
@synthesize windowBounds=_windowBounds;
@synthesize tabBar1=_tabBar1;


- (void)viewDidLoad { // register for notifications, etc
    [super viewDidLoad];
    
    self.drivingViewController = [ self.navigationController.viewControllers objectAtIndex:0];
    self.windowBounds = [UIScreen mainScreen].bounds;
    //NSLog (@"windowBounds height %f", self.windowBounds.size.height);
    [self addObserver:kOnMotionStart withSelector:@selector(onMotionStart:)];
    [self addObserver:kOnMotionStop withSelector:@selector(onMotionStop:)];
   
    }
- (void)viewDidAppear:(BOOL)animated { // register for notifications, etc
    [super viewDidAppear:animated];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        self.view.frame=CGRectMake(0,0,self.windowBounds.size.width,self.windowBounds.size.height);
    }
    else
        self.view.frame=CGRectMake(0,20,self.windowBounds.size.width,self.windowBounds.size.height+self.tabBar.frame.size.height-20);
}

- (void)dealloc { // best place to unload notifications, release any retained objects, etc
    
    [self removeObserver:kOnMotionStart];
    [self removeObserver:kOnMotionStop];
     
    self.drivingViewController = nil;
}

- (void)onMotionStart:(NSNotification *)notif {
    //NSLog(@"%@:onMotionStart - switch to driving mode...", self.class);
    //NSLog (@"navigatorController frame %@", self.view);
    
    self.selectedIndex=0;
    
    [self postNotification:kOnDoneMotionStart withObject:nil];
}


- (void)onMotionStop:(NSNotification *)notif {
    //NSLog(@"%@:onMotionStop - turn off driving mode, show summary...", self.class);
    //NSLog (@"navigatorController frame %@", self.view);
    
    [self postNotification:kOnDoneMotionStop withObject:nil];
    
}

// for iOS6, rootController needs to handle rotations
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotate {
    return YES;
}


@end

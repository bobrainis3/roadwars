//
//  RWAPIOperation.h
//  RoadWars
//
//  Created by Christian Allen on 7/9/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kFL_API_MAX_ATTEMPTS 1

@interface RWAPIOperation : NSOperation <NSCoding,NSCopying>

@property (nonatomic, strong) NSString *apicall;
@property (nonatomic, strong) NSString *apidata;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSDictionary *result;
@property (nonatomic, assign) NSUInteger attempts;

- (NSDictionary *)refreshSession;
- (NSData *)getBodyForRequest;
- (NSDictionary *)callApi;
- (void)setResultWithJsonResponse:(NSDictionary *)response;
- (id)initWithApiCall:(NSString *)apiCall andData:(NSString *)data;

@end

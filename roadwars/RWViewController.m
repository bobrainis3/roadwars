//
//  RWViewController.m
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
#import "RWViewController.h"
#import "RWNavigationController.h"
#import "Constants.h"

@interface RWViewController ()

@end

@implementation RWViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    RWNavigationController *masterNav = (RWNavigationController *)self.navigationController;
    if ( masterNav != nil ) {
        [masterNav setNavigationItems:self.navigationItem forController:self];
    }
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gravel.png"]];
   
}

// for prior to iOS 6, deprecated in iOS6.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ((interfaceOrientation == UIInterfaceOrientationPortrait ||
             interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)?YES:NO);
}

@end

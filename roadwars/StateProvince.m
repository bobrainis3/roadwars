//
//  StateProvince.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "StateProvince.h"
#import "Country.h"
#import "Road.h"
#import "Town.h"


@implementation StateProvince

@dynamic country;
@dynamic towns;
@dynamic roads;

@end

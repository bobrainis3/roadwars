//
//  main.m
//  roadwars
//
//  Created by Christian Allen on 11/5/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RWAppDelegate class]));
    }
}

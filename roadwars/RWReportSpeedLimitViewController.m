//
//  RWReportSpeedLimitViewController.m
//  RW
//
//  Created by Bob Rainis on 11/8/11.
//  Copyright (c) 2011 bladenet, LLC. All rights reserved.
//

#import "RWReportSpeedLimitViewController.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#pragma mark PassTouch

@interface UIButton (PassTouch)
@end

@implementation UIButton (PassTouch)
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.nextResponder touchesBegan:touches withEvent:event];
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    [self.nextResponder touchesMoved:touches withEvent:event];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self.nextResponder touchesEnded:touches withEvent:event];
}
@end

@implementation RWReportSpeedLimitViewController

@synthesize b5=_b5,b10=_b10,b15=_b15,b20=_b20,b25=_b25,b30=_b30,b35=_b35,b40=_b40,b45=_b45,b50=_b50,b55=_b55,b60=_b60,b65=_b65,b70=_b70,b75=_b75,b80=_b80;
@synthesize buttonSelected=_buttonSelected;
@synthesize postedSpeedLimit=_postedSpeedLimit;
@synthesize speedReportTimer=_speedReportTimer;
@synthesize roadNameLabel=_roadNameLabel;
@synthesize roadName=_roadName;
@synthesize townName=_townName;
@synthesize townNameLabel=_townNameLabel;

#pragma mark - View lifecycle

-(void) viewDidLoad{
    [super viewDidLoad];
    [self addObserver:kOnMotionStop withSelector:@selector(onMotionStop:)];
    self.buttonSelected=nil;
    self.speedReportTimer= [NSTimer scheduledTimerWithTimeInterval:4
                                                            target:self
                                                          selector:@selector(onMotionStop:)
                                                          userInfo:nil
                                                           repeats:NO];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.roadNameLabel.text=[NSString stringWithFormat:@"%@, %@",self.roadName, self.townName ] ;
      
    //NSLog (@"posted speed <%d>", self.postedSpeedLimit.intValue);
    switch (self.postedSpeedLimit.intValue) {
        case 20: self.b20.highlighted=YES;break;
        case 25: self.b25.highlighted=YES;break;
        case 30: self.b30.highlighted=YES;break;
        case 35: self.b35.highlighted=YES;break;
        case 40: self.b40.highlighted=YES;break;
        case 45: self.b45.highlighted=YES;break;
        case 50: self.b50.highlighted=YES;break;
        case 55: self.b55.highlighted=YES;break;
        case 60: self.b60.highlighted=YES;break;
        case 65: self.b65.highlighted=YES;break;
        case 70: self.b70.highlighted=YES;break;
        case 75: self.b75.highlighted=YES;break;
        case 80: self.b80.highlighted=YES;break;
    }
    
  }

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    if (self.buttonSelected!=nil)
        [self postNotification:kOnReportSpeedLimit withObject:self.buttonSelected];
    
    [self removeObserver:kOnMotionStop];
}


-(IBAction)toggleDown:(id)sender {
    
    self.buttonSelected = sender;
    //NSLog (@"Toggle Down %d",self.buttonSelected.tag);
    
    
    self.b20.highlighted=(self.buttonSelected.tag==20)?YES:NO;
    self.b25.highlighted=(self.buttonSelected.tag==25)?YES:NO;
    self.b30.highlighted=(self.buttonSelected.tag==30)?YES:NO;;
    self.b35.highlighted=(self.buttonSelected.tag==35)?YES:NO;;
    self.b40.highlighted=(self.buttonSelected.tag==40)?YES:NO;;
    self.b45.highlighted=(self.buttonSelected.tag==45)?YES:NO;;
    self.b50.highlighted=(self.buttonSelected.tag==50)?YES:NO;;
    self.b55.highlighted=(self.buttonSelected.tag==55)?YES:NO;;
    self.b60.highlighted=(self.buttonSelected.tag==60)?YES:NO;;
    self.b65.highlighted=(self.buttonSelected.tag==65)?YES:NO;;
    self.b70.highlighted=(self.buttonSelected.tag==70)?YES:NO;;
    self.b75.highlighted=(self.buttonSelected.tag==75)?YES:NO;;
    self.b80.highlighted=(self.buttonSelected.tag==80)?YES:NO;;
    [self.navigationController popToRootViewControllerAnimated:YES]; 
}

/*
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
     NSLog(@"DOUBLE TOUCH Began");
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if(((UITouch *)[touches anyObject]).tapCount == 1){
        NSLog(@"DOUBLE TOUCH");
        [self.navigationController popToRootViewControllerAnimated:YES];        
    }
    [super touchesEnded:touches withEvent:event];
}
*/

- (void)onMotionStop:(NSNotification *)notif {
    //NSLog(@"%@:onMotionStop in report speeed lit, pop controller..", self.class);
    [self.navigationController popToRootViewControllerAnimated:YES]; 
        
}


@end

//
//  RWAPI.h
//  RW
//
//  Created by Christian Allen on 9/25/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RWAPIOperation;

@interface RWAPI : NSObject

//typedef void (^RWAPIInitializeCompletionBlock)(NSDictionary* settings);
typedef void (^RWAPIFailureBlock)(NSError *err);
typedef void (^RWAPICompletionBlock)(NSDictionary *result);

// API methods
+ (NSOperation *)initialize:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)reportInaccuracyLat:(CGFloat)lat lng:(CGFloat)lng streetId:(NSNumber *)streetId streetName:(NSString *)streetName speedLimit:(NSInteger)speedLimit direction:(NSNumber *)direction andUserSpeedLimit:(NSInteger)userSpeedLimit completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)incrementAccount:(NSNumber *)account accountStreetId:(NSNumber *)streetId accountStreetName:(NSString *)streetName accountTownId:(NSNumber *)townId accountTownName:(NSString *)townName accountStateProvinceId:(NSNumber *)stateProvinceId accountStateProvinceName:(NSString *)stateProvinceName accountCountryId:(NSNumber *)countryId accountCountryName:(NSString *)countryName accountStreetLat:(CGFloat)lat accountStreetLng:(CGFloat)lng isNewRoad:(BOOL)isNewRoad coin:(NSNumber *)coin driveTime:(NSNumber *)driveTime isFriend:(NSNumber *)isFriend drivingSpeed:(NSNumber *)drivingSpeed speedLimit:(NSNumber *)speedLimit speedLimitMQ:(NSNumber *)speedLimitMQ accelX:(CGFloat)accelX accelY:(CGFloat)accelY accelZ:(CGFloat)accelZ completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)roadAccountInfo:(NSNumber *)account accountStreetId:(NSNumber *)streetId accountStreetName:(NSString *)streetName accountTownId:(NSNumber *)townId accountTownName:(NSString *)townName accountStateProvinceId:(NSNumber *)stateProvinceId accountStateProvinceName:(NSString *)stateProvinceName accountCountryId:(NSNumber *)countryId accountCountryName:(NSString *)countryName accountStreetLat:(CGFloat)lat accountStreetLng:(CGFloat)lng isNewRoad:(BOOL)isNewRoad coin:(NSNumber *)coin driveTime:(NSNumber *)driveTime turbo:(NSNumber *)turbo completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)updateUserFbId:(NSNumber *)fbId withAccessToken:(NSString *)accessToken name:(NSString *)name firstName:(NSString *)firstName lastName:(NSString *)lastName gender:(NSString *)gender completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)userFeedback:(NSString *)subject body:(NSString *)body completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)getSkinny:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)getTurf:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)getPotentialTurf:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)getHitlistWithIdList:(NSString *)idList forDate:(NSString *)forDate fromDate:(NSString *)fromDate  toDate:(NSString *)toDate completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)acquireAttackLock:(NSNumber *)fbId defendingRoadExtId:(NSNumber *)defendingRoadExtId gameType:(NSNumber *)gameType gameName:(NSString *)gameName completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)releaseAttackLock:(NSNumber *)attackingRoadExtId defendingRoadExtId:(NSNumber *)defendingRoadExtId winnerFbId:(NSNumber *)winnerFbId won:(BOOL)won attackerPayment:(NSNumber *) attackerPayment gameType:(NSNumber *)gameType gameName:(NSString *)gameName completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)getDailyStats: (NSNumber *)fbId date:(NSString *)fromDate isAggregated:(BOOL) isAggregated  completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)facebookInvite: (NSNumber *)fbId  completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)wipeAll;

+ (NSOperation *)wipeUser;

+ (NSOperation *)wipeSkinny;

+ (NSOperation *)getInaccuracies:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)checkForUpdate:(NSNumber *)version completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

+ (NSOperation *)checkForSystemMessage:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;


// Convenience methods
+ (NSOperation *)addOperation:(RWAPIOperation *)operation withCompletion:(RWAPICompletionBlock)completion andFailure:(RWAPIFailureBlock)failure;

+ (NSOperation *)addOperationWithNoData:(NSString *)apiCall completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure;

@end

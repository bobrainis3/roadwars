//
//  Rider.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "Rider.h"
#import "Area.h"
#import "Coverage.h"
#import "UserDailyStat.h"

@implementation Rider

@dynamic coin;
@dynamic dailyTake;
@dynamic dateCreated;
@dynamic lastUpdated;
@dynamic lifetimeCoin;
@dynamic lifetimeRoads;
@dynamic name;
@dynamic roads;
@dynamic coverages;
@dynamic ownerships;
@dynamic runnerUpships;
@dynamic userDailyStat;

@end

//
//  RWAPI.m
//  RW
//
//  Created by Christian Allen on 9/25/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//
//  This is the static API to call in any part of app; calls are static, and are
//  added to the API queue, and thus run whenever there is connectivity, and also
//  persisted when app goes to background or is terminated.
//
//  IMPORTANT: all API calls have a completion and failure callback, so you can run
//  further code with the results once they return. The next API call may or may not
//  start in the middle of those callbacks, as they are asynchronous with the API
//  call itself.  If you NEED to have something done before next API call is made,
//  do it in the setResultWithJson method of the APIOperation itself.
//
//  Example call, elsewhere in app:
//
//  [RWAPI callInitializeWithCompletion:^(NSDictionary *)settings {
//          // do something on main thread after the API call returns from server with settings
//      }, ^(NSError *)error {
//          // do something on main thread when the call fails (will have failed multiple attempts to server)
//  }];
//

#import "RWAPI.h"
#import "RWAPIOperationQueue.h"
#import "RWInitializeOperation.h"
#import "RWContext.h"
#import "RWAPIOperation.h"
#import "RWServices.h"

@implementation RWAPI

+ (NSOperation *)initialize:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {

    return [RWAPI addOperation:[[RWInitializeOperation alloc] init] withCompletion:completion andFailure:failure];
}

+ (NSOperation *)reportInaccuracyLat:(CGFloat)lat lng:(CGFloat)lng streetId:(NSNumber *)streetId streetName:(NSString *)streetName speedLimit:(NSInteger)speedLimit direction:(NSNumber*)direction andUserSpeedLimit:(NSInteger)userSpeedLimit completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure{

    RWAPIOperation *reportInaccuracy = [[RWAPIOperation alloc] initWithApiCall:@"reportInaccuracy" andData:[NSString stringWithFormat:@"lat=%5.8f&lng=%5.8f&streetId=%@&streetName=%@&appSpeedLimit=%d&userSpeedLimit=%d&direction=%d", lat, lng, streetId, streetName, speedLimit, userSpeedLimit, direction.intValue]];
    
    return [RWAPI addOperation:reportInaccuracy withCompletion:completion andFailure:nil]; // only call we truly don't care about results
}

+ (NSOperation *)incrementAccount:(NSNumber *)account accountStreetId:(NSNumber *)streetId accountStreetName:(NSString *)streetName accountTownId:(NSNumber *)townId accountTownName:(NSString *)townName accountStateProvinceId:(NSNumber *)stateProvinceId accountStateProvinceName:(NSString *)stateProvinceName accountCountryId:(NSNumber *)countryId accountCountryName:(NSString *)countryName accountStreetLat:(CGFloat)lat accountStreetLng:(CGFloat)lng isNewRoad:(BOOL)isNewRoad coin:(NSNumber *)coin driveTime:(NSNumber *)driveTime isFriend:(NSNumber *)isFriend drivingSpeed:(NSNumber *)drivingSpeed speedLimit:(NSNumber *)speedLimit speedLimitMQ:(NSNumber *)speedLimitMQ accelX:(CGFloat)accelX accelY:(CGFloat)accelY accelZ:(CGFloat)accelZ completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    
    NSMutableString *params = [[NSMutableString alloc] init];
    if ( [account intValue] != 0 ) {
        [params appendFormat:@"account=%@", account];
        [params appendFormat:@"&accountStreetId=%@", streetId];
        [params appendFormat:@"&accountTownId=%@", townId];
        [params appendFormat:@"&accountStateProvinceId=%@", stateProvinceId];
        [params appendFormat:@"&accountCountryId=%@", countryId];
        [params appendFormat:@"&drivingSpeed=%@&speedLimit=%@&speedLimitMQ=%@&accelX=%f&accelY=%f&accelZ=%f", drivingSpeed,speedLimit, speedLimitMQ,accelX, accelY,accelZ];
        if ( isNewRoad ) {
            [params appendFormat:@"&isNewRoad=%@&accountStreetLat=%f&accountStreetLng=%f&accountStreetName=%@&accountTownName=%@&accountStateProvinceName=%@&accountCountryName=%@", (isNewRoad ? @"true" : @"false"), lat, lng, streetName, townName, stateProvinceName, countryName];
        }
    }
    NSString *coinStr=(coin.intValue>=0)?@"coinUp":@"coinDown";
    if ( [coin intValue] != 0 ) {
        if ( [account intValue] == 0 )
            [params appendFormat:@"%@=%d&coin=%d",coinStr, abs(coin.intValue), coin.intValue];
        else
            [params appendFormat:@"&%@=%d&coin=%d",coinStr ,abs(coin.intValue), coin.intValue];
    }
    if ( [driveTime intValue] != 0 ) {
        [params appendFormat:@"&driveTime=%@", driveTime];
    }
   // NSLog (@"isFriend in API call <%d>", isFriend.intValue);
    if ( isFriend.intValue==1 ) {
        [params appendFormat:@"&isFriend=true"];
    }
    
    //NSLog(@"incrementAccount params <%@>", params);
    RWAPIOperation *increment = [[RWAPIOperation alloc] initWithApiCall:@"increment" andData:params];
    
    return [RWAPI addOperation:increment withCompletion:completion andFailure:failure];
}

+ (NSOperation *) roadAccountInfo:(NSNumber *)account accountStreetId:(NSNumber *)streetId accountStreetName:(NSString *)streetName accountTownId:(NSNumber *)townId accountTownName:(NSString *)townName accountStateProvinceId:(NSNumber *)stateProvinceId accountStateProvinceName:(NSString *)stateProvinceName accountCountryId:(NSNumber *)countryId accountCountryName:(NSString *)countryName accountStreetLat:(CGFloat)lat accountStreetLng:(CGFloat)lng isNewRoad:(BOOL)isNewRoad coin:(NSNumber *)coin driveTime:(NSNumber *)driveTime turbo:(NSNumber *)turbo completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    
    NSMutableString *params = [[NSMutableString alloc] init];
         [params appendFormat:@"account=%@", account];
        [params appendFormat:@"&accountStreetId=%@", streetId];
        [params appendFormat:@"&accountTownId=%@", townId];
        [params appendFormat:@"&accountStateProvinceId=%@", stateProvinceId];
        [params appendFormat:@"&accountCountryId=%@", countryId];
        if ( isNewRoad ) {
            [params appendFormat:@"&isNewRoad=%@&accountStreetLat=%f&accountStreetLng=%f&accountStreetName=%@&accountTownName=%@&accountStateProvinceName=%@&accountCountryName=%@", (isNewRoad ? @"true" : @"false"), lat, lng, streetName, townName, stateProvinceName, countryName];
        }
        
    //NSLog(@"roadAccountInfo params <%@>", params);
    RWAPIOperation *increment = [[RWAPIOperation alloc] initWithApiCall:@"increment" andData:params];
    
    return [RWAPI addOperation:increment withCompletion:completion andFailure:failure];
}

+ (NSOperation *)getDailyStats: (NSNumber *)fbId date:(NSString *)fromDate isAggregated:(BOOL) isAggregated completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure{
   
    RWAPIOperation *getDailyStats;
    if (isAggregated)
        getDailyStats = [[RWAPIOperation alloc] initWithApiCall:@"getDailyStats" andData:[NSString stringWithFormat:@"fbId=%@&fromDate=%@&isAggregated=%d", fbId, fromDate, isAggregated]];
    else
         getDailyStats = [[RWAPIOperation alloc] initWithApiCall:@"getDailyStats" andData:[NSString stringWithFormat:@"fbId=%@", fbId]];
    
    return [RWAPI addOperation:getDailyStats withCompletion:completion andFailure:failure];
}

+ (NSOperation *)updateUserFbId:(NSNumber *)fbId withAccessToken:(NSString *)accessToken name:(NSString *)name firstName:(NSString *)firstName lastName:(NSString *)lastName gender:(NSString *)gender completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    
    RWAPIOperation *updateUser = [[RWAPIOperation alloc] initWithApiCall:@"updateUser" andData:[NSString stringWithFormat:@"fbId=%@&fbAt=%@&name=%@&first=%@&last=%@&gender=%@", fbId, accessToken, name, firstName, lastName, gender]];

    //NSLog(@"updateUser <%@>", [NSString stringWithFormat:@"fbId=%@&fbAt=%@&name=%@&first=%@&last=%@&gender=%@", fbId, accessToken, name, firstName, lastName, gender]);
    
    return [RWAPI addOperation:updateUser withCompletion:completion andFailure:failure];
}

+ (NSOperation *)userFeedback:(NSString *)subject body:(NSString *)body completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    
    RWAPIOperation *sendFeedback = [[RWAPIOperation alloc] initWithApiCall:@"sendFeedback" andData:[NSString stringWithFormat:@"subject=%@&body=%@", subject, body]];
    
    return [RWAPI addOperation:sendFeedback withCompletion:completion andFailure:failure];
}

+ (NSOperation *)getSkinny:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    return [RWAPI addOperation:[[RWAPIOperation alloc] initWithApiCall:@"getSkinny" andData:nil] withCompletion:completion andFailure:failure];
}

+ (NSOperation *)getTurf:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    return [RWAPI addOperationWithNoData:@"getTurf" completion:completion failure:failure];
}

+ (NSOperation *)getPotentialTurf:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    return [RWAPI addOperationWithNoData:@"getPotentialTurf" completion:completion failure:failure];
}

+ (NSOperation *)getHitlistWithIdList:(NSString *)idList forDate:(NSString *)forDate fromDate:(NSString *)fromDate  toDate:(NSString *)toDate completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    return [RWAPI addOperation:[[RWAPIOperation alloc] initWithApiCall:@"getHitlist" andData:[NSString stringWithFormat:@"%@&forDate=%@",idList,forDate]] withCompletion:completion andFailure:failure];
}

+ (NSOperation *)acquireAttackLock:(NSNumber *)fbId defendingRoadExtId:(NSNumber *)defendingRoadExtId gameType:(NSNumber *)gameType gameName:(NSString *)gameName completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    NSString *params=(defendingRoadExtId.intValue!=0)? [NSString stringWithFormat:@"fbId=%@&roadExtId=%@&gameType=%@&gameName=%@",fbId,defendingRoadExtId,gameType,gameName] :[NSString stringWithFormat:@"fbId=%@&gameType=%@&gameName=%@",fbId, gameType, gameName] ;
    NSLog (@"acquireAttackLock params <%@>", params);
    return [RWAPI addOperation:[[RWAPIOperation alloc] initWithApiCall:@"acquireAttackLock" andData:params] withCompletion:completion andFailure:failure];
}

+ (NSOperation *)releaseAttackLock:(NSNumber *)attackingRoadExtId defendingRoadExtId:(NSNumber *)defendingRoadExtId winnerFbId:(NSNumber *)winnerFbId won:(BOOL)won attackerPayment:(NSNumber *) attackerPayment gameType:(NSNumber *)gameType gameName:(NSString *)gameName completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure{
    NSString *params=(won)? [NSString stringWithFormat:@"defendingRoadExtId=%@&won=t&attackerPayment=%@&gameType=%@&gameName=%@",defendingRoadExtId, attackerPayment, gameType, gameName] :[NSString stringWithFormat:@"defendingRoadExtId=%@&attackerPayment=%@&gameType=%@&gameName=%@",defendingRoadExtId, attackerPayment, gameType, gameName] ;
    //NSLog(@"releaseAttackLock params %@",params);
    return [RWAPI addOperation:[[RWAPIOperation alloc] initWithApiCall:@"releaseAttackLock" andData:params] withCompletion:completion andFailure:failure];
}

+ (NSOperation *)wipeAll {
    return [RWAPI addOperationWithNoData:@"wipeAll" completion:nil failure:nil];
}

+ (NSOperation *)wipeUser {
    return [RWAPI addOperationWithNoData:@"wipe" completion:nil failure:nil];
}

+ (NSOperation *)wipeSkinny {
    return [RWAPI addOperationWithNoData:@"wipeSkinny" completion:nil failure:nil];
}

+ (NSOperation *)getInaccuracies:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    return [RWAPI addOperationWithNoData:@"getInaccuracies" completion:completion failure:failure];
}

+ (NSOperation *)checkForUpdate:(NSNumber *)version completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure{
    RWAPIOperation *checkForUpdates = [[RWAPIOperation alloc] initWithApiCall:@"checkForUpdates" andData:[NSString stringWithFormat:@"version=%@", version]];
    
    return [RWAPI addOperation:checkForUpdates withCompletion:completion andFailure:failure];
}

+ (NSOperation *)facebookInvite: (NSNumber *)fbId  completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure{
    RWAPIOperation *facebookInvite = [[RWAPIOperation alloc] initWithApiCall:@"onFacebookInvite" andData:[NSString stringWithFormat:@"fbId=%@", fbId]];
    
    return [RWAPI addOperation:facebookInvite withCompletion:completion andFailure:failure];
}

+ (NSOperation *)checkForSystemMessage:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure{
    return [RWAPI addOperationWithNoData:@"checkForSystemMessage" completion:completion failure:failure];
}

#pragma mark Convenience methods for faster adding and setup of API operations and callbacks

+ (NSOperation *)addOperationWithNoData:(NSString *)apiCall completion:(RWAPICompletionBlock)completion failure:(RWAPIFailureBlock)failure {
    return [RWAPI addOperation:[[RWAPIOperation alloc] initWithApiCall:apiCall andData:nil] withCompletion:completion andFailure:failure];
}

+ (NSOperation *)addOperation:(RWAPIOperation *)operation withCompletion:(RWAPICompletionBlock)completion andFailure:(RWAPIFailureBlock)failure {
    
    __weak RWAPIOperation *weakRef = operation; // need this to access results in completion block

    operation.completionBlock = ^{
        
        if ( weakRef.isCancelled ) return;
        
        if ( !weakRef.error && completion != nil ) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                completion(weakRef.result);
            });
        } else if ( failure != nil ) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                failure(weakRef.error);
            });
        }
    };
    [[RWServices apiQueue] addOperation:operation];
    return operation;
}

@end

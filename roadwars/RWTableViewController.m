//
//  RWTableViewController.m
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//

#import "RWTableViewController.h"
#import "RWTableViewCell.h"
#import "RWNavigationController.h"
#import "Constants.h"
#import "RWMasterNavigationController.h"
#import "EGORefreshTableHeaderView.h"
#import "NSObject+ObserverShortcuts.h"
#import <CoreData/CoreData.h>
#import "RWContext.h"
#import "CurrentUser.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) 

@interface RWTableViewController ()

@end

@implementation RWTableViewController

@synthesize placeholderView = _placeholderView;
@synthesize tableView = _tableView;
@synthesize tableData = _tableData;
@synthesize hasRefresh = _hasRefresh;
@synthesize refreshView = _refreshView;
@synthesize lastRefresh = _lastRefresh;
@synthesize isRefreshing = _isRefreshing;

- (void)viewDidLoad {
    [super viewDidLoad];

     
    [self initializeData];
    // NSLog (@"self.tableData.fetchedObjects <%@>, self.tableData.fetchedObjects.count <%d>",self.tableData.fetchedObjects, self.tableData.fetchedObjects.count);
    if ([RWContext me].fbId.longLongValue > 0 ) {
        
        if ( self.tableData.fetchedObjects != nil && self.tableData.fetchedObjects.count > 0 && self.placeholderView != nil ) {
            self.placeholderView.hidden = YES;
        }
    }
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self.tableView setContentInset:UIEdgeInsetsMake(-50, 0, 0, 0)];
    }
   
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.tableData = nil;
    self.tableView = nil;
    self.placeholderView = nil;
}

- (void) viewWillAppear:(BOOL)animated{
    if ( self.hasRefresh ) {
        if (_refreshView==nil){
            _refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height-30, self.view.frame.size.width, self.tableView.bounds.size.height)];
            _refreshView.delegate = self;
            [self.tableView addSubview:_refreshView];
            
            self.lastRefresh = [[NSDate alloc] init];
        }
    }

}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ( self.tableData != nil ) {
        return self.tableData.sections.count;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( self.tableData != nil ) {
        return self.tableData.fetchedObjects.count;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 9.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 9.0)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = kTableCellHeight;
    NSInteger totalRows = [self tableView:tableView numberOfRowsInSection:indexPath.section];
    if ( totalRows == 1 || (totalRows > 1 && indexPath.row == (totalRows - 1)) ) {
        cellHeight = kTableCellHeightBottomRow;
    }
    return cellHeight;
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view {
    //NSLog(@"%@::egoRefreshTableHeaderDidTriggerRefresh - refreshing table", self.class);
    /*
    if ( self.hud == nil ) self.hud = [LGViewHUD defaultHUD];
    self.hud.activityIndicatorOn = YES;
    self.hud.topLabel.text = NSLocalizedString(@"Updating...", nil);
    [self.hud showInView:self.view];
     */
    
     dispatch_async(dispatch_get_main_queue(), ^{
        [self doRefreshDataFromServer]; // this part can/should be subclassed
        [self postNotification:kOnInitializeRoadWarsServer withObject:nil]; //returns up-to-date info on me
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            self.isRefreshing = NO;
            [self.refreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
            //[self.hud hideWithAnimation:(HUDAnimation)nil];
            self.lastRefresh = [[NSDate alloc] init];
           
        });
    });
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	return self.isRefreshing; // should return if data source model is reloading
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view {
	return self.lastRefresh; // should return date data source was last changed
}

- (void)configureCell:(RWTableViewCell *)cell withObject:(NSObject *)obj {
    // OVERRIDE this to implement, please
}

- (void)doRefreshDataFromServer {
    // OVERRIDE this to implement
    
    // this is automatically closed out after changes are made to tableView (see below)
    if ( self.navigationController && [self.navigationController isKindOfClass:[RWNavigationController class]] ) {
        [(RWNavigationController *)self.navigationController showActivity];
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // OVERRIDE this to implement
    return nil;
}

- (RWTableViewCell *)dequeueOrNew:(NSString *)nibName {
    // all RW cells have a xib, so use the name as dequeueing identifier too
    // DON'T FORGET to fill in Identifier field in XIB file
    RWTableViewCell *cell = (RWTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:nibName];
    if ( cell == nil ) cell = [[RWTableViewCell alloc] initWithNibName:nibName];
    return cell;
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

// used for refreshView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	[self.refreshView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	[self.refreshView egoRefreshScrollViewDidEndDragging:scrollView];
    
}

#pragma mark - NSFetchedResultsControllerDelegate stuff

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
    //NSLog (@"begin update");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert: {
            //NSLog(@"%@:controllerDidChangeObject - INSERT", self.class);
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
        }
            break;
            
        case NSFetchedResultsChangeDelete: {
           //NSLog(@"%@:controllerDidChangeObject - DELETE", self.class);
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
        }
            break;
            
        case NSFetchedResultsChangeUpdate: {
            //NSLog(@"%@:controllerDidChangeObject - UPDATE", self.class);
            [self configureCell:(RWTableViewCell *)[tableView cellForRowAtIndexPath:indexPath] withObject:anObject];
        }
            break;
            
        case NSFetchedResultsChangeMove: {
           // NSLog(@"%@:controllerDidChangeObject - MOVE", self.class);
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationNone];
        }
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ( controller.fetchedObjects != nil && controller.fetchedObjects.count > 0 && self.placeholderView != nil ) {
        self.placeholderView.hidden = YES;
    } else if ( self.placeholderView != nil ) {
        self.placeholderView.hidden = NO;
    }
    [self.tableView endUpdates];
    //NSLog (@"end update");

  /*BR:ios7 really flashes, ios6 flashes more subtly
    for (UITableViewCell *cell in self.tableView.visibleCells) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        RWTableViewCell *rwCell = (RWTableViewCell *)cell;
        [rwCell setBackgroundImageForRow:indexPath.row ofTotalRows:controller.fetchedObjects.count];
    }
   */
    
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(canHideActivity)
                                   userInfo:nil
                                    repeats:NO];
    
}
- (void) canHideActivity {
    if ( self.navigationController && [self.navigationController isKindOfClass:[RWNavigationController class]] ) {
        [(RWNavigationController *)self.navigationController hideActivity];
    }
}

- (void)initializeData {
    // override this to initialize the resultscontroller that drives the tableview
}

- (void)refreshTable {
    if ( self.tableData != nil ) {
        NSError *error;
        [self.tableData performFetch:&error];
        if ( error != nil ) {
            NSLog(@"%@:refreshTable - error while refreshing table data: %@", self.class, error.localizedDescription);
        }
    }
}

@end

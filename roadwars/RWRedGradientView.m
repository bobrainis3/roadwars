//
//  RWRedGradientView.m
//  RW
//
//  Created by Bob Rainis.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWRedGradientView.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation RWRedGradientView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CALayer *layer = self.layer;
    layer.cornerRadius = 0.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 0.0f;
    
    CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.layer.bounds;
    _gradientLayer.colors = [NSArray arrayWithObjects:
                             (id)kColorRedViewTop.CGColor,
                             (id)kColorRedViewBottom.CGColor,
                             
                             nil];
    _gradientLayer.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
    [self.layer insertSublayer:_gradientLayer atIndex:0];
    
    
}

@end

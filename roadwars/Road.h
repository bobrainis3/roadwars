//
//  Road.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Area.h"

@class Country, StateProvince, Town;

@interface Road : Area

@property (nonatomic, retain) Town *town;
@property (nonatomic, retain) StateProvince *stateProvince;
@property (nonatomic, retain) Country *country;

@end

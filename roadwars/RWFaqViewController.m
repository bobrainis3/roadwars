//
//  RWFaqViewController
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWFaqViewController.h"
#import "Constants.h"

@interface RWFaqViewController ()

@end

@implementation RWFaqViewController

@synthesize faqWebView=_faqWebView;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSString *url = [serverParameters objectForKey:@"faqUrl"];
    
    [self.faqWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

@end

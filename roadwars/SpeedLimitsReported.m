//
//  SpeedLimitsReported.m
//  roadwars
//
//  Created by Admin on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "SpeedLimitsReported.h"


@implementation SpeedLimitsReported

@dynamic currentDrivingSpeed;
@dynamic currentMQSpeed;
@dynamic currentReportedSpeed;
@dynamic dateStamp;
@dynamic lat;
@dynamic lon;
@dynamic road;
@dynamic state;
@dynamic town;
@dynamic heading;
@dynamic roadId;

@end

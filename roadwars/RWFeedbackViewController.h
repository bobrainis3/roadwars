//
//  RWFeedbackViewController.h
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWViewController.h"
#import <MessageUI/MessageUI.h>

@interface RWFeedbackViewController : RWViewController <UITextViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextView *feedbackComment;
@property (nonatomic, weak) IBOutlet UISlider *feedbackRating;
@property (nonatomic, weak) IBOutlet UIButton *useEmailView;

- (IBAction)doSubmitFeedback:(id)sender;
- (IBAction)doSendFeedbackEmail:(UIButton *)sender;


@end

//
//  RWSkinnyViewController.m
//  roadwars
//
//  Created by Bob Rainis.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWSkinnyViewController.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWTableViewCell.h"
#import "RWAPI.h"
#import "UIView+UIView_.h"
#import "RWContext.h"

@interface RWSkinnyViewController ()

@end

@implementation RWSkinnyViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
}
- (void)dealloc {
    self.tableView.delegate=nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
   
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog (@"skinny count <%d>", [[RWContext skinny]count]);
    if ([[RWContext skinny]count])
        return [[RWContext skinny]count];
    else
        return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RWTableViewCell *cell = nil;
    
    if ([[RWContext skinny]count])
        cell = [self dequeueOrNew:kTableCellSkinny];
    else
        cell = [self dequeueOrNew:kTableCellSkinnyEmpty];
    
    [cell setBackgroundImageForRow:indexPath.row ofTotalRows:([[RWContext skinny]count])?[[RWContext skinny]count]:1];
    
    [self configureCell:cell withObject:([[RWContext skinny]count])?[[RWContext skinny] objectAtIndex:indexPath.row]:nil];
    
    if ( cell == nil ) NSLog(@"RWSkinnyfViewController::cellForRow... - returning a null cell :(");
    return cell;
}

- (void)configureCell:(RWTableViewCell *)cell withObject:(NSObject *)obj {
    NSString *skinnyText = (NSString *)obj;
    // NSLog (@"skinnyText is <%@>", skinnyText);
    
    cell.titleLabel.text = skinnyText;
    if ([skinnyText rangeOfString:@" took"].location != NSNotFound  || [skinnyText rangeOfString:@" safe"].location != NSNotFound || [skinnyText rangeOfString:@" earned"].location != NSNotFound || [skinnyText rangeOfString:@" bonus"].location != NSNotFound || [skinnyText rangeOfString:@"Bonus"].location != NSNotFound || [skinnyText rangeOfString:@" received"].location != NSNotFound || [skinnyText rangeOfString:@" defending"].location != NSNotFound || [skinnyText rangeOfString:@"Collected"].location != NSNotFound
        || [skinnyText rangeOfString:@"Smooth"].location != NSNotFound)
        cell.leftImageView.image = [UIImage imageNamed:@"coin-plus"];
    else
        if ([skinnyText rangeOfString:@" captured"].location != NSNotFound )
            cell.leftImageView.image = [UIImage imageNamed:@"flag-lost"];
        else
            if ([skinnyText rangeOfString:@"Lost"].location != NSNotFound || [skinnyText rangeOfString:@" won"].location != NSNotFound)
                cell.leftImageView.image = [UIImage imageNamed:@"bl_06"];
            else
                if ([skinnyText rangeOfString:@"Captured"].location != NSNotFound )
                    cell.leftImageView.image = [UIImage imageNamed:@"flag"];
                else
                    if ([skinnyText rangeOfString:@"Won "].location != NSNotFound || [skinnyText rangeOfString:@" lost"].location != NSNotFound)
                        cell.leftImageView.image = [UIImage imageNamed:@"w_06"];
                    else
                        if ([skinnyText rangeOfString:@"Reported"].location != NSNotFound)
                            cell.leftImageView.image =[UIImage imageNamed:@"55"];
                        else
                            if ([skinnyText rangeOfString:@"Put Phone Down"].location != NSNotFound)
                                cell.leftImageView.image =[UIImage imageNamed:@"busted-phone"];
                            else
                                if ([skinnyText rangeOfString:@"30 MINUTE PENALTY"].location != NSNotFound)
                                    cell.leftImageView.image =[UIImage imageNamed:@"busted-speed"];
                                else
                                    if ([skinnyText rangeOfString:@"Restart"].location != NSNotFound)
                                        cell.leftImageView.image =[UIImage imageNamed:@"phone-bust"];
                                    else
                                        if ([skinnyText rangeOfString:@" paid"].location != NSNotFound || [skinnyText rangeOfString:@"Lose "].location != NSNotFound || [skinnyText rangeOfString:@"Paid "].location != NSNotFound)
                                            cell.leftImageView.image =[UIImage imageNamed:@"coin-minus"];
                                        else
                                            cell.leftImageView.image =[UIImage imageNamed:@"Icon"];
}

- (IBAction)doDeleteSkinny:(id)sender{
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Delete Activity List"
                                                    message:@"Are you sure you want to delete?"
                                                   delegate:self
                                          cancelButtonTitle:@"NO"
                                          otherButtonTitles:@"YES",nil];
    [alert show];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		//NSLog(@"user pressed NO");
    }else {
		//NSLog(@"user pressed YES");
        [[RWContext skinny]removeAllObjects];
        [self.tableView reloadData];
        [[NSUserDefaults standardUserDefaults] setObject:[RWContext skinny] forKey:@"skinny" ];
        [[NSUserDefaults standardUserDefaults]synchronize];
	}
}

@end

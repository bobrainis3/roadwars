//
//  RWBlueGradientView.m
//  RW
//
//  Created by Bob Rainis.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWBlueGradientView.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation RWBlueGradientView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CALayer *layer = self.layer;
    layer.cornerRadius = 5.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 1.0f;
    layer.borderColor = kColorDaytimeViewTop.CGColor;

    CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.layer.bounds;
    _gradientLayer.colors = [NSArray arrayWithObjects:
                             (id)kColorDaytimeViewTop.CGColor,
                             (id)kColorDaytimeViewTop.CGColor,
                             (id)kColorDaytimeViewBottom.CGColor,
                             nil];
    _gradientLayer.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:0.5f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
    [self.layer insertSublayer:_gradientLayer atIndex:0];
    
    
}

@end

//
//  RWFollowUsFacebookViewController
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWFollowUsFacebookViewController.h"
#import "Constants.h"

@interface RWFollowUsFacebookViewController ()

@end

@implementation RWFollowUsFacebookViewController

@synthesize followUsWebView=_followUsWebView;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSString *url = [serverParameters objectForKey:@"facebookUrl"];
    NSLog(@"do facebook <%@>", url);
      
    [self.followUsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

@end

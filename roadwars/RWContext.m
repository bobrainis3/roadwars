//
//  RWContext.m
//  roadwars
//
//  Created by Christian Allen on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
//  This all convenience for getting user data
//

#import "RWContext.h"
#import "User.h"
#import "CurrentUser.h"
#import "RWHitlist.h"
#import <CoreData/CoreData.h>
#import "NSObject+ObserverShortcuts.h" // RWContext sends notifications, but doesn't receive them - updated directly...
#import "Constants.h"
#import "userDailyStat.h"

#define kCacheHitlist @"Root"

@implementation RWContext

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize hitlist = _hitlist;
@synthesize skinny = _skinny;

+ (NSManagedObjectContext *)managedObjectContext {
    return [[RWContext sharedContext] managedObjectContext];
}

+ (NSManagedObject *)newObject:(NSString *)className {
    return [NSEntityDescription insertNewObjectForEntityForName:className inManagedObjectContext:[RWContext managedObjectContext]];
}

+ (NSEntityDescription *)entityForName:(NSString *)className {
    return [NSEntityDescription entityForName:className inManagedObjectContext:[RWContext managedObjectContext]];
}

+ (void)deleteObject:(NSManagedObject *)obj {
    [[[RWContext sharedContext] managedObjectContext] deleteObject:obj];
}

+ (void)commit {
    NSError *error;
    [[[RWContext sharedContext] managedObjectContext] save:&error];
    if ( error != nil ) {
        NSLog(@"RWContext:commit - error was %@", error.localizedDescription);
    }
}

+ (void)loadMockData {
    //RWContext *context = [RWContext sharedContext];
    User *hitlist1 = (User *)[RWContext newObject:kModelUser];
    User *hitlist2 = (User *)[RWContext newObject:kModelUser];
    User *hitlist3 = (User *)[RWContext newObject:kModelUser];
    hitlist1.name = @"Christian Allen";
    hitlist2.name = @"Paul English";
    hitlist3.name = @"Bob Rainis";
    hitlist1.fbId = [NSNumber numberWithInteger:722767061];
    hitlist2.fbId = [NSNumber numberWithInteger:722767061];
    hitlist3.fbId = [NSNumber numberWithInteger:722767061];
    hitlist1.dailyTake = [NSNumber numberWithInteger:1200];
    hitlist2.dailyTake = [NSNumber numberWithInteger:1000];
    hitlist3.dailyTake = [NSNumber numberWithInteger:1700];
    NSError *error;
    [[[RWContext sharedContext] managedObjectContext] save:&error];
    if ( error != nil ) {
        NSLog(@"%@:loadMockData - error was %@", [RWContext class], error.localizedDescription);
    } else {
        NSLog(@"%@:loadMockData - saved", [RWContext class]);
    }
}

- (void)dealloc {
    self.skinny = nil;
    self.hitlist = nil;
}

+ (RWHitlist *)hitlistForDelegate:(id<NSFetchedResultsControllerDelegate>)delegate {

    RWContext *context = [RWContext sharedContext];
    
    if ( context.hitlist == nil ) {
        context.hitlist = [[RWHitlist alloc] init];
    }
    
    context.hitlist.delegate = delegate;

    NSError *error;
    [context.hitlist performFetch:&error];
    if ( 1 == 0 && context.hitlist.fetchedObjects.count == 0 ) {
        [RWContext loadMockData];
        [context.hitlist performFetch:&error];
    }
    if ( error != nil ) {
        NSLog(@"%@:hitlistForDelegate - fetch returned error: %@", [RWContext class], error.localizedDescription);
    }
    
    return context.hitlist;
}

+ (CurrentUser *)me {
    return [[RWContext sharedContext] me];
}

+ (NSMutableArray *)skinny {
    return [[RWContext sharedContext] skinny];
}

+ (BOOL)isFbConnected {
    return [[[RWContext me] fbId] longLongValue] > 0;
}

+ (void)setCurrentUserFbId:(NSString *)fbId name:(NSString *)name first:(NSString *)first last:(NSString *)last gender:(NSString *)gender email:(NSString *)email{
    NSError *error;
  
    BOOL bSetModelBonusDefault=NO;
    RWContext *context = [RWContext sharedContext];
    if (context.me==nil){
        bSetModelBonusDefault=YES;
        context.me = [NSEntityDescription insertNewObjectForEntityForName:kModelCurrentUser inManagedObjectContext:context.managedObjectContext];
        context.me.userDailyStat =  [NSEntityDescription insertNewObjectForEntityForName:kModeluserDailyStat inManagedObjectContext:context.managedObjectContext];
        [context.managedObjectContext save:&error];
        if ( error != nil ) {
            NSLog(@"initializeCurrentUser - error while saving: %@", error);
        }
        
    }
    
    // BR: Version 1.1, do not want to change data model, will fix for 1.2
    NSLog (@"email is <%@>", email);
    
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"emailAddress"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if ( fbId.longLongValue == 0 ) {
        // hmphf - we got a bum fbId... maybe in the future this is a "logout" mechanism?
        [context postNotification:kOnCurrentUserFbDisconnect withObject:context.me];
    } else if ( context.me.fbId.longLongValue == 0 ) {
        // setting FB ID for the first time... big deal
        context.me.fbId = [NSNumber numberWithLongLong:[fbId longLongValue]];
        context.me.name = name;
        context.me.first = first;
        context.me.last = last;
        context.me.gender = gender;
        if (bSetModelBonusDefault){ //if male, enable models.
            // BR: for Version 1, no models
           // [[NSUserDefaults standardUserDefaults] setBool:[NSNumber numberWithBool:([gender isEqualToString:@"male"])?YES:NO] forKey:MODELS_PREF_KEY];
            [[NSUserDefaults standardUserDefaults] setBool:[NSNumber numberWithBool:NO] forKey:MODELS_PREF_KEY];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
       
        NSError *error = nil;
        [context.managedObjectContext save:&error];
        if ( error != nil ) {
            NSLog(@"RWContext:setCurrentUser - error while saving is %@", error.localizedDescription);
        } else {
            NSLog(@"RWContext:setCurrentUser - user is now %@", context.me);
        }
        [context postNotification:kOnCurrentUserFbConnect withObject:context.me];
    } else if ( context.me.fbId.longLongValue == [fbId longLongValue] ) {
        // already logged in as this user. maybe check if name has changed?
        if ( ![name isEqualToString:context.me.name] ) {
            context.me.name = name;
            context.me.first = first;
            context.me.last = last;
            context.me.gender = gender;
            [RWContext commit];
        }
    } else if ( context.me.fbId.longLongValue > 0 && context.me.fbId.longLongValue != fbId.longLongValue ) {
        // ugh - was previously logged in as somebody else, now switching users...
        // in the future, we should probably stop everything in game, stop listening to user interaction,
        // load info from server about this user, then resume...
        [context postNotification:kOnCurrentUserFbSwitch withObject:context.me];
    }
}

- (void)initializeCurrentUser {
    
    self.skinny=[[NSMutableArray alloc]init];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kModelCurrentUser];
    
    NSError *error;
    NSManagedObjectContext *objectContext = [self managedObjectContext];
    NSArray *currentUsers = [objectContext executeFetchRequest:fetchRequest error:&error];
    
    if ( error == nil ) {
        //NSLog(@"Current user(s): %@", currentUsers);
        if ( currentUsers != nil && currentUsers.count > 0 ) {
            _me = [currentUsers objectAtIndex:0];
            if (_me.userDailyStat==nil)
                _me.userDailyStat =  [NSEntityDescription insertNewObjectForEntityForName:kModeluserDailyStat inManagedObjectContext:objectContext];
        }
        
        else {
            _me = [NSEntityDescription insertNewObjectForEntityForName:kModelCurrentUser inManagedObjectContext:objectContext];
            _me.userDailyStat =  [NSEntityDescription insertNewObjectForEntityForName:kModeluserDailyStat inManagedObjectContext:objectContext];
            [objectContext save:&error];
            if ( error != nil ) {
                NSLog(@"initializeCurrentUser - error while saving: %@", error);
            }
               
        }
         
         
    } else {
        NSLog(@"initializeCurrentUser - error while fetching: %@", error);
    }
    //NSLog(@"%@:initializeCurrentUser - starting up with currentUser: %@", self.class, _me);
}

+ (void)reloadHitlist:(NSArray *)hitlistUsers {
    RWContext *context = [RWContext sharedContext];
    NSError *error;
    NSFetchRequest *getAllUsers = [[NSFetchRequest alloc] initWithEntityName:kModelUser];
    NSArray *allUsers = [context.managedObjectContext executeFetchRequest:getAllUsers error:&error];
    
    // out with the old...
    if ( allUsers != nil ) {
        CurrentUser *me = [RWContext me];
        for (NSManagedObject *managedObject in allUsers) {
            if ( managedObject != me ) {
                [context.managedObjectContext deleteObject:managedObject];
                
            }
        }
    }
    
    // in with the new...
    if ( hitlistUsers != nil ) {
        for (int i = 0; i < hitlistUsers.count; i++ ) {
            NSDictionary *userData = [hitlistUsers objectAtIndex:i];
            //NSLog (@"userData <%@>", userData);
            User *newUser = (User *)[RWContext newObject:kModelUser];
            newUser.fbId = [userData objectForKey:@"fbId"];
            newUser.name = [userData objectForKey:@"name"];
            newUser.first = [userData objectForKey:@"first"];
            newUser.last = [userData objectForKey:@"last"];
            newUser.coin = [userData objectForKey:@"coin"];
            //newUser.coin = [userData objectForKey:@"totalCoin"];
            newUser.dailyTake = [userData objectForKey:@"dailyTake"];
            newUser.roads = [userData objectForKey:@"streets"];
            //newUser.roads = [userData objectForKey:@"totalRoads"];
            newUser.userDailyStat = (userDailyStat *)[RWContext newObject:kModeluserDailyStat];
            //NSLog (@"newUser %@",newUser);
        }
    }
    [RWContext commit];
}

// usage: [RWContext getEntityOfType:kModelUser where[NSPredicate predicateWithFormat:@"self.fbId == %@", facebookId]];
+ (NSManagedObject *)getEntityOfType:(NSString *)entityName where:(NSPredicate *)predicate {
    RWContext *context = [RWContext sharedContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:entityName inManagedObjectContext:context.managedObjectContext];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError *error;
    NSManagedObject *ret = nil;
    NSArray *results = [context.managedObjectContext executeFetchRequest:request error:&error];
    if (results != nil) {
        NSUInteger count = [results count]; // May be 0 if the object has been deleted.
        if ( count == 1 ) {
            ret = [results objectAtIndex:0];
        } else {
           // NSLog(@"%@:getEntityOfType - WARNING: fetch request for single object returned an array...", [RWContext class]);
        }
    } else {
        NSLog(@"%@:getEntityOfType - error while fetching object: %@", [RWContext class], error.localizedDescription);
    }
    return ret;
}

// usage: [RWContext getEntityOfType:kModelUser where[NSPredicate predicateWithFormat:@"self.fbId == %@", facebookId]];
+ (NSArray *)getEntityOfTypeReturningArray:(NSString *)entityName where:(NSPredicate *)predicate {
    RWContext *context = [RWContext sharedContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:entityName inManagedObjectContext:context.managedObjectContext];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *results = [context.managedObjectContext executeFetchRequest:request error:&error];
    return results;
}


#pragma mark Singleton and Init
static RWContext *context = nil;

+ (RWContext *)sharedContext {
    static dispatch_once_t onceQueue;
    dispatch_once(&onceQueue, ^{
        context = [[self alloc] init];
    });
    return context;
}

- (id)init {
    
    /* in case we want to update database without having user delete app first
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL bInstalledVersion54 = [prefs integerForKey:@"bInstalledVersion54"];
    if (bInstalledVersion54 == FALSE) { // replace db
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        
        NSString *myFilePath = [documentsDirectoryPath stringByAppendingPathComponent:@"RoadWars.sqlite"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:myFilePath error:NULL];
        
        bInstalledVersion54 = TRUE;
        [prefs setInteger:bInstalledVersion54 forKey:@"bInstalledVersion54"];
        
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
     */
    
    if ((self = [super init])) {
        [self initializeCurrentUser];
        if ( _me == nil ) {
            NSLog(@"current user is nil");
        }
    }
    return self;
}

#pragma mark - Core Data stack

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"RoadWars" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"RoadWars.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - add skinny messages
+ (void) addSkinnyMessage: (NSString *)skinnyMessage appendTime:(BOOL)appendTime{
    
    NSString *_date;
    if (appendTime){
    NSString * timeStampString = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
    NSTimeInterval _interval=[timeStampString doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateStyle:NSDateFormatterShortStyle];
    //NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"Ehh:mma" options:0 locale:[NSLocale currentLocale]];
    [_formatter setDateFormat:@"E h:mma"];
    _date=[_formatter stringFromDate:date];
    } else
        _date=@"";
    
    [self.skinny addObject:[NSString stringWithFormat:@"%@ %@",skinnyMessage, _date]];
}


@end

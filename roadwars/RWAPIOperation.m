//
//  RWAPIOperation.m
//  RoadWars
//  Created by Christian Allen on 7/9/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWAPIOperation.h"
#import "UIDevice+IdentifierAddition.h"
#import "RWContext.h"
#import "Constants.h"

#define kFL_KEY_APIDATA @"kFL_KEY_APIDATA"
#define kFL_KEY_APICALL @"kFL_KEY_APICALL"
#define kFL_KEY_ATTEMPTS @"kFL_KEY_ATTEMPTS"

@implementation RWAPIOperation

@synthesize error = _error;
@synthesize apidata = _apidata;
@synthesize apicall = _apicall;
@synthesize attempts = _attempts;
@synthesize result = _result;

- (id)initWithApiCall:(NSString *)apiCall andData:(NSString *)data
{
    if ((self = [super init])) {
        _apicall = apiCall;
        _apidata = data;
        _attempts = 0;
    }
    return self;
}

- (NSData *)getBodyForRequest {
    if ( self.apidata == nil ) return nil;
    else return [self.apidata dataUsingEncoding:NSUTF8StringEncoding]; // most basic conversion to data
}

- (NSDictionary *)refreshSession { // checking this before addeding to op queue is not good enough, so...
    NSString *params = [NSString stringWithFormat:@"deviceId=%@&deviceType=%@&secret=%@", [[UIDevice currentDevice] uniqueDeviceIdentifier], kServerDeviceType, kServerSecret];
    
    //NSLog(@"RWAPIOperation::refreshSession - refreshing session from server with %@", params);
    return [self callApiWithAction:@"initialize" andData:[params dataUsingEncoding:NSUTF8StringEncoding]];
}

- (NSDictionary *)callApi {
    return [self callApiWithAction:_apicall andData:[self getBodyForRequest]];
}

- (NSDictionary *)callApiWithAction:(NSString *)action andData:(NSData *)data {
   
    NSString *urlString = [NSString stringWithFormat:@"http://%@%@/%@/", kServerHost, kServerApiPath, action];
    NSLog (@"urlString is <%@><%@>", urlString,data);
    //NSString *urlString = [NSString stringWithFormat:@"http://15.185.111.108%@/%@/", kServerApiPath, action];
   
    NSURL *url = [NSURL URLWithString:urlString];
    NSError *urlError;
    NSURLResponse *response;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = data;
    
    // TODO: this var is for debugging only - remove when we go live
    NSString *dataAsQueryString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
   // if ( dataAsQueryString.length > 200 ) dataAsQueryString = [NSString stringWithFormat:@"%@%@", [dataAsQueryString substringToIndex:200], @"..."];
    
    if ( self.isCancelled ) return nil; // may have been canceled during getBodyForRequest
    
    /*
     // TODO: enable this by creating a RWAPIClient class...
    if ( [RWContext timeSinceLastApiCall] > kserverSessionDuration && ![action isEqualToString:@"initialize"] ) {
        [self refreshSession];
        NSLog(@"RWAPIOperation::callApi - just came back from calling emergency session for API operation %d (%@)", self.hash, action);
    }
    */
    
    if ( self.isCancelled ) return nil; // may have been canceled during refreshSession
    
    NSLog(@"RWAPIOperation::callApi - sending request for API operation #%d (/api/%@/?%@), attempt #%d", self.hash, action, dataAsQueryString, (self.attempts + 1));
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&urlError];
    
    if ( self.isCancelled ) return nil;
    
    // TODO: this var is just for debugging - later we should comment it out - extra work for nothing
    NSString *responseUtf8 = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSDictionary *json = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:responseData
                                                                         options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves
                                                                           error:&urlError];
   NSLog(@"FLAPIOperation::callApi - API call %@ with data [%@] returned %@, and error %@ for operation #%d", urlString, request.HTTPBody, json, urlError, [self hash]);
    
    NSNumber *hadSuccessNum = (NSNumber *)[json objectForKey: @"success"];
    BOOL hadSuccess = (hadSuccessNum && [hadSuccessNum boolValue] == YES);
    if ( (!hadSuccess || urlError != nil) && self.attempts < kFL_API_MAX_ATTEMPTS ) { // WTF!!  fail - do it again...
       // NSLog(@"RWAPIOperation::callApi - REDOING API call #%d (/api/%@/?%@) because response was %@, hadSuccess was %@, urlError was %@, and attempts were %d", self.hash, action, dataAsQueryString, responseUtf8, (hadSuccess ? @"true" : @"false"), urlError, self.attempts);
        self.attempts++;
        if ( !hadSuccess ) [self refreshSession];  // refresh session before next attempt, if necessary
        json = [self callApiWithAction:action andData:data];
    } else {
        //[RWContext noteLastApiCall];  // make a note of last successful api call
        //if ( responseUtf8.length > 200 ) responseUtf8 = [NSString stringWithFormat:@"%@%@", [responseUtf8 substringToIndex:200], @"..."];
        //NSLog(@"RWAPIOperation::callApi - success for api call #%d (/api/%@/?%@), response was %d bytes total, starting with this: %@", self.hash, action, dataAsQueryString, responseData.length, responseUtf8);
    }
    
    return json;
}

- (void)setResultWithJsonResponse:(NSDictionary *)response {
    // OVERRIDE for API calls that actually do something with the response (not all of them do)
    self.result = response;
}

#pragma mark NSOperation Overrides
- (void)main {
    //NSLog(@"FLAPIOperation::main - kicking off FLAPIOperation #%d, %@", [self hash], self.apicall);
    @autoreleasepool {
        @try {
            NSDictionary *jsonResponse = [self callApi];
            if ( self.isCancelled ) return;
            if ( jsonResponse != nil && [jsonResponse isKindOfClass:[NSDictionary class]] ) {
                [self setResultWithJsonResponse:jsonResponse];
            }
        } @catch (NSException *exception) {
            NSLog(@"RWAPIOperation triggered exception: %@", exception);
            self.error = [NSError errorWithDomain:exception.name code:400 userInfo:exception.userInfo];
        }
    }
}


#pragma mark NSCoder methods
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.apicall forKey:kFL_KEY_APICALL];
    [encoder encodeInteger:self.attempts forKey:kFL_KEY_ATTEMPTS];
    [encoder encodeObject:self.apidata forKey:kFL_KEY_APIDATA];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super init])) {
        self.apicall = [decoder decodeObjectForKey:kFL_KEY_APICALL];
        self.attempts = [decoder decodeIntegerForKey:kFL_KEY_ATTEMPTS];
        self.apidata = [decoder decodeObjectForKey:kFL_KEY_APIDATA];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    id theCopy = [[[self class] allocWithZone:zone] init];  // use designated initializer
    
    [theCopy setApicall:[self.apicall copyWithZone:zone]];
    [theCopy setApidata:[self.apidata copyWithZone:zone]];
    [theCopy setAttempts:self.attempts];
    
    return theCopy;
}

@end

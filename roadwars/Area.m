//
//  Area.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "Area.h"
#import "Rider.h"


@implementation Area

@dynamic name;
@dynamic abbreviation;
@dynamic lat;
@dynamic lng;
@dynamic ownerAccount;
@dynamic runnerUpAccount;
@dynamic dateCreated;
@dynamic lastUpdated;
@dynamic dailyTake;
@dynamic extId;
@dynamic lifetimeTake;
@dynamic owner;
@dynamic runnerUp;
@dynamic coverages;

@end

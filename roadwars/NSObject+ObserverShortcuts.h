//
//  NSObject+ObserverShortcuts.h
//  roadwars
//
//  Created by Christian Allen on 11/6/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
//  I hate how verbose adding and removing observers is, so I made these easy-to-add shortcuts
//  which are much shorter than their NSNotificationCenter counterparts, still seem to cover
//  the 98% use case
//

#import <Foundation/Foundation.h>

@interface NSObject (ObserverShortcuts)

-(void)addObserver:(NSString *)notificationName withSelector:(SEL)selector;
-(void)removeObserver:(NSString *)notificationName;
-(void)postNotification:(NSString *)notificationName withObject:(NSObject *)obj; // only one that's already simple

@end

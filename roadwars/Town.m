//
//  Town.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "Town.h"
#import "Country.h"
#import "Road.h"
#import "StateProvince.h"


@implementation Town

@dynamic stateProvince;
@dynamic country;
@dynamic roads;

@end

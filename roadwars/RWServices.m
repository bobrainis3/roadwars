//
//  RWServices.m
//  roadwars
//
//  Created by Bob Rainis on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWServices.h"
#import "RWLocationService.h"
#import "RWGeocodingService.h"
#import "RWAccelerometerService.h"
#import "RWBonusTimerService.h"
#import "RWDrivingTimerService.h"
#import "RWFacebookService.h"
#import "RWAPIOperationQueue.h"
#import "RWAudioService.h"

#define kKeyApiOperationQueue @"kKeyApiOperationQueue"

@implementation RWServices
@synthesize locationService = _locationService;
@synthesize geocodingService = _geocodingService;
@synthesize accelerometerService = _accelerometerService;
@synthesize facebookService = _facebookService;
@synthesize apiQueue = _apiQueue;
@synthesize audioService = _audioService;
@synthesize bonusTimerService=_bonusTimerService;

#pragma mark Convenience getters

+ (RWLocationService *)locationService { return [[RWServices sharedServices] locationService]; }
+ (RWGeocodingService *)geocodingService { return [[RWServices sharedServices] geocodingService]; }
+ (RWAccelerometerService *)accelerometerService { return [[RWServices sharedServices] accelerometerService]; }
+ (RWBonusTimerService *)bonusTimerService { return [[RWServices sharedServices] bonusTimerService]; }
+ (RWDrivingTimerService *)drivingTimerService { return [[RWServices sharedServices] drivingTimerService]; }
+ (RWFacebookService *)facebookService { return [[RWServices sharedServices] facebookService]; }
+ (RWAPIOperationQueue *)apiQueue { return [[RWServices sharedServices] apiQueue]; }
+ (RWAudioService *)audioService { return [[RWServices sharedServices] audioService]; }

#pragma mark Singleton and Init
static RWServices *services = nil;

+ (RWServices *)sharedServices {
    static dispatch_once_t onceQueue;
    dispatch_once(&onceQueue, ^{
        services = [[self alloc] init];
    });
    return services;
}

- (id)init {
    if ((self = [super init])) {
        // instantiate all services here
        _locationService = [[RWLocationService alloc] init];
        _geocodingService = [[RWGeocodingService alloc] init];
        _accelerometerService = [[RWAccelerometerService alloc] init];
        _bonusTimerService = [[RWBonusTimerService alloc]init];
        _drivingTimerService = [[RWDrivingTimerService alloc]init];
        _facebookService = [[RWFacebookService alloc] init];
        _apiQueue = (RWAPIOperationQueue *)[RWServices loadObjectOfClass:[RWAPIOperationQueue class] fromKey:kKeyApiOperationQueue withDefault:nil];
        _audioService = [[RWAudioService alloc]init];
        
    }
    return self;
}

+ (void)startMotionDetection {
    [[[RWServices locationService] locationManager] startUpdatingLocation];
   // [[[RWServices locationService] locationManager] startUpdatingHeading];
}

+ (void)startLocationService {
    [[[RWServices locationService] locationManager] startUpdatingLocation];
    //[[[RWServices locationService] locationManager] startUpdatingHeading];
}

+ (void)stopLocationService {
    [[[RWServices locationService] locationManager] stopUpdatingLocation];
   // [[[RWServices locationService] locationManager] stopUpdatingHeading];
}

+ (void)startAccelerometerService {
    [[RWServices accelerometerService] startAccelerometerDetect];
}

+ (void)stopAccelerometerService {
    [[RWServices accelerometerService] stopAccelerometerDetect];
}

+ (void)startBonusTimerService {
    [[RWServices bonusTimerService] start];
}

+ (void)stopBonusTimerService {
    [[RWServices bonusTimerService] stop];
}

#pragma mark Storage Convenience - need ways to persist/serialize/deserialize service states (like apiqueue)

+ (void)storeObject:(NSObject *)root withKey:(NSString *)key {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:root];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
}

+ (NSObject *)loadObjectOfClass:(Class)aClass fromKey:(NSString *)key withDefault:(NSObject *)defaultObj {
    
    NSData *storedData = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    NSObject *temp = nil;
    
    if ( storedData != nil ) {
        temp = [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
        if ( temp != nil ) {
            
            if ( [temp respondsToSelector:@selector(mutableCopyWithZone:)] ) {
                return [temp mutableCopy];
            } else if ( [temp respondsToSelector:@selector(copyWithZone:)] ) {
                return [temp copy];
            } else {
                return temp;
            }
        } else {
            return (defaultObj == nil) ? [[aClass alloc] init] : defaultObj;
        }
    } else {
        return (defaultObj == nil) ? [[aClass alloc] init] : defaultObj;
    }
}

@end

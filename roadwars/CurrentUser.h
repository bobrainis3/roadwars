//
//  CurrentUser.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "User.h"


@interface CurrentUser : User

@property (nonatomic, retain) NSNumber * bonusTime;
@property (nonatomic, retain) NSNumber * attackLevel;
@property (nonatomic, retain) NSNumber * attackRandomizedDiceOrder;
@property (nonatomic, retain) NSNumber * numBonusRecieved;

@end

//
//  RWAPIOperationQueue.m
//  RoadWars
//
//  Created by Christian Allen on 9/25/12.
//  Copyright (c) 2012 bladnet LLC. All rights reserved.
//

#import "RWAPIOperationQueue.h"
#import "RWAPIOperation.h"
#import "Reachability.h"
#import "UIDevice+IdentifierAddition.h"
#import "RWAppDelegate.h"
#import "RWContext.h"
#import "RWServices.h"
#import "RWFacebookService.h"
#import "CurrentUser.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWAPI.h"
#import <FacebookSDK/FBGraphUser.h>
#import <FacebookSDK/FBSession.h>
#import <FacebookSDK/FBAccessTokenData.h>

#define kFL_KEY_OPERATIONS @"kFL_KEY_OPERATIONS"
#define kFL_KEY_LASTCALL @"kFL_KEY_LASTCALL"

@implementation RWAPIOperationQueue

@synthesize lastCall = _lastCall;
@synthesize apiReach = _apiReach;

- (id)init {
    if ((self = [super init])) {
        self.maxConcurrentOperationCount = 1;
        _lastCall = nil;  // api hasn't been called yet
        // so we suspend the APIQueue by default, and let Reachibility turn it on and off with connectivity
        self.suspended = YES;
        [self addObserver:kReachabilityChangedNotification withSelector:@selector(onNetworkChange:)];
        [self addObserver:kOnFacebookFriendsRequestComplete withSelector:@selector(onFacebookFriendsUpdate:)];
        [self addObserver:kOnUserAccountUpdate withSelector:@selector(onUserAccountUpdate:)];
        [self addObserver:kOnRoadUpdate withSelector:@selector(onRoadUpdate:)];
        
        [self addObserver:kOnFacebookMeRequestComplete withSelector:@selector(onFacebookMeRequestComplete:)];
        [self addObserver:kOnFacebookLogin withSelector:@selector(onFacebookLogin:)];
        
        _apiReach = [Reachability reachabilityWithHostname:kServerHost];
        [_apiReach startNotifier];
        //NSLog(@"RWAPIOperationQueue::init - started queue suspended");
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:kReachabilityChangedNotification];
    [self removeObserver:kOnFacebookFriendsRequestComplete];
    [self removeObserver:kOnFacebookMeRequestComplete];
    [self removeObserver:kOnUserAccountUpdate];
    [self removeObserver:kOnRoadUpdate];
    [self removeObserver:kOnFacebookLogin];
}

-(void)onNetworkChange:(NSNotification *)notif { // here's the part where reachability's notify turns queue on/off
    if ( self.apiReach == notif.object ) { // there could be other reachability objects throwing notifications out there...
        NetworkStatus remoteHostStatus = [self.apiReach currentReachabilityStatus];
        [self setSuspended:(remoteHostStatus == NotReachable)]; // suspends queue when not reachable, resumes when it is
        //NSLog(@"RWAPIOperationQueue::onNetworkChange - API queue is %@", ((remoteHostStatus == NotReachable) ? @"suspended" : @"resumed"));
    }
}

- (void)onFacebookFriendsUpdate:(NSNotification *)notif {
    if ( notif != nil && notif.object != nil ) {
        NSArray *friends = (NSArray *)notif.object;
        NSMutableString *friendIds = nil;
        for (NSDictionary<FBGraphUser>* friend in friends) {
            if ( friendIds == nil ) {
                friendIds = [[NSMutableString alloc] initWithFormat:@"friends=%@", friend.id];
            } else {
                [friendIds appendFormat:@",%@", friend.id];
            }
        }
        if (friendIds.length>0){ //only do if we have friends
            NSDate *today = [NSDate date];
          //  NSDate *thisWeek  = [today dateByAddingTimeInterval: -604800.0];
            NSDateFormatter *       formatter;
            
            formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat: @"yyyy-MM-dd"];
            [formatter setTimeZone: [NSTimeZone localTimeZone]];
            
            NSString *stringToday = [formatter stringFromDate: today];
            

            [RWAPI getHitlistWithIdList:friendIds forDate:stringToday fromDate:nil toDate:nil completion:^(NSDictionary * results) {
                NSDictionary *resultsData = [results objectForKey:@"data"];
                if ( resultsData != nil ) {
                    //NSLog (@"hitlist <%@>",resultsData);
                    NSArray *hitlistUsers = [resultsData objectForKey:@"hitlist"];
                    if ( hitlistUsers != nil ) {
                        [RWContext reloadHitlist:hitlistUsers];
                    } else {
                        NSLog(@"Did not get hitlist");
                    }
                }
            } failure:^(NSError *error) {
                // sigh... what to do?
            }];
        }
    }
}

- (void)onUserAccountUpdate:(NSNotification *)notif{
    //NSLog (@"onUserAccountUpdate");
    CurrentUser *me = [RWContext me];
    
    if ( notif != nil && notif.object != nil ) {
        NSDictionary *d =(NSDictionary*)notif.object;
        NSNumber *account = [d objectForKey:@"incrementAccount"];
        //NSLog (@"accountupdate <%@>",d);
        [RWAPI incrementAccount:[d objectForKey:@"incrementAccount"]
                accountStreetId:[d objectForKey:@"accountStreetId"]
              accountStreetName:[d objectForKey:@"accountStreetName"]
                  accountTownId:[d objectForKey:@"accountTownId"]
                accountTownName:[d objectForKey:@"accountTownName"]
         accountStateProvinceId:[d objectForKey:@"accountStateProvinceId"]
       accountStateProvinceName:[d objectForKey:@"accountStateProvinceName"]
               accountCountryId:[d objectForKey:@"accountCountryId"]
             accountCountryName:[d objectForKey:@"accountCountryName"]
               accountStreetLat:[[d objectForKey:@"accountStreetLat"] doubleValue]
               accountStreetLng:[[d objectForKey:@"accountStreetLng"] doubleValue]
                      isNewRoad:(account.intValue==1)?YES:NO//[[d objectForKey:@"isNewRoad"] boolValue]
                           coin:[d objectForKey:@"coin"]
                      driveTime:[d objectForKey:@"driveTime"]
                       isFriend:[d objectForKey:@"isFriend"]
                   drivingSpeed:[d objectForKey:@"drivingSpeed"]
                     speedLimit:[d objectForKey:@"speedLimit"]
                   speedLimitMQ:[d objectForKey:@"speedLimitMQ"]
                         accelX:[[d objectForKey:@"accelX"] doubleValue]
                         accelY:[[d objectForKey:@"accelY"] doubleValue]
                         accelZ:[[d objectForKey:@"accelZ"] doubleValue]
                     completion:^(NSDictionary *result) {
                         if ( result != nil ) {
                             //NSLog (@"result from onUserAccountUpdate - %@", result);
                             if ([[result objectForKey:@"success"] intValue] == 1){
                    //BR:  place to store info for the CurrentUser.
                    NSDictionary *userData=[[result objectForKey:@"data"]objectForKey:@"user"];
                    NSNumber *toll = [[result objectForKey:@"data"]objectForKey:@"toll"];
                    if (toll != nil && toll.intValue!=0)
                        [self postNotification:kOnTollsOut withObject:result];
                    
                    me.fbId = [userData objectForKey:@"fbId"];
                    me.name = [userData objectForKey:@"name"];
                    me.first= [userData objectForKey:@"first"];
                    me.last = [userData objectForKey:@"last"];
                    me.coin = [userData objectForKey:@"coin"];
                    me.dailyTake = [userData objectForKey:@"dailyTake"];
                    me.roads = [userData objectForKey:@"streets"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [RWContext commit];
                    });
                                   
                    if ([[result objectForKey:@"data"]objectForKey:@"captured"]!=nil){
                      [self postNotification:kOnCapturedRoad withObject:result];
                    } else{
                        NSDictionary *roadCoverageDict = [result objectForKey:@"data"];
                        [self postNotification:kOnIsRoadOwnedAccountUpdate withObject:roadCoverageDict];
                    }
                }
            }
        } failure:^(NSError *error) {
            // do something on failure?
        }];
    }
      
}

- (void)onRoadUpdate:(NSNotification *)notif{
   // NSLog (@"onRoadUpdate");
    
    
    if ( notif != nil && notif.object != nil ) {
        NSDictionary *d =(NSDictionary*)notif.object;
        //NSLog (@"accountupdate <%@>",d);
        [RWAPI roadAccountInfo:[NSNumber numberWithInt:0]
                accountStreetId:[d objectForKey:@"accountStreetId"]
              accountStreetName:[d objectForKey:@"accountStreetName"]
                  accountTownId:[d objectForKey:@"accountTownId"]
                accountTownName:[d objectForKey:@"accountTownName"]
         accountStateProvinceId:[d objectForKey:@"accountStateProvinceId"]
       accountStateProvinceName:[d objectForKey:@"accountStateProvinceName"]
               accountCountryId:[d objectForKey:@"accountCountryId"]
             accountCountryName:[d objectForKey:@"accountCountryName"]
               accountStreetLat:[[d objectForKey:@"accountStreetLat"] doubleValue]
               accountStreetLng:[[d objectForKey:@"accountStreetLng"] doubleValue]
                      isNewRoad:NO//[[d objectForKey:@"isNewRoad"] boolValue]
                           coin:[NSNumber numberWithInt:0]
                          driveTime:0
                          turbo:0
                    completion:^(NSDictionary *result) {
                        if ( result != nil ) {
                            //NSLog (@"result from onRoadUpdate - %@", result);
                            if ([[result objectForKey:@"success"] intValue] == 1){
                                NSDictionary *roadCoverageDict = [[result objectForKey:@"data"]objectForKey:@"road"];
                                [self postNotification:kOnIsRoadOwned withObject:roadCoverageDict];
                                
                            }
                        }
                    } failure:^(NSError *error) {
                        // do something on failure?
                     }];
    }
    
 
}

- (void)onFacebookMeRequestComplete:(NSNotification *)notif {
    
    if ( notif != nil && notif.object != nil ) { // the object will be a FB response to /graph/me
        // for one, get my turf after you've gotten (or updated) my FB ID...
        
        //BR: looking to associate RW user with FB user
        NSDictionary<FBGraphUser> *me = (NSDictionary<FBGraphUser> *)notif.object;
        
        double meId = [me.id doubleValue];
        NSDictionary *userData = (NSDictionary *)notif.object;
        NSString *gender = userData[@"gender"];
        //NSLog(@"gender <%@>", gender);
         NSString *fbAccessToken = [[[FBSession activeSession] accessTokenData] accessToken];
        //NSLog(@"fbAccessToken <%@>",fbAccessToken);
        [RWAPI updateUserFbId:[NSNumber numberWithDouble:meId] withAccessToken:fbAccessToken name:me.name firstName:me.first_name lastName:me.last_name gender:gender completion:^(NSDictionary *result) {
            //NSLog (@"result from onFacebookMeRequestComplete - %@", result);
            //BR:  place to store info for the CurrentUser.
            CurrentUser *me = [RWContext me];
            NSDictionary *userData=[[result objectForKey:@"data"]objectForKey:@"user"];
            me.fbId = [userData objectForKey:@"fbId"];
            me.name = [userData objectForKey:@"name"];
            me.first= [userData objectForKey:@"first"];
            me.last = [userData objectForKey:@"last"];
            me.coin = [userData objectForKey:@"coin"];
            me.dailyTake = [userData objectForKey:@"dailyTake"];
            me.roads = [userData objectForKey:@"streets"];
            me.gender = [userData objectForKey:@"gender"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [RWContext commit];
            });

        } failure:^(NSError *error) {
            // do anything on failure?
            NSLog (@"error from onFacebookMeRequestComplete - %@", error);
        }];
    }
}

- (void)onFacebookLogin:(NSNotification *)notif {
    if ( notif != nil && notif.object != nil ) {
    }
}

#pragma mark NSCoder methods
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.operations forKey:kFL_KEY_OPERATIONS]; // should work bc all are FLAPIOperations
}

- (id)initWithCoder:(NSCoder *)decoder {
    //NSLog(@"FLAPIOperationQueue::initWithCoder - thawing APIQueue from frozen");
    if ((self = [self init])) {
        NSArray *ops = (NSArray *)[decoder decodeObjectForKey:kFL_KEY_OPERATIONS];
        for (RWAPIOperation *op in ops) {
            [self addOperation:op];
        }
        // no use decoding lastcall, since it will be updated from adding initialize call
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"FLAPIOperationQueue { operationsCount: %d }", self.operationCount];
}

@end

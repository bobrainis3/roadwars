//
//  RWHitlistTableViewController.h
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTableViewController.h"
#import <CoreData/CoreData.h>

@class RWHitlist;

@interface RWHitlistTableViewController : RWTableViewController
@property (nonatomic, assign) BOOL bTopDogIsMe;
@property (nonatomic, assign) int indexPathForAttack;
@property (nonatomic, weak) IBOutlet UIImageView *sortCoinImageView;
@property (nonatomic, weak) IBOutlet UIImageView *sortRoadImageView;
@property (nonatomic, weak) IBOutlet UIImageView *sortDailyImageView;
@property (nonatomic, weak) IBOutlet UIImageView *sortWeeklyImageView;


- (IBAction)doFacebookConnect:(id)sender;
- (IBAction)doAttackPlay:(id)sender;
- (IBAction)doRoadCoinSort:(id)sender;
- (IBAction)doDailyWeeklySort:(id)sender;
- (IBAction)doReturnToDriveView:(id)sender;

@end

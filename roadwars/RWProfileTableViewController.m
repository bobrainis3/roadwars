//
//  RWProfileTableViewController.m
//  roadwars
//
//  Created by Christian Allen on 11/16/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWProfileTableViewController.h"
#import "UIView+UIView_.h"
#import "User.h"
#import "RWTableViewCell.h"
#import <FacebookSDK/FBProfilePictureView.h>
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWContext.h"
#import "CurrentUser.h"
#import "RWAttackPlayViewController.h"
#import "RWAPI.h"
#import "MWPhotoBrowser.h"
#import "RWGetMyTurfViewController.h"

@interface RWProfileTableViewController ()

@end

@implementation RWProfileTableViewController

@synthesize profilePicture = _profilePicture;
@synthesize profilePictureView = _profilePictureView;
@synthesize userName = _userName;
@synthesize flagTotal = _flagTotal;
@synthesize coinTotal = _coinTotal;
@synthesize user = _user;
@synthesize attackButton=_attackButton;
@synthesize attackLabel=_attackLabel;
@synthesize attackAnywhereView=_attackAnywhereView;
@synthesize bTopDogIsMe=_bTopDogIsMe;
@synthesize indexPathForAttack=_indexPathForAttack;
@synthesize droveTodayLabel=_droveTodayLabel;
@synthesize droveWeekLabel=_droveWeekLabel;
@synthesize attacksTodayLabel=_attacksTodayLabel;
@synthesize attacksWeekLabel=_attacksWeekLabel;
@synthesize attacksPercentWonLabel=_attacksPercentWonLabel;
@synthesize sortAlpha=_sortAlpha;
@synthesize turfView=_turfView;
@synthesize skinnyView=_skinnyView;
@synthesize emptyImageView=_emptyImageView;
@synthesize todayCoinEarnedLabel=_todayCoinEarnedLabel;
@synthesize todayRoadsCapturedLabel=_todayRoadsCapturedLabel;

BOOL bButtonPressed;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    if ([[[RWContext me] fbId] longLongValue] > 0)
        self.emptyImageView.hidden=YES;
    else
        self.emptyImageView.hidden=NO;
    
     self.attackLabel.textColor=kColorGold;
     //NSLog (@"self.bTopDogIsMe %d self.indexPathForAttack %d", self.bTopDogIsMe , self.indexPathForAttack);
    if ( self.user != nil ) {
       
        //NSLog (@"first <%@> last <%@", self.user.first, self.user.last);
        self.profilePicture.profileID = self.user.fbId.stringValue;
        [self formatNumbers:self.user];
        [self setTitle:[NSString stringWithFormat:@"%@ %@.",self.user.first,[self.user.last substringToIndex:1]]];
        self.skinnyView.hidden=self.turfView.hidden=(self.user==[RWContext me])?NO:YES;
        if (self.sortAlpha<1.0){ // sort by roads
            
            if (self.user==[RWContext me] || (self.bTopDogIsMe && self.indexPathForAttack>1) ||
                (!self.bTopDogIsMe && [RWContext me].roads.intValue > self.user.roads.intValue)){
                if (self.user==[RWContext me]){
                    if ([RWContext me].numBonusRecieved.intValue<1){
                        self.attackAnywhereView.hidden=YES;
                        self.attackLabel.hidden=self.attackButton.hidden=YES;
                    }
                    else {
                         //BR: for version 1, no models
                        /*
                        if ([[NSUserDefaults standardUserDefaults] boolForKey: MODELS_PREF_KEY])
                           
                         self.attackLabel.text=@"Show Today's Bonuses";
                         else
                         */
                        {
                            self.attackAnywhereView.hidden=YES;
                            self.attackLabel.hidden=self.attackButton.hidden=YES;
                        }
                    }
                    
                }else{
                    self.attackAnywhereView.hidden=YES;
                    self.attackLabel.hidden=self.attackButton.hidden=YES;
                }
            }
        } else {
            if (self.user==[RWContext me] || (self.bTopDogIsMe && self.indexPathForAttack>1) ||
                (!self.bTopDogIsMe && [RWContext me].coin.intValue > self.user.coin.intValue)){
                self.attackAnywhereView.hidden=YES;
                self.attackLabel.hidden=self.attackButton.hidden=YES;
            }
        }
    }
    else // no user object sent, use CurrentUser
        [self setUser:[RWContext me]];

    //hide the view when the download starts
    self.profilePicture.startHandler = ^(DBFBProfilePictureView* view){
        view.layer.opacity = 0.0f;
    };
    //show the view when the download completes, or show the empty image
    self.profilePicture.completionHandler = ^(DBFBProfilePictureView* view, NSError* error){
        if(error) {
            view.showEmptyImage = YES;
            //NSLog(@"Loading profile picture failed with error: %@", error);
        }
        [UIView animateWithDuration:0.1f animations:^{
            view.layer.opacity = 1.0f;
        }];
    };
    
    
}

-(void) viewWillAppear:(BOOL)animated{    [super viewWillAppear:animated];
    bButtonPressed=NO;
}

- (void)setUser:(User *)user {
    //NSLog (@"user <%@>", user);
    _user = user;
    self.userName.text = user.name;
    self.profilePicture.profileID = user.fbId.stringValue;
    [self formatNumbers:user];
    NSNumber *fbId =self.user.fbId;
    NSDate *today = [NSDate date];
    NSDate *thisWeek  = [today dateByAddingTimeInterval: -6666604800.0];
    NSDateFormatter *       formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"yyyy-MM-dd"];
    [formatter setTimeZone: [NSTimeZone localTimeZone]];
    
    NSLog (@"user.coin <%@> user.roads <%@>", user.coin, user.roads);
    NSString *stringDate = [formatter stringFromDate: thisWeek];
    //NSLog (@"1.stringDate %@,self.user.fbId %@ ",stringDate, fbId);
    [RWAPI getDailyStats:fbId
                    date:stringDate
            isAggregated:NO
              completion:^(NSDictionary *result) {
                  //NSLog (@"1.result from getDailyStats - %@", result);
                  NSDictionary *stats=[[result objectForKey:@"data"]objectForKey:@"stats"];
                  // NSLog (@"stats from getDailyStats - %@", stats);
                  
                  NSNumber *totalAttack = [stats objectForKey:@"totalAttack"];
                 // NSNumber *roadsUpAttack = [stats objectForKey:@"roadsUpAttack"];
                  NSNumber *driveTime = [stats objectForKey:@"driveTime"];
                  // NSLog(@"drive time today <%@>",driveTime);
                  float winPercentage;
                  
                  float todayDrivingMinutes;
                  todayDrivingMinutes=driveTime.floatValue/60;
                  //NSLog(@"2.drive time today <%f>",todayDrivingMinutes);
                  if (todayDrivingMinutes<60)
                      self.droveTodayLabel.text=[NSString stringWithFormat:@"%3.0fm", todayDrivingMinutes];
                  else
                      self.droveTodayLabel.text=[NSString stringWithFormat:@"%3.1fh", todayDrivingMinutes/60];
                  
                  self.attacksTodayLabel.text = [NSString stringWithFormat:@"%3.0f", totalAttack.floatValue];
                  
                  if (self.user==[RWContext me]){
                      if (user.userDailyStat.totalAttack.intValue==0)
                          self.attacksPercentWonLabel.text = nil;
                      else {
                          winPercentage=user.userDailyStat.roadsUpAttack.floatValue/user.userDailyStat.totalAttack.floatValue*100;
                          
                          if (winPercentage==100.0f)
                              self.attacksPercentWonLabel.text = [NSString stringWithFormat:@"(%3.0f%%)", winPercentage];
                          else if (winPercentage<10.0f)
                              self.attacksPercentWonLabel.text = [NSString stringWithFormat:@"(%1.0f%%)", winPercentage];
                          else
                              self.attacksPercentWonLabel.text = [NSString stringWithFormat:@"(%2.0f%%)", winPercentage];
                      }
                      
                      
                  }
                  NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                  [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                  
                  NSNumber *coinUpDriving = [stats objectForKey:@"coinUpDriving"];
                  NSNumber *coinUpTolls = [stats objectForKey:@"coinUpTolls"];
                  NSNumber *coinDownTolls = [stats objectForKey:@"coinDownTolls"];
                  NSNumber *roadsUpDriving =[stats objectForKey:@"roadsUpDriving"];
                  NSNumber *roadsDownDriving =[stats objectForKey:@"roadsDownDriving"];
                  NSNumber *roadsUpAttack =[stats objectForKey:@"roadsUpAttack"];
                  NSNumber *roadsDownAttack =[stats objectForKey:@"roadsDownAttack"];
                  
                  self.todayCoinEarnedLabel.text=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[ NSNumber numberWithInt: coinUpDriving.intValue + coinUpTolls.intValue - coinDownTolls.intValue]]];
                 
                  self.todayRoadsCapturedLabel.text=[NSString stringWithFormat:@"%d", roadsUpDriving.intValue - roadsDownDriving.intValue + roadsUpAttack.intValue - roadsDownAttack.intValue];

                 // dispatch_async(dispatch_get_main_queue(), ^{
                 //     [RWContext commit];
                 // });
              } failure:^(NSError *error) {
                  // do anything on failure?
                  NSLog (@"error from getDailyStats - %@", error);
              }];
    
    // NSLog (@"2.stringDate %@,self.user.fbId %@ ",stringDate, fbId);
    [RWAPI getDailyStats:fbId
                    date:stringDate
            isAggregated:YES
              completion:^(NSDictionary *result) {
                  //NSLog (@"2.result from getDailyStats - %@", result);
                  NSDictionary *stats=[[result objectForKey:@"data"]objectForKey:@"stats"];
                  // NSLog (@"stats from getDailyStats - %@", stats);
                  NSNumber *weeklyTotalAttack = [stats objectForKey:@"totalAttack"];
                  NSNumber *weeklyRoadsCapturedAttack = [stats objectForKey:@"roadsUpAttack"];
                  NSNumber *weeklyDrivingTime = [stats objectForKey:@"driveTime"];
                  float winPercentage;
                  
                  float weeklyDrivingMinutes;
                  weeklyDrivingMinutes=weeklyDrivingTime.floatValue/60;
                  if (weeklyDrivingMinutes<60)
                      self.droveWeekLabel.text=[NSString stringWithFormat:@"%3.0fm", weeklyDrivingMinutes];
                  else
                      self.droveWeekLabel.text=[NSString stringWithFormat:@"%3.1fh", weeklyDrivingMinutes/60];
                  
                  
                  self.attacksWeekLabel.text = [NSString stringWithFormat:@"%3.0f", weeklyTotalAttack.floatValue];
                  
                  if (self.user==[RWContext me]){
                      if (weeklyTotalAttack.intValue==0)
                          self.attacksWeeklyPercentWonLabel.text = nil;
                      else {
                          winPercentage=weeklyRoadsCapturedAttack.floatValue/weeklyTotalAttack.floatValue*100;
                          if (winPercentage==100.0f)
                              self.attacksWeeklyPercentWonLabel.text = [NSString stringWithFormat:@"(%3.0f%%)", winPercentage];
                          else if (winPercentage<10.0f)
                              self.attacksWeeklyPercentWonLabel.text = [NSString stringWithFormat:@"(%1.0f%%)", winPercentage];
                          else
                              self.attacksWeeklyPercentWonLabel.text = [NSString stringWithFormat:@"(%2.0f%%)", winPercentage];
                      }
                  }
              } failure:^(NSError *error) {
                  // do anything on failure?
                  NSLog (@"error from getDailyStats - %@", error);
              }];
    

}

-(void) formatNumbers: (User *)user{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedNumberString = [numberFormatter stringFromNumber:user.coin];
    self.coinTotal.text = formattedNumberString;
    formattedNumberString = [numberFormatter stringFromNumber:user.roads];
    self.flagTotal.text = formattedNumberString;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RWTableViewCell *cell = nil;
    cell = [self dequeueOrNew:kTableCellHitlist];
    if ( cell == nil ) NSLog(@"RWHitlistTableViewController::cellForRow... - returning a null cell :(");
    return cell;
}

- (void)configureCell:(RWTableViewCell *)cell withObject:(User *)user {
    cell.titleLabel.text = user.name;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ( [[segue identifier] isEqualToString:kSegueAttackToAttackPlay] ) {
        RWAttackPlayViewController *attackController = [segue destinationViewController];
        attackController.user = self.user;
        attackController.roadId = 0;
    }
    else  if ( [[segue identifier] isEqualToString:kSegueAttackToShowBonus] ) {
        NSMutableArray *photosIn = [[NSMutableArray array]init];
        MWPhotoBrowser *photoBrowser = [segue destinationViewController];

        for (int i=50; i<= [RWContext me].numBonusRecieved.intValue; i+=50){
            NSString *fileName=[NSString stringWithFormat:@"model_%d", i];
            [photosIn addObject:[MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:fileName ofType:@"jpg"]]];
        }
        // Store photos
        if ([photoBrowser initWithPhotos:photosIn bBonusModels:TRUE] == nil)
            NSLog(@"Problem with photo browser");
    }
    else  if ( [[segue identifier] isEqualToString:kSegueMyTurf] ) {
            RWGetMyTurfViewController *getMyTurfController = [segue destinationViewController];
            getMyTurfController.user = self.user;
        
    }
    
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!bButtonPressed){
        
        if(((UITouch *)[touches anyObject]).tapCount == 1){
            NSLog(@"SINGLE TOUCH");
            
            if (!self.attackAnywhereView.isHidden) {
                if (self.user==[RWContext me])
                    [self performSegueWithIdentifier:kSegueAttackToShowBonus sender:nil];
                else
                    [self performSegueWithIdentifier:kSegueAttackToAttackPlay sender:nil];
            }
            
        }
    }
    [super touchesEnded:touches withEvent:event];
}

- (IBAction)getTurf:(id)sender{
    bButtonPressed=YES;
    [self performSegueWithIdentifier:kSegueMyTurf sender:nil];
}

- (IBAction)getSkinny:(id)sender{
    bButtonPressed=YES;
    [self performSegueWithIdentifier:kSegueSkinny sender:nil];
}


@end

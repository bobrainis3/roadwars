//
//  RWHitlistTableViewController.m
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//
#import <CoreData/CoreData.h>

#import "RWHitlistTableViewController.h"
#import "RWMasterNavigationController.h"
#import "RWProfileTableViewController.h"
#import "RWAttackPlayViewController.h"
#import "RWNavigationController.h"
#import "RWContext.h"
#import "CurrentUser.h"
#import "RWTableViewCell.h"
#import "RWServices.h"
#import "RWHitlist.h"
#import "RWFacebookService.h"
#import "Constants.h"
#import "User.h"
#import "NSObject+ObserverShortcuts.h"
#import <FacebookSDK/FBProfilePictureView.h>
#import <FacebookSDK/FBSession.h>
#import <FacebookSDK/FBGraphUser.h>
#import "MWPhotoBrowser.h"

//#define kHitlistRefreshInterval -60  // number of seconds to wait until ok to refresh (should be negative)
#define kHitlistRefreshInterval -1  // number of seconds to wait until ok to refresh (should be negative)

@implementation RWHitlistTableViewController
@synthesize bTopDogIsMe=_bTopDogIsMe;
@synthesize indexPathForAttack=_indexPathForAttack;
@synthesize sortCoinImageView=_sortCoinImageView;
@synthesize sortRoadImageView=_sortRoadImageView;
@synthesize sortDailyImageView=_sortDailyImageView;
@synthesize sortWeeklyImageView=_sortWeeklyImageView;

BOOL bCanSelectRow;

- (void)initializeData {
    [super initializeData];
    
    self.tableData = [RWContext hitlistForDelegate:self];
  
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.hasRefresh=YES;
    
    self.sortRoadImageView.image= [UIImage imageNamed:@"flag.png"];
    self.sortCoinImageView.image= [UIImage imageNamed:@"coin.png"];
    self.sortDailyImageView.image= [UIImage imageNamed:@"1.png"];
    self.sortWeeklyImageView.image= [UIImage imageNamed:@"7.png"];
    
    self.sortCoinImageView.alpha=.2;
    self.sortRoadImageView.alpha=1.0;
    self.sortWeeklyImageView.alpha=.2;
    self.sortDailyImageView.alpha=1.0;
    
   

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
  
    self.bTopDogIsMe=NO;
    bCanSelectRow=NO;
    if ([RWContext me].fbId.longLongValue > 0 ) {
        [NSTimer scheduledTimerWithTimeInterval:2.0
                                         target:self
                                       selector:@selector(canSelectRow)
                                       userInfo:nil
                                        repeats:NO];
        if ( self.lastRefresh == nil || [self.lastRefresh timeIntervalSinceNow] < kHitlistRefreshInterval ) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self doRefreshDataFromServer]; // this part can/should be subclassed
                [self postNotification:kOnInitializeRoadWarsServer withObject:nil]; //returns up-to-date info on me
                dispatch_async(dispatch_get_main_queue(), ^{
                      self.isRefreshing = NO;
                    [self.refreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
                    //[self.hud hideWithAnimation:(HUDAnimation)nil];
                    self.lastRefresh = [[NSDate alloc] init];
                    [self.tableView reloadData];
                });
            });
            
        }
    }
}
- (void)dealloc {
    self.tableView.delegate=nil;
}

- (void) canSelectRow{
    bCanSelectRow=YES;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RWTableViewCell *cell = nil;

    cell = [self dequeueOrNew:kTableCellHitlist];
    [cell setBackgroundImageForRow:indexPath.row ofTotalRows:self.tableData.fetchedObjects.count];
    
    User *user = [self.tableData objectAtIndexPath:indexPath];
    // am I top of hitlist
    //NSLog (@"indexpath.item %d", indexPath.item);
    if (user == [RWContext me] && indexPath.item==0)
        self.bTopDogIsMe=YES;
    cell.rightButton.tag=indexPath.row;
    [self configureCell:cell withObject:user];
       
    if ( cell == nil ) NSLog(@"RWHitlistTableViewController::cellForRow... - returning a null cell :(");
    return cell;
}

- (void)configureCell:(RWTableViewCell *)cell withObject:(NSObject *)obj {
    User *user = (User *)obj;
    //NSLog (@"user is <%@>", user);
    // cell.rightButton.layer.sublayers = nil;
    // BR: is the user me?
    if (user == [RWContext me]){
        if (user.first.length && user.last.length)
            cell.titleLabel.text = [NSString stringWithFormat:@"%@ %@. %@", user.first, [user.last substringToIndex:1], @"(me)"];
        cell.rightButton.titleLabel.hidden=YES;
        cell.rightButton.titleLabel.text=@"activity";
        [cell.rightButtonImageView setImage:[UIImage imageNamed:@"activity.png"]];
        
    } else {
        cell.titleLabel.text = [NSString stringWithFormat:@"%@ %@.", user.first, [user.last substringToIndex:1]];
        
        //int numRoads = (self.bTopDogIsMe)?[RWContext me].roads.intValue-50:[RWContext me].roads.intValue;//BR:server param
        if (self.sortCoinImageView.alpha<1.0){ // sort by roads
            if ((user.roads.intValue>0 && ((self.bTopDogIsMe && cell.rightButton.tag==1) || (user.roads.intValue>= [RWContext me].roads.intValue)))){
                cell.rightButton.titleLabel.hidden=YES;
                cell.rightButton.titleLabel.text=@"attack";
                [cell.rightButtonImageView setImage:[UIImage imageNamed:@"attack-btn.png"]];
            }else{
                cell.rightButton.titleLabel.hidden = YES;
                cell.rightButton.titleLabel.text=@"info";
                [cell.rightButtonImageView setImage:[UIImage imageNamed:@"info.png"]];
            }
        }
        else
            cell.rightButton.hidden = (user.coin.intValue>0 && ((self.bTopDogIsMe && cell.rightButton.tag==1) || (user.coin.intValue>= [RWContext me].coin.intValue)))?NO:YES;
        
    }
    
    cell.leftProfileView.profileID = user.fbId.stringValue;
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    if (self.sortCoinImageView.alpha<1.0){ // sort by roads
        cell.rightLabel.text = [numberFormatter stringFromNumber:user.roads];
        cell.descriptionLabel.text = [numberFormatter stringFromNumber:user.coin];
        cell.rightImageView.image = [UIImage imageNamed:@"flag.png"];
        cell.leftImageView.image = [UIImage imageNamed:@"coin.png"];
    } else {
        cell.rightLabel.text = [numberFormatter stringFromNumber:user.coin];
        cell.descriptionLabel.text = [numberFormatter stringFromNumber:user.roads];
        cell.rightImageView.image = [UIImage imageNamed:@"coin.png"];
        cell.leftImageView.image = [UIImage imageNamed:@"flag.png"];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.indexPathForAttack=indexPath.item;
    if (bCanSelectRow)
        [self performSegueWithIdentifier:kSegueHitlistToProfile sender:[self.tableData objectAtIndexPath:indexPath]];
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:kSegueHitlistToProfile] ) {
        RWProfileTableViewController *profileController = [segue destinationViewController];
        profileController.user = (User *)sender;
        profileController.bTopDogIsMe=self.bTopDogIsMe;
        profileController.indexPathForAttack=self.indexPathForAttack;
        profileController.sortAlpha = self.sortCoinImageView.alpha;
    }
    else
        if ( [[segue identifier] isEqualToString:kSegueAttackToAttackPlay] ) {
            RWAttackPlayViewController *attackController = [segue destinationViewController];
            attackController.user = (User *)sender;
            attackController.roadId = 0;
        }
        else  if ( [[segue identifier] isEqualToString:kSegueQuickHelpImages] ) {
            MWPhotoBrowser *photoBrowser = [segue destinationViewController];
            NSMutableArray *photosIn = [NSMutableArray array];
            [self.navigationController setNavigationBarHidden:YES];
            [photosIn addObject:[MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"homePage" ofType:@"png"]]];
            [photosIn addObject:[MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"driveScreen_1" ofType:@"png"]]];
            [photosIn addObject:[MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"attackList" ofType:@"png"]]];
            [photosIn addObject:[MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"attackMode" ofType:@"png"]]];
            [photosIn addObject:[MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"newScreen" ofType:@"png"]]];
            
            // Store photos
            if ([photoBrowser initWithPhotos:photosIn bBonusModels:FALSE] == nil)
                NSLog(@"Problem with photo browser");
            
        }

}

- (void)doRefreshDataFromServer {
    [super doRefreshDataFromServer];
    self.bTopDogIsMe=NO;
    //NSLog (@"fbId <%@>",[RWContext me].fbId );
    //if ([RWContext me].fbId.longLongValue > 0 ) { // only if we've ever done this before should this be automagic
        [self doFacebookConnect:nil]; // refreshing hitlist means re-connect w facebook, get updated friends list, hitlist
    //}
   // [self.tableView reloadData];
    
}

- (IBAction)doFacebookConnect:(id)sender {
    NSLog(@"%@::doFacebookConnect", self.class);
    [[RWServices facebookService] openSession];
   
    
}

- (IBAction)doAttackPlay:(UIButton*)sender {
    NSLog (@"sender.titleLabel.text %@", sender.titleLabel.text);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow: sender.tag inSection: 0];
    if ([sender.titleLabel.text rangeOfString:@"attack"].location != NSNotFound) {
        sender.backgroundColor = [UIColor grayColor];
        [self performSelector:@selector(selectRowAtIndexPath:) withObject:indexPath afterDelay:0.5];
    } else if ([sender.titleLabel.text rangeOfString:@"info"].location != NSNotFound) {
        [self performSegueWithIdentifier:kSegueHitlistToProfile sender:[self.tableData objectAtIndexPath:indexPath]];
    } else {// must be activity
        [self postNotification:kOnGetSkinny withObject:nil];
        [self performSegueWithIdentifier:kSegueHitlistToActivity sender:[self.tableData objectAtIndexPath:indexPath]];
    }

  
}
-(void) selectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:kSegueAttackToAttackPlay sender:[self.tableData objectAtIndexPath:indexPath]];

}

- (IBAction)doRoadCoinSort:(UIButton*)sender {
    //NSLog(@"%f::doRoadCoinSort", self.sortCoinImageView.alpha);
    NSString *sortString1, *sortString2;
    if (self.sortCoinImageView.alpha<1.0){
        self.sortCoinImageView.alpha=1.0;
        self.sortRoadImageView.alpha=.2;
        sortString1=@"coin";
        sortString2=@"roads";
    }
    else{
        self.sortCoinImageView.alpha=.2;
        self.sortRoadImageView.alpha=1.0;
        sortString1=@"roads";
        sortString2=@"coin";

    }       
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:sortString1 ascending:NO];
    NSSortDescriptor *sort2 = [[NSSortDescriptor alloc] initWithKey:sortString2 ascending:NO];
  	
    [self.tableData.fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort, sort2, nil]];
    [self initializeData];
    [self.tableView reloadData];
   
}

- (IBAction)doDailyWeeklySort:(UIButton*)sender {
    //NSLog(@"%f::doDailyWeeklySort", self.sortWeeklyImageView.alpha);
    //NSString *sortString1, *sortString2;
    if (self.sortWeeklyImageView.alpha<1.0){
        self.sortWeeklyImageView.alpha=1.0;
        self.sortWeeklyImageView.highlighted=YES;
        self.sortDailyImageView.alpha=.2;
        self.sortDailyImageView.highlighted=NO;
      //  sortString1=@"coin";
      //  sortString2=@"roads";
    }
    else{
        self.sortWeeklyImageView.alpha=.2;
        self.sortWeeklyImageView.highlighted=NO;
        self.sortDailyImageView.alpha=1.0;
        self.sortDailyImageView.highlighted=YES;
        
      //  sortString1=@"roads";
     //   sortString2=@"coin";
        
    }
    /*
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:sortString1 ascending:NO];
    NSSortDescriptor *sort2 = [[NSSortDescriptor alloc] initWithKey:sortString2 ascending:NO];
  	
    [self.tableData.fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort, sort2, nil]];
    [self initializeData];
    [self.tableView reloadData];
    */
}

- (IBAction)doReturnToDriveView:(id)sender {
    self.navigationController.tabBarController.selectedIndex=0;
}

@end

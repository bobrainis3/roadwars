//
//  RWTableViewCell.h
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBFBProfilePictureView.h"

#define kTableCellTurf @"RWTableViewCellTurf"
#define kTableCellTurfEmpty @"RWTableViewCellTurfEmpty"
#define kTableCellPotentialTurf @"RWTableViewCellPotentialTurf"
#define kTableCellPotentialTurfEmpty @"RWTableViewCellPotentialTurfEmpty"
#define kTableCellSkinny @"RWTableViewCellSkinny"
#define kTableCellSkinnyEmpty @"RWTableViewCellSkinnyEmpty"
#define kTableCellLooseEnds @"RWTableViewCellLooseEnds"
#define kTableCellLooseEndsEmpty @"RWTableViewCellLooseEndsEmpty"
#define kTableCellHitlist @"RWTableViewCellHitlist"
#define kTableCellBigButton @"RWTableViewCellBigButton"
#define kTableCellLoser @"RWTableViewCellLoser"
#define kTableCellHitlistEmpty @"RWTableViewCellHitlistEmpty"
#define kTableCellHitlistDisconnected @"RWTableViewCellHitlistDisconnected"
#define kTableCellShop @"RWTableViewCellShop"
#define kTableCellFriends @"RWTableViewCellFriends"
#define kTableCellDriveSnapshot @"RWTableViewCellDriveSnapshot"
#define kTableCellSettings @"RWTableViewCellSettings"
#define kTableCellSoundBoard @"RWSoundBoardCell"
#define kTableCellProfileTop @"RWTableViewCellProfileTop"
#define kTableCellSettings @"RWTableViewCellSettings"

#define kTableCellHeight 50.0
#define kTableCellHeightBigButton 60.0
#define kTableCellHeightBottomRow 56.0
#define kTableCellHeightInfo 175.0

@class RWBigButton;

@interface RWTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *leftImageView;
@property (nonatomic, strong) IBOutlet DBFBProfilePictureView *leftProfileView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *rightImageView;
@property (nonatomic, weak) IBOutlet UILabel *rightLabel;
@property (nonatomic, weak) IBOutlet UILabel *rightDescriptionLabel;
@property (nonatomic, weak) IBOutlet UIButton *rightButton;
@property (nonatomic, weak) IBOutlet UIImageView *rightButtonImageView;


- (id)initWithNibName:(NSString *)nibNameOrNil;
- (void)setBackgroundImageForRow:(NSUInteger)row ofTotalRows:(NSUInteger)totalRows;

@end

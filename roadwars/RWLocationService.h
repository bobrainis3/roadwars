//
//  RWLocationService.m
//  roadwars
//
//  Created by Bob Rainis on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>

@interface RWLocationService : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *lastLocation;
@property (nonatomic, strong) CLLocation *previousLocation;
@property (nonatomic, assign) BOOL bDriving;
@property (nonatomic, assign) BOOL bDoneStartOrStop;
@property (nonatomic, strong) NSMutableArray *headingArray;
@property (nonatomic, strong) NSNumber *heading;


- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error;

@end
//
//  Coverage.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "Coverage.h"
#import "Area.h"
#import "Rider.h"


@implementation Coverage

@dynamic account;
@dynamic rider;
@dynamic area;

@end

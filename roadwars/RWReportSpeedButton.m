//
//  RWReportSpeedButton.m
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//

#import "RWReportSpeedButton.h"
#import "Constants.h"

@implementation RWReportSpeedButton

- (void)awakeFromNib {
    // Initialization code
    self.contentMode = UIViewContentModeCenter;
    [self setBackgroundImage:[UIImage imageNamed:@"speedlimit-blank.png"] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont fontWithName:kFontNameSansBold size:50.0];
}

@end

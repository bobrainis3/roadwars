//
//  RWTellFriendsViewController.h
//  roadwars
//
//  Created by Bob Rainis.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTableViewController.h"


@interface RWTellFriendsViewController : RWTableViewController
@property (nonatomic, weak) IBOutlet UIButton *connectButton;
@property (nonatomic, strong) NSMutableArray *friendsArray;

@property (strong, nonatomic) NSMutableArray* allTableData;
@property (strong, nonatomic) NSMutableDictionary* filteredTableData;
@property (strong, nonatomic) NSArray* letters;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, assign) bool isFiltered;

- (IBAction)doDisconnectFacebook;
- (void)onFacebookFriendsInvite:(NSNotification *)notif;

@end

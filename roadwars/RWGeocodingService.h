//
//  RWGeocodingService.m
//  roadwars
//
//  Created by Bob Rainis on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
@class Reachability;
@interface RWGeocodingService : NSObject 
@property (nonatomic, strong) CLGeocoder * geoCoder;
@property (nonatomic, strong) Reachability *mqReach; // reverse geocoder from MQ

- (void)onNetworkChange:(NSNotification *)notif;
-(void) reverseGeocode: (CLLocation *)updatedLocation;
-(void) fetchedMQData:(NSData *)responseData;
@end
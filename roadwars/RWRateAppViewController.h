//
//  RWRateAppViewController.h
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWViewController.h"
#import <StoreKit/StoreKit.h>

@interface RWRateAppViewController : RWViewController <SKStoreProductViewControllerDelegate>
@property (nonatomic, weak) IBOutlet UIWebView *rateUsWebView ;

@end

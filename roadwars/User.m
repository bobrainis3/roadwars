//
//  User.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic fbId;
@dynamic first;
@dynamic gender;
@dynamic last;

@end

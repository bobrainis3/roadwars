//
//  RWShinyButton.m
//  RW
//
//  Created by Christian Allen on 9/26/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWShinyButton.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation RWShinyButton

- (void)awakeFromNib {

    self.contentMode = UIViewContentModeScaleAspectFill;
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[[self layer] setBorderWidth:2.0f];
    //[[self layer] setBorderColor:kColorGrungeButtonTopBorder.CGColor];
    [self layer].cornerRadius = 8.0f;
    [self layer].masksToBounds = YES;
    [self layer].borderWidth = 0.0f;
    self.titleLabel.font = [UIFont fontWithName:kFontNameGrunge size:20.0];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)kColorGold.CGColor, (id)kColorGold.CGColor, nil];
    [self.layer insertSublayer:gradient atIndex:0];
    
}

@end

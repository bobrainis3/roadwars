//
//  RWSettingsViewController.h
//  roadwars

#import "RWTableViewController.h"
#import <StoreKit/StoreKit.h>


@interface RWSettingsViewController : RWTableViewController <SKStoreProductViewControllerDelegate>
@property (nonatomic, strong) MPMoviePlayerViewController *player;
- (IBAction)doReviews;

@end

//
//  RWTableViewController.h
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "EGORefreshTableHeaderView.h"
#import "RWViewController.h"

@class RWTableViewCell;

@interface RWTableViewController : RWViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, NSFetchedResultsControllerDelegate, EGORefreshTableHeaderDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *placeholderView;
@property (nonatomic, strong) NSFetchedResultsController *tableData;
@property (nonatomic, assign) BOOL hasRefresh;
@property (nonatomic, assign) BOOL isRefreshing;
@property (nonatomic, strong) EGORefreshTableHeaderView *refreshView;
@property (nonatomic, strong) NSDate *lastRefresh;

- (RWTableViewCell *)dequeueOrNew:(NSString *)nibName;
- (void)configureCell:(RWTableViewCell *)cell withObject:(NSObject *)obj;
- (void)initializeData;
- (void)refreshTable;
- (void)doRefreshDataFromServer;

@end

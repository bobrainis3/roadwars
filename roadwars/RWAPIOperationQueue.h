//
//  RWAPIOperationQueue.h
//
//  Created by Christian Allen on 7/10/12.
//  Copyright (c) 2012 dssDigital Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Reachability;
@class FBSession;

@interface RWAPIOperationQueue : NSOperationQueue <NSCoding>

@property (nonatomic, strong) NSDate *lastCall;
@property (nonatomic, strong) Reachability *apiReach;

- (void)onNetworkChange:(NSNotification *)notif;
- (void)onFacebookFriendsUpdate:(NSNotification *)notif;
- (void)onUserAccountUpdate:(NSNotification *)notif;
- (void)onFacebookMeRequestComplete:(NSNotification *)notif;
- (void)onFacebookLogin:(NSNotification *)notif;
@end

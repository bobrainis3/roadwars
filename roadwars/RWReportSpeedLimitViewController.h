//
//  RWReportSpeedLimitViewController
//  RW
//
//  Created by Bob Rainis on 11/8/11.
//  Copyright (c) 2011 bladenet, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWViewController.h"

@interface RWReportSpeedLimitViewController : RWViewController 

@property (nonatomic, weak) IBOutlet UIButton *b5;
@property (nonatomic, weak) IBOutlet UIButton *b10;
@property (nonatomic, weak) IBOutlet UIButton *b15;
@property (nonatomic, weak) IBOutlet UIButton *b20;
@property (nonatomic, weak) IBOutlet UIButton *b25;
@property (nonatomic, weak) IBOutlet UIButton *b30;
@property (nonatomic, weak) IBOutlet UIButton *b35;
@property (nonatomic, weak) IBOutlet UIButton *b40;
@property (nonatomic, weak) IBOutlet UIButton *b45;
@property (nonatomic, weak) IBOutlet UIButton *b50;
@property (nonatomic, weak) IBOutlet UIButton *b55;
@property (nonatomic, weak) IBOutlet UIButton *b60;
@property (nonatomic, weak) IBOutlet UIButton *b65;
@property (nonatomic, weak) IBOutlet UIButton *b70;
@property (nonatomic, weak) IBOutlet UIButton *b75;
@property (nonatomic, weak) IBOutlet UIButton *b80;

@property (nonatomic, weak) IBOutlet UIButton *buttonSelected;;
@property (nonatomic, strong) NSNumber *postedSpeedLimit;;
@property (nonatomic, strong) NSString *roadName;
@property (nonatomic, weak) IBOutlet UILabel *roadNameLabel;
@property (nonatomic, strong) NSString *townName;
@property (nonatomic, weak) IBOutlet UILabel *townNameLabel;
@property (nonatomic, strong) NSTimer *speedReportTimer;


-(IBAction) toggleDown:(id)sender ;


@end

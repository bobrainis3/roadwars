//
//  RWContext.h
//  roadwars
//
//  Created by Christian Allen on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.

#define kModelUser @"User"
#define kModelRider @"Rider"
#define kModelCurrentUser @"CurrentUser"
#define kModelSpeedLimitsReported @"SpeedLimitsReported"
#define kModeluserDailyStat @"UserDailyStat"

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CurrentUser;
@class RWHitlist;
@class SpeedLimitsReported;

typedef enum {
    RWResultsSortDefault,
    RWResultsSortAlpha,
    RWResultsSortDailyTake,
    RWResultsSortCount
} RWResultsSort;

@interface RWContext : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) CurrentUser *me;
@property (nonatomic, strong) RWHitlist *hitlist;
@property (nonatomic, strong) NSMutableArray *skinny;


+ (NSManagedObjectContext *)managedObjectContext;
+ (NSManagedObject *)newObject:(NSString *)className;
+ (NSEntityDescription *)entityForName:(NSString *)className;
+ (void)deleteObject:(NSManagedObject *)obj;
+ (void)commit;
+ (void)loadMockData;
+ (RWHitlist *)hitlistForDelegate:(id<NSFetchedResultsControllerDelegate>)delegate;
+ (CurrentUser *)me;
+ (NSMutableArray *)skinny;
+ (BOOL)isFbConnected;
+ (void)setCurrentUserFbId:(NSString *)fbId name:(NSString *)name first:(NSString *)first last:(NSString *)last gender:(NSString *)gender email:(NSString *)email;
+ (void)reloadHitlist:(NSArray *)hitlistUsers;
+ (NSManagedObject *)getEntityOfType:(NSString *)entityName where:(NSPredicate *)predicate;
+ (NSArray *)getEntityOfTypeReturningArray:(NSString *)entityName where:(NSPredicate *)predicate ;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (void) addSkinnyMessage: (NSString *)skinnyMessage appendTime:(BOOL)appendTime;

@end

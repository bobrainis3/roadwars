//
//  RWInitializeOperation.h
//  RW
//
//  Created by Christian Allen on 9/25/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWAPIOperation.h"

@interface RWInitializeOperation : RWAPIOperation

@end

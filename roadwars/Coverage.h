//
//  Coverage.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Area, Rider;

@interface Coverage : NSManagedObject

@property (nonatomic, retain) NSNumber * account;
@property (nonatomic, retain) Rider *rider;
@property (nonatomic, retain) Area *area;

@end

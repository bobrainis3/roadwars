//
//  RWFollowUsTwitterViewController
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWFollowUsTwitterViewController.h"
#import "Constants.h"

@interface RWFollowUsTwitterViewController ()

@end

@implementation RWFollowUsTwitterViewController

@synthesize followUsWebView=_followUsWebView;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSString *url = [serverParameters objectForKey:@"twitterUrl"];
    
    NSLog(@"do twitter <%@>", url);

      
    [self.followUsWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

@end

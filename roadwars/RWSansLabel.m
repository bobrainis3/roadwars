//
//  RWSansLabel.m
//  RW
//
//  Created by Christian Allen on 9/18/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWSansLabel.h"
#import "Constants.h"

@implementation RWSansLabel

- (void)awakeFromNib {  // CA: simply allows us to use the PT Sans font in interface builder
    [super awakeFromNib];
    self.font = [UIFont fontWithName:kFontNameSans size:self.font.pointSize];
}

@end

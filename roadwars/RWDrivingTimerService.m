//
//  RWDrivingTimerService.m
//  roadwars
//
//  Created by Bob Rainis on 11/16/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWDrivingTimerService.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"

@implementation RWDrivingTimerService
@synthesize delegate;
@synthesize running;
@synthesize seconds_elapsed;
@synthesize start_date;

-(id) init {
	self = [super init];
    [self addObserver:kOnMotionStart withSelector:@selector(onMotionStart:)];
    [self addObserver:kOnMotionStop withSelector:@selector(onMotionStop:)];
    
	running = FALSE;
	seconds_elapsed = 0;
    delegate=self;
	return self;
}
- (void)dealloc { // best place to unload notifications, release any retained objects, etc
    
    [self removeObserver:kOnMotionStart];
    [self removeObserver:kOnMotionStop];
}
- (void)onMotionStart:(NSNotification *)notif {
    //NSLog(@"%@:onMotionStart-driving - turn on driving clock", self.class);
    [self resetDrivingTimer];
    [self start];
    NSLog(@"second elapsed <%d>", seconds_elapsed);
    
}


- (void)onMotionStop:(NSNotification *)notif {
   // NSLog(@"%@:onMotionStop-driving - turn off driving clock", self.class);
    [self stop];
    //NSLog(@"currentElapsedSeconds <%d>", self.currentElapsedSeconds);
    //NSLog(@"second elapsed <%d>", self.seconds);
    //NSLog(@"minutes elapsed <%d>", self.minutes);
    //NSLog(@"hours elapsed <%d>", self.hours);
   // NSLog(@"current driving minutes <%d>", self.currentElapsedSeconds);
    NSInteger weeklyDrivingTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"weeklyDrivingTime"];
    NSInteger todayDrivingTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"todayDrivingTime"];
   // NSLog (@" before weeklyDrivingTime <%d> todayDrivingTime <%d>", weeklyDrivingTime, todayDrivingTime);
    [[NSUserDefaults standardUserDefaults] setInteger: todayDrivingTime+self.minutes forKey:@"todayDrivingTime"];
    [[NSUserDefaults standardUserDefaults] setInteger: weeklyDrivingTime+self.minutes forKey:@"weeklyDrivingTime"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    weeklyDrivingTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"weeklyDrivingTime"];
    todayDrivingTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"todayDrivingTime"];
   // NSLog (@" after weeklyDrivingTime <%d> todayDrivingTime <%d>", weeklyDrivingTime, todayDrivingTime);
    
}

-(void) start {
	[self startAt:[NSDate date]];
}

-(void) startAt:(NSDate *)s_date {
	self.start_date = s_date;
	update_interval = [NSTimer timerWithTimeInterval:(NSTimeInterval)1.0 target:self selector:NSSelectorFromString(@"handleUpdateInterval:") userInfo:nil repeats:YES];
	[[NSRunLoop mainRunLoop] addTimer:update_interval forMode:NSDefaultRunLoopMode];
	running = TRUE;
	[delegate timerDidUpdate:self];
}

-(void) stop {
	seconds_elapsed += [self currentElapsedSeconds];
	self.start_date = nil;
	[update_interval invalidate];
	update_interval = nil;
	[delegate timerDidUpdate:self];
	running = FALSE;
    
}

-(NSInteger) currentElapsedSeconds {
	if(start_date != nil){
		return [[NSNumber numberWithDouble:[start_date timeIntervalSinceNow]] integerValue] * -1;
	} else {
		return 0;
	}
}

-(void) handleUpdateInterval:(NSTimer *)update_interval {
	[delegate timerDidUpdate:self];
  
}

-(NSInteger) hours {
	return (seconds_elapsed + [self currentElapsedSeconds]) / 3600;
}

-(NSInteger) minutes {
	return ((seconds_elapsed + [self currentElapsedSeconds]) / 60) % 60;
}

-(NSInteger) seconds {
	return (seconds_elapsed + [self currentElapsedSeconds]) % 60;
}

-(void) setHours:(NSInteger)new_hours{
	seconds_elapsed = seconds_elapsed - ([self hours] * 3600) + (new_hours * 3600);
	[delegate timerDidUpdate:self];
}

-(void) setMinutes:(NSInteger)new_minutes{
	seconds_elapsed = seconds_elapsed - ([self minutes] * 60) + (new_minutes * 60);
	[delegate timerDidUpdate:self];
}

-(void) setSeconds:(NSInteger)new_seconds{
	seconds_elapsed = seconds_elapsed - [self seconds] + new_seconds;
	[delegate timerDidUpdate:self];
}


-(void) resetDrivingTimer {
	seconds_elapsed = 0;
  	[delegate timerDidUpdate:self];
}


-(void) setByTimeInterval:(NSTimeInterval)interval {
	self.seconds_elapsed = [[NSNumber numberWithDouble:interval] integerValue];
	[delegate timerDidUpdate:self];
}

-(NSString *) stringHours{
	return [NSString stringWithFormat:@"%i",[self hours]];
}

-(NSString *) stringMinutes{
	return [self paddedIntString:[self minutes] toLength:2];
}

-(NSString *) stringSeconds{
	return [self paddedIntString:[self seconds] toLength:2];
}

-(NSString *) paddedIntString:(NSInteger)value toLength:(NSInteger)length {
	NSString *output;
	output = [NSString stringWithFormat:@"%i", value];
	while([output length] < length){
		output = [@"0" stringByAppendingString:output];
	}
	
	return output;
}

- (void)timerDidUpdate:(RWDrivingTimerService *)drivingTimer {
  //  NSLog(@"Driving timer did update");
}

@end

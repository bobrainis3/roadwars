//
//  RWSansTextView.m
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWSansTextView.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation RWSansTextView

- (void)awakeFromNib {  // CA: simply allows us to use the PT Sans font in interface builder
    [super awakeFromNib];
    
    self.font = [UIFont fontWithName:kFontNameSans size:self.font.pointSize];

    /*
    CALayer *layer = self.layer;
    layer.cornerRadius = 8.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 1.0f;
    layer.borderColor = [UIColor grayColor].CGColor;
     */
}

@end

//
//  userDailyStat.h
//  roadwars
//
//  Created by Bob Rainis on 4/22/13.
//  Copyright (c) 2013 BLADE LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface userDailyStat : NSManagedObject

@property (nonatomic, retain) NSNumber * coinUpDriving;
@property (nonatomic, retain) NSNumber * coinDownDriving;
@property (nonatomic, retain) NSNumber * coinUpTolls;
@property (nonatomic, retain) NSNumber * coinDownTolls;
@property (nonatomic, retain) NSNumber * roadsUpDriving;
@property (nonatomic, retain) NSNumber * roadsDownDriving;
@property (nonatomic, retain) NSNumber * roadsUpAttack;
@property (nonatomic, retain) NSNumber * roadsDownAttack;
@property (nonatomic, retain) NSNumber * driveTime;
@property (nonatomic, retain) NSNumber * totalAttack;
@property (nonatomic, retain) User *user;

@end

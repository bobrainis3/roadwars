//
//  RWOptionsViewController.m
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWOptionsViewController.h"
#import "Constants.h"

@interface RWOptionsViewController ()

@end

@implementation RWOptionsViewController

@synthesize optionAudio = _optionAudio;

- (IBAction)doOptionAudio:(id)sender {
    NSLog(@"%@:doOptionAudio - clicked", self.class);
}

- (IBAction)doSoundBoard:(id)sender {
    [self performSegueWithIdentifier:kSegueOptionsToSoundBoard sender:nil];
}

@end

//
//  UIView+UIView_.m
//  didgeridoo
//
//  Created by Doug Scandrett on 10/24/11.
//  Copyright (c) 2011 FleetingLook, Inc. All rights reserved.
//

#import "UIView+UIView_.h"

@implementation UIView (UIView_)

+ (UIView *)uiViewFromNibName:(NSString *)name owner:(id)anOwner {
    
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:name 
                                                      owner:anOwner
                                                    options:nil];
    
    UIView* myView = (UIView *)[nibViews objectAtIndex:0];
    
    return myView;
}

+ (UITableViewCell *)uiTableViewCellFromNibName:(NSString *)name owner:(id)anOwner {
    return (UITableViewCell *)[UIView uiViewFromNibName:name owner:anOwner];
}

@end

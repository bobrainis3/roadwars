//
//  RWAudioService.m
//  roadwars
//
//  Created by Bob Rainis on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "RWAudioService.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"

@implementation RWAudioService
@synthesize sfxSoundsDictionary = _sfxSoundsDictionary;
@synthesize audioPlayer = _audioPlayer;
@synthesize bCanPlaySound = _bCanPlaySound;
@synthesize lastSoundEvent = _lastSoundEvent;

- (id) init {
	self = [super init];
	if (self != nil) {
        [[AVAudioSession sharedInstance] setCategory:@"AVAudioSessionCategoryAmbient" error:nil];
        
        /* need to first figure out if bluetooth active
        [[AVAudioSession sharedInstance] setCategory:@"AVAudioSessionCategoryPlayAndRecord" error:nil];
      
        UInt32 allowBluetoothInput = 1;
        AudioSessionSetProperty(
                                kAudioSessionProperty_OverrideCategoryEnableBluetoothInput,
                                sizeof (allowBluetoothInput),
                                &allowBluetoothInput);
         */
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        _sfxSoundsDictionary = [prefs objectForKey:@"sfxSoundsDictionary"];
  //      if (self.sfxSoundsDictionary == nil) {
            self.sfxSoundsDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        kSoundCoinBig1 ,kSoundEventBigBonus,
                                        kSoundCoinSmall1,kSoundEventSmallBonus ,
                                        kSoundBusted1, kSoundEventBusted,
                                        kSoundDanger1,kSoundEventDanger,
                                        kSoundTickDown ,kSoundEventSpeedLimitDown,
                                        kSoundTickUp,kSoundEventSpeedLimitUp,
                                        kSoundTicLight1,kSoundEventPaulComfortingTic,
                                        kSoundSwerve1, kSoundEventLoseBonusTime,
                                        kSoundCaptured2,kSoundEventRoadCapturedFromFriend,
                                        kSoundCaptured1,kSoundEventRoadCaptured,
                                        nil
                                        ];
            [prefs setObject:self.sfxSoundsDictionary forKey:@"sfxSoundsDictionary"];
            [prefs synchronize];
        }
	//}
    self.bCanPlaySound=YES; // in game, only play every N seconds
    self.lastSoundEvent=@"";
	return self;
}

- (void)dealloc {
    self.sfxSoundsDictionary=nil;
    self.lastSoundEvent=nil;
}

- (void) playSoundEvent:(NSString *)soundEvent :(NSInteger)fromWhere {
    BOOL bCallAllowAudio=YES; // if in a donotplayaudio loop, and new sound comes in
    // if a new sound, forget about time interval...maybe
    if (![self.lastSoundEvent isEqualToString:soundEvent]){
        //NSLog(@"audio different");
        self.bCanPlaySound=YES;
    }
    if (self.bCanPlaySound==NO)
        bCallAllowAudio=NO;
    
    self.lastSoundEvent=soundEvent;
    if (self.bCanPlaySound || fromWhere>=0){ // from soundboard
        NSLog (@"playing sound %d", self.bCanPlaySound);
        self.bCanPlaySound=NO;
        [self playSound:[self.sfxSoundsDictionary objectForKey:soundEvent]];
        if (fromWhere<0){ // from game
            if (bCallAllowAudio){
                NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
                NSNumber *audioInterval = [serverParameters objectForKey:@"audioInterval"];
                //NSLog(@"server parameters <%@>", serverParameters);
                //NSLog (@"going to call doAllowAudio in %d seconds", audioInterval.intValue);
                [self performSelector:@selector(doAllowAudio) withObject:nil afterDelay:audioInterval.intValue];
            }
        }
        else // soundboard
            self.bCanPlaySound=YES;
    }
}
- (void) doAllowAudio{
    //NSLog(@"doAllowAudio");
    self.bCanPlaySound=YES;
}

- (void) playSound:(NSString *)sound {
    if ( ![kSoundMute isEqualToString:sound] ) {
        NSURL *fileURL = nil;
        if ( [sound rangeOfString:@".m4a"].location != NSNotFound ) {
            NSString *soundWithoutExt = [sound stringByReplacingOccurrencesOfString:@".m4a" withString:@""];
            fileURL = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:soundWithoutExt ofType: @"m4a"]];
        } else {
            fileURL = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:sound ofType: @"mp3"]];
            
        }
       // NSLog (@"fileUrl <%@>",fileURL);
        
        NSError *error = nil;
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
        if ( error == nil ) {
            self.audioPlayer.numberOfLoops = 0;
            self.audioPlayer.volume = 1.0;
            [self.audioPlayer prepareToPlay];
            [self.audioPlayer play];
            self.audioPlayer.delegate=self;
            self.bCanPlaySound=NO;
        } else {
            NSLog(@"%@:playSFXSound - error while trying to play %@: %@", self.class, sound, error.localizedDescription);
        }
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player
                       successfully:(BOOL)flag {
    
//    NSLog (@"Finished playing sound  %d",flag);
//    [self doAllowAudio];
    
}
- (void) updateSound:(NSString *)sound forSoundEvent:(NSString *)soundEvent {
    [self.sfxSoundsDictionary setObject:sound forKey:soundEvent];
    [[NSUserDefaults standardUserDefaults] setObject:self.sfxSoundsDictionary forKey:@"sfxSoundsDictionary"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end

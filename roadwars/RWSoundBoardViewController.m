//
//  RWSoundBoardViewController.m
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWSoundBoardViewController.h"
#import "RWAudioService.h"
#import "RWServices.h"
#import "RWTableViewCellSoundBoard.h"
#import "Constants.h"

typedef enum {
    RWSoundBoardOptionA,
    RWSoundBoardOptionB,
    RWSoundBoardOptionC,
    RWSoundBoardOptionMute
} RWSoundBoardOption;

@interface RWSoundBoardViewController ()

@property (nonatomic, strong) NSArray *sfxKeysAlphabetical;
@property (nonatomic, strong) NSDictionary *sfxOptions;

@end

@implementation RWSoundBoardViewController

@synthesize sfxOptions = _sfxOptions;

- (id)initWithCoder:(NSCoder *)decoder {
    if ( self = [super initWithCoder:decoder] ) {
        _sfxKeysAlphabetical = [[[[RWServices audioService] sfxSoundsDictionary] allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        _sfxOptions = [NSDictionary dictionaryWithObjectsAndKeys:
                       [NSArray arrayWithObjects:kSoundCoinBig1, kSoundCoinBig2, kSoundCoinSmall1, nil], kSoundEventBigBonus,
                       [NSArray arrayWithObjects:kSoundCoinSmall1, kSoundCoinBig1, kSoundCoinBig2, nil], kSoundEventSmallBonus,
                       [NSArray arrayWithObjects:kSoundTickUp, kSoundTicLight1, kSoundTicLight2, nil], kSoundEventSpeedLimitUp,
                       [NSArray arrayWithObjects:kSoundTickDown, kSoundTicHeavy1, kSoundTicLight2, nil], kSoundEventSpeedLimitDown,
                       [NSArray arrayWithObjects:kSoundBusted1, kSoundBusted2, kSoundBusted3, nil], kSoundEventBusted,
                       [NSArray arrayWithObjects:kSoundDanger1, kSoundDanger2, kSoundDanger3, nil], kSoundEventDanger,
                       [NSArray arrayWithObjects:kSoundTicLight1, kSoundTicLight2, kSoundTicHeavy1, nil], kSoundEventPaulComfortingTic,
                       [NSArray arrayWithObjects:kSoundSwerve1,kSoundBusted2, kSoundBusted3, nil], kSoundEventLoseBonusTime,
                       [NSArray arrayWithObjects:kSoundCaptured2, kSoundCaptured1, nil], kSoundEventRoadCapturedFromFriend,
                       [NSArray arrayWithObjects:kSoundCaptured1, kSoundCaptured2, nil], kSoundEventRoadCaptured,
                       nil];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //NSLog(@"self.sfxKeysAlphabetical.count <%d>", self.sfxKeysAlphabetical.count);
    return self.sfxKeysAlphabetical.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"indexPath <%@>",indexPath);
    
    RWTableViewCellSoundBoard *cell = (RWTableViewCellSoundBoard *)[self.tableView dequeueReusableCellWithIdentifier:kTableCellSoundBoard];
    if ( cell == nil ) {
        cell = [[RWTableViewCellSoundBoard alloc] initWithNibName:kTableCellSoundBoard];
        [cell.muteButton addTarget:self action:@selector(doSoundUpdate:) forControlEvents:UIControlEventTouchUpInside];
        [cell.aButton addTarget:self action:@selector(doSoundUpdate:) forControlEvents:UIControlEventTouchUpInside];
        [cell.bButton addTarget:self action:@selector(doSoundUpdate:) forControlEvents:UIControlEventTouchUpInside];
        [cell.cButton addTarget:self action:@selector(doSoundUpdate:) forControlEvents:UIControlEventTouchUpInside];
        cell.muteButton.tag = RWSoundBoardOptionMute;
        cell.aButton.tag = RWSoundBoardOptionA;
        cell.bButton.tag = RWSoundBoardOptionB;
        cell.cButton.tag = RWSoundBoardOptionC;
      
    }

    [cell setBackgroundImageForRow:indexPath.row ofTotalRows:self.sfxKeysAlphabetical.count];
   // NSLog(@"indexPath.row <%d>", indexPath.row);
    
    [self configureCell:cell atIndexPath:indexPath withObject:[self.sfxKeysAlphabetical objectAtIndex:indexPath.row]];
    
    if ( cell == nil ) NSLog(@"RWHitlistTableViewController::cellForRow... - returning a null cell :(");
    return cell;
}

- (void)configureCell:(RWTableViewCellSoundBoard *)cell atIndexPath:(NSIndexPath *)indexPath withObject:(NSObject *)obj {
    NSString *soundEvent = (NSString *)obj;
    cell.titleLabel.text = soundEvent;
    cell.tag = indexPath.row; // hack - easier way to reconstruct row later (see below)
    //NSLog (@"cell.tag <%d>",cell.tag);
    
    cell.muteButton.alpha = cell.aButton.alpha = cell.bButton.alpha = cell.cButton.alpha = 0.2;
    cell.aButton.titleLabel.textColor = cell.bButton.titleLabel.textColor = cell.cButton.titleLabel.textColor = [UIColor darkGrayColor];
    
    NSString *sound = [[[RWServices audioService] sfxSoundsDictionary] objectForKey:soundEvent];
    //NSLog(@"Sound should be %@ for row %d", sound, indexPath.row);
    NSArray *soundOptions = [self.sfxOptions objectForKey:soundEvent];

    cell.bButton.hidden = (soundOptions.count < 2);
    cell.cButton.hidden = (soundOptions.count < 3);

    if ( [kSoundMute isEqualToString:sound] ) {
        cell.muteButton.alpha = 1.0;
    } else if ( [sound isEqualToString:[soundOptions objectAtIndex:RWSoundBoardOptionA]] ) {
        cell.aButton.alpha = 1.0;
        cell.aButton.titleLabel.textColor = kColorGreen;
    } else if ( [sound isEqualToString:[soundOptions objectAtIndex:RWSoundBoardOptionB]] ) {
        cell.bButton.alpha = 1.0;
        cell.bButton.titleLabel.textColor = kColorGreen;
    } else if ( [sound isEqualToString:[soundOptions objectAtIndex:RWSoundBoardOptionC]] ) {
        cell.cButton.alpha = 1.0;
        cell.cButton.titleLabel.textColor = kColorGreen;
    }
}

- (IBAction)doSoundUpdate:(id)sender {
    UIButton *btn = (UIButton *)sender;
   
    NSIndexPath *indexPathReconstructed;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        indexPathReconstructed = [NSIndexPath indexPathForRow:btn.superview.superview.superview.tag inSection:0]; // lazy
    }else
        indexPathReconstructed = [NSIndexPath indexPathForRow:btn.superview.superview.tag inSection:0]; // lazy
    
    RWSoundBoardOption option = btn.tag;
    NSString *soundEvent = [self.sfxKeysAlphabetical objectAtIndex:indexPathReconstructed.row];
    if ( option == RWSoundBoardOptionMute ) {
        [[RWServices audioService] updateSound:kSoundMute forSoundEvent:soundEvent];
    } else {
        [[RWServices audioService] updateSound:[[self.sfxOptions objectForKey:soundEvent] objectAtIndex:option] forSoundEvent:soundEvent];
    }
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathReconstructed] withRowAnimation:UITableViewRowAnimationFade];
    [[RWServices audioService] playSoundEvent:soundEvent:btn.tag];
}

@end

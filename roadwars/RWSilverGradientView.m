//
//  RWSilverGradientView.m
//  RW
//
//  Created by Christian Allen on 10/31/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWSilverGradientView.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation RWSilverGradientView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CALayer *layer = self.layer;
    layer.cornerRadius = 9.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 0.0f;
    layer.borderColor = kColorSilverGradientBorder.CGColor;

    CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.layer.bounds;
    _gradientLayer.colors = [NSArray arrayWithObjects:
                             (id)kColorSilverGradientTop.CGColor,
                             (id)kColorSilverGradientBottom.CGColor,
                             nil];
    _gradientLayer.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
    [self.layer insertSublayer:_gradientLayer atIndex:0];
    
    // BR: Use this view behind buttons, so wanted rounded edges
    // ask Christian if I should make a new/different view.
    self.layer.cornerRadius = 8.0f;
    _gradientLayer.cornerRadius = 8.0f;
}

@end

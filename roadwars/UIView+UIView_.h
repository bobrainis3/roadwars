//
//  UIView+UIView_.h
//  didgeridoo
//
//  Created by Doug Scandrett on 10/24/11.
//  Copyright (c) 2011 FleetingLook, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIView_)

+ (UIView *)uiViewFromNibName:(NSString *)name owner:(id)anOwner;
+ (UITableViewCell *)uiTableViewCellFromNibName:(NSString *)name owner:(id)anOwner;

@end

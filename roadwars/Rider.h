//
//  Rider.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Area, Coverage, userDailyStat;

@interface Rider : NSManagedObject

@property (nonatomic, retain) NSNumber * coin;
@property (nonatomic, retain) NSNumber * dailyTake;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSDate * lastUpdated;
@property (nonatomic, retain) NSNumber * lifetimeCoin;
@property (nonatomic, retain) NSNumber * lifetimeRoads;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * roads;
@property (nonatomic, retain) NSSet *coverages;
@property (nonatomic, retain) NSSet *ownerships;
@property (nonatomic, retain) NSSet *runnerUpships;
@property (nonatomic, retain) userDailyStat *userDailyStat;

@end

@interface Rider (CoreDataGeneratedAccessors)

- (void)addCoveragesObject:(Coverage *)value;
- (void)removeCoveragesObject:(Coverage *)value;
- (void)addCoverages:(NSSet *)values;
- (void)removeCoverages:(NSSet *)values;

- (void)addOwnershipsObject:(Area *)value;
- (void)removeOwnershipsObject:(Area *)value;
- (void)addOwnerships:(NSSet *)values;
- (void)removeOwnerships:(NSSet *)values;

- (void)addRunnerUpshipsObject:(Area *)value;
- (void)removeRunnerUpshipsObject:(Area *)value;
- (void)addRunnerUpships:(NSSet *)values;
- (void)removeRunnerUpships:(NSSet *)values;

@end

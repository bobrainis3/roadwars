//
//  NSObject+ObserverShortcuts.m
//  roadwars
//
//  Created by Christian Allen on 11/6/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "NSObject+ObserverShortcuts.h"

@implementation NSObject (ObserverShortcuts)

-(void)addObserver:(NSString *)notificationName withSelector:(SEL)selector {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:selector name:notificationName object:nil];
}

-(void)removeObserver:(NSString *)notificationName {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notificationName object:nil];
}

-(void)postNotification:(NSString *)notificationName withObject:(NSObject *)obj {
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:obj];
}

@end

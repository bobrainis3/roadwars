//
//  RWFeedbackViewController.m
//  roadwars
//
//  Created by Christian Allen on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWFeedbackViewController.h"
#import "RWAPI.h"
#import <sys/utsname.h>

@interface RWFeedbackViewController ()

@end

@implementation RWFeedbackViewController

@synthesize feedbackComment = _feedbackComment;
@synthesize feedbackRating = _feedbackRating;
@synthesize useEmailView = _useEmailView;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    /*
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
     */
    return YES;
}

- (IBAction)doSubmitFeedback:(id)sender {
    //NSLog(@"%@:doSubmitFeedback - sending...", self.class);
    
    NSString *feedback = [NSString stringWithFormat:@"%@ %@", self.feedbackComment.text, [[NSUserDefaults standardUserDefaults] objectForKey:@"emailAddress"]];
    [RWAPI userFeedback:@"Feedback" body:feedback  completion:^(NSDictionary *result) {
        //NSLog (@"result from doSubmitFeedback - %@", result);
        NSNumber *success=[result objectForKey:@"success"];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:(success.intValue==1)?@"Feedback":@"Feedback Not Sent"
                                                        message:[result objectForKey:@"message"]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:(success.intValue==1)?nil:@"Cancel", nil];
        [alert show];
    } failure:^(NSError *error) {
        // do anything on failure?
        NSLog (@"error from doSubmitFeedback - %@", error);
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		//NSLog(@"user pressed OK");
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else {
		//NSLog(@"user pressed Cancel");
	}
}

- (IBAction)doSendFeedbackEmail:(UIButton *)sender {
    
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Road Wars Feedback"];
        
        struct utsname u;
        uname(&u);
        NSString *nameString = [NSString stringWithFormat:@"%s", u.machine];
        
        NSString *s = [NSString stringWithFormat:@"%@\n\n%@\n%@\n%@\n Road Wars Version %@", @"", nameString, [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
        
        [controller setMessageBody:s isHTML:NO];
        
        NSArray *recipients = [NSArray arrayWithObjects:@"feedback@roadwars.com", nil];
        [controller setToRecipients:recipients];
        
        [self presentModalViewController:controller animated:YES];
        
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissModalViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
     [self resignFirstResponder];
        [super touchesEnded:touches withEvent:event];
}


@end

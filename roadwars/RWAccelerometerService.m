//
//  RWAccelerometerService.m
//  roadwars
//
//  Created by Bob Rainis on 11/14/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWAccelerometerService.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWContext.h"

// TODO, these are all server parameters
#define greenThreshold .1f
#define yellowThreshold .25f
#define redThreshold .35f
#define bustedThreshold .30

@implementation RWAccelerometerService
@synthesize motionManager = _motionManager;
@synthesize motionManagerQueue = _motionManagerQueue;
@synthesize lastSmoothV=_lastSmoothV;

- (id) init {
	self = [super init];
	if (self != nil) {
        self.motionManager = [[CMMotionManager alloc] init];
        [self.motionManager setAccelerometerUpdateInterval:0.01 ];// 100Hz BR:server paraqm
        self.motionManagerQueue = [[NSOperationQueue alloc]init];
     	}
    // check on Swerve then Acceleration
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    accelerometerThreshold = [serverParameters objectForKey:@"accelerometerThreshold"];
      if (accelerometerThreshold.doubleValue < .2)
        accelerometerThreshold = [NSNumber numberWithDouble:.4];
    //NSLog (@"accelerometerThreshold <%f>", accelerometerThreshold.doubleValue);
    numberTimesThinkPhoneMoving=0;


	return self;
}

- (void)dealloc {
    self.motionManager = nil;
    self.motionManagerQueue = nil;
}

- (void)startAccelerometerDetect{
    //NSLog (@"startAccelerometerDetect");
    activityThreshold=[NSNumber numberWithDouble:ACTIVITY_THRESHOLD]; 
    [self.motionManager startAccelerometerUpdatesToQueue:self.motionManagerQueue
                                             withHandler:^(CMAccelerometerData *data, NSError *error) {
                                                 [self updateAccelerometerData:data.acceleration];
                                                 
                                             }
     ];
}
- (void)stopAccelerometerDetect{
   // NSLog (@"stopAccelerometerDetect");
    [self.motionManager stopAccelerometerUpdates];
}


- (void)updateAccelerometerData:(CMAcceleration)acceleration{
    //NSLog(@"UpdateAccelerometerData");
    // NSLog (@"--->> X <%f> Y <%f> Z <%f>", acceleration.x, acceleration.y, acceleration.z);
    
    // accelerometer, get current reading and pass to accelerometer transformation and smoothing....
    Vector accelv;
	accelv.x=acceleration.x;
	accelv.y=acceleration.y;
	accelv.z=acceleration.z;
    
	[self addVec:accelv];
	[self calcStats];
    Vector smoothVlocal=self.lastSmoothV=smoothV;

    NSNumber * accelerometerActivity=[NSNumber numberWithInt:kAccelerometerActivityNone];
    
    if(timeout<=0 && (smoothVlocal.x!=0.0&&smoothVlocal.y!=0.0&&smoothVlocal.z!=0.0))	{
        if (smoothVlocal.z > 2 || smoothVlocal.z < -2) return; // throw out wacky results
        
        if (smoothVlocal.z > -0.60f){ // think the user is moving phone BR: server param
            NSLog (@"think the user is moving phone, don't count it for driving");
            NSLog (@"--->> X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z);
            //[[RWContext skinny]addObject:[NSString stringWithFormat:@"Debug - think moving phone X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z]];
            if (++numberTimesThinkPhoneMoving >= 4){ //BR:server param
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self postNotification:kOnAccelerometerDetectMovingPhone withObject:accelerometerActivity];
                });
            }
            
        } else { // check swerve then accel/braking
            numberTimesThinkPhoneMoving=0;
            //
            //-------------------Swerve
            // SWERVE LEFT
            if (smoothVlocal.x>greenThreshold) {
                // figure out how much over the threshold
                if(smoothVlocal.x>accelerometerThreshold.doubleValue){
                    accelerometerActivity=[NSNumber numberWithInt:kAccelerometerActivityLeftTurnBusted];
                    NSLog (@"Swerve Left threshold");
                    NSLog (@"accelerometerThreshold <%f>", accelerometerThreshold.doubleValue);
                    NSLog (@"--->> X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z);
                   // [[RWContext skinny]addObject:[NSString stringWithFormat:@"Debug - Swerve Left X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self postNotification:kOnAccelerometerIncident withObject:accelerometerActivity];
                    });
                }
            }
            //SWERVE RIGHT
            else if(smoothVlocal.x<-greenThreshold) {
                if(smoothVlocal.x<-accelerometerThreshold.doubleValue){
                    accelerometerActivity=[NSNumber numberWithInt:kAccelerometerActivityRightTurnBusted];
                    NSLog (@"Swerve Right threshold");
                    NSLog (@"accelerometerThreshold <%f>", accelerometerThreshold.doubleValue);
                    NSLog (@"--->> X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z);
                    //[[RWContext skinny]addObject:[NSString stringWithFormat:@"Debug - Swerve Right X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self postNotification:kOnAccelerometerIncident withObject:accelerometerActivity];
                    });
                }
            }
            
            // Now check on accel/decel
            
            if(smoothVlocal.y>greenThreshold){ // BRAKE - DECELERATE
                if(smoothVlocal.y>accelerometerThreshold.doubleValue){
                    accelerometerActivity= [NSNumber numberWithInt:kAccelerometerActivityBrakeBusted];
                    NSLog (@"Brake threshold");
                    NSLog (@"accelerometerThreshold <%f>", accelerometerThreshold.doubleValue);
                    NSLog (@"--->> X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z);
                   // [[RWContext skinny]addObject:[NSString stringWithFormat:@"Debug - Brake X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self postNotification:kOnAccelerometerIncident withObject:accelerometerActivity];
                    });
                }
            }
            else if(smoothVlocal.y<-greenThreshold){ // ACCELERATE
                if(smoothVlocal.y<-accelerometerThreshold.doubleValue){
                    accelerometerActivity=[NSNumber numberWithInt:kAccelerometerActivityAccelerateBusted];
                    NSLog (@"Accelerate threshold");
                    NSLog (@"accelerometerThreshold <%f>", accelerometerThreshold.doubleValue);
                    NSLog (@"--->> X <%f> Y <%f> Z <%f>", smoothVlocal.x, smoothVlocal.y, smoothVlocal.z);
                    //[[RWContext skinny]addObject:[NSString stringWithFormat:@"Debug - Accelerate X <%f> Y <%f> Z <%f>",smoothVlocal.x, smoothVlocal.y, smoothVlocal.z]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self postNotification:kOnAccelerometerIncident withObject:accelerometerActivity];
                    });
                }
            }
        }
        [self normalizeAccelerometer];
        
    }
    
}
- (void) normalizeAccelerometer {
    // Normalize accelerometer
    gravV=meanV;
    timeout=kSmoothTimeout;
    
}


#pragma mark - Accelerometer

-(void) addVec:(Vector) inputv {
	if(timeout>0)
		timeout--;
    
	xEHistory[windowIndex] = inputv.x;
	yEHistory[windowIndex] = inputv.y;
	zEHistory[windowIndex++] = inputv.z;
	
	if (windowIndex >= WINDOW_SIZE)
		windowIndex=0;
    
	//update gravity vector
	RotMat *rMat=[[RotMat alloc] initWithVec:gravV];
	rotV=[rMat operateOnVec:inputv];
	
	xSmooth[sWindowIndex] = rotV.x;
	ySmooth[sWindowIndex] = rotV.y;
	zSmooth[sWindowIndex++] = rotV.z;
	
	if (sWindowIndex >= SMOOTH_WINDOW_SIZE)
		sWindowIndex=0;
	
	smoothV.x=0;
	smoothV.y=0;
	smoothV.z=0;
	for(int kk=0;kk<SMOOTH_WINDOW_SIZE;kk++)
	{
		smoothV.x+=xSmooth[kk];
		smoothV.y+=ySmooth[kk];
		smoothV.z+=zSmooth[kk];
	}
	smoothV.x /= SMOOTH_WINDOW_SIZE;
	smoothV.y /= SMOOTH_WINDOW_SIZE;
	smoothV.z /= SMOOTH_WINDOW_SIZE;
    
}

-(void) calcStats {
	
    
	meanV.x=0.0;
	meanV.y=0.0;
	meanV.z=0.0;
	double varX=0.0, varY=0.0, varZ=0.0;
	
	for(int i=0; i < WINDOW_SIZE; i++)
	{
		meanV.x += xEHistory[i];
		meanV.y += yEHistory[i];
		meanV.z += zEHistory[i];
	}
	meanV.x /= WINDOW_SIZE;
	meanV.y /= WINDOW_SIZE;
	meanV.z /= WINDOW_SIZE;
	
	for(int i=0; i < WINDOW_SIZE; i++)
	{
		varX = (xEHistory[i] - meanV.x) * (xEHistory[i] - meanV.x);
		varY = (yEHistory[i] - meanV.y) * (yEHistory[i] - meanV.y);
		varZ = (zEHistory[i] - meanV.z) * (zEHistory[i] - meanV.z);
	}
	varX /= WINDOW_SIZE;
	varY /= WINDOW_SIZE;
	varZ /= WINDOW_SIZE;
	
  	//matlab says this is .0004 but someone thinks this is too easy...
	if(varX<[activityThreshold doubleValue] && varY<[activityThreshold doubleValue] && varZ<[activityThreshold doubleValue]) {
		if(timeout==1) { // No activity
            // NSLog (@"<%@>, Stats <X=%f><Y=%f><Z=%f>", @"No Activity", smoothV.x,smoothV.y, smoothV.z);
        }
        else if (timeout<=0 || timeout>kSmoothTimeout) { // Normalize
            // NSLog (@"Activity <%@>, Stats <X=%f><Y=%f><Z=%f>", @"Normalize", smoothV.x,smoothV.y, smoothV.z);
            gravV=meanV;
            timeout=5;
        }
	}
    
}

@end

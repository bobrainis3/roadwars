//
//  RWTopDarkGradientView.m
//  RW
//

#import "RWTopDarkGradientView.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation RWTopDarkGradientView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CALayer *layer = self.layer;
    layer.cornerRadius = 0.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 0.0f;
    layer.borderColor = kColorGold.CGColor;

    CAGradientLayer *_gradientLayer = [CAGradientLayer layer];
    _gradientLayer.frame = self.layer.bounds;
    _gradientLayer.colors = [NSArray arrayWithObjects:
                             (id)kColorHudBackgroundDark.CGColor,
                             (id)kColorHudBackgroundDark.CGColor,
                             (id)[UIColor clearColor].CGColor,
                             nil];
    _gradientLayer.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:.5f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
    [self.layer insertSublayer:_gradientLayer atIndex:0];
    
    self.layer.cornerRadius = 0.0f;
    _gradientLayer.cornerRadius = 0.0f;
    self.alpha=.8;
}

@end

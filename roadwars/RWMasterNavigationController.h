//
//  RWMasterNavigationController.h
//  roadwars
//
//  Created by Christian Allen on 11/6/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RWDriveViewController;

@interface RWMasterNavigationController : UITabBarController <UITabBarControllerDelegate>

@property (nonatomic, strong) IBOutlet UIView *settingsView;
@property (nonatomic, strong) IBOutlet RWDriveViewController *drivingViewController;
@property (nonatomic, strong) IBOutlet UIButton *leftButton;
@property (nonatomic, assign) CGRect windowBounds;
@property (nonatomic, strong) IBOutlet UITabBar *tabBar1;


- (void)onMotionStart:(NSNotification *)notif;
- (void)onMotionStop:(NSNotification *)notif;

//- (void)addSettingsToNavigationItem:(UINavigationItem *)navigationItem forController:(UIViewController *)controller;
//- (void)doShowSettings:(id)sender;
//- (IBAction)doHideSettings:(id)sender;
//- (IBAction)doAbout:(id)sender;
//- (IBAction)doOptions:(id)sender;
//- (IBAction)doDriveSimulation:(id)sender;
//- (IBAction)doFeedback:(id)sender;
//- (IBAction)doAttack:(id)sender;

@end

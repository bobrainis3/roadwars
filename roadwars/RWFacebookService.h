//
//  RWFacebookService.h
//  roadwars
//
//  Created by Christian Allen on 11/17/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Facebook;
@class FBSession;

@interface RWFacebookService : NSObject

@property (nonatomic, strong) Facebook *facebook;

- (void)openSession;
- (void)getMeAndFriends:(BOOL)shouldGetFriendsToo;
- (void)getFriends:(BOOL)forInvite;

@end

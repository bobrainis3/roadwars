//
//  RWBigButton.m
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//

#import "RWBigButton.h"
#import "Constants.h"

@implementation RWBigButton

- (void)awakeFromNib {
    // Initialization code
    self.contentMode = UIViewContentModeCenter;
    [self setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont fontWithName:kFontNameGrunge size:22.0];
}

@end

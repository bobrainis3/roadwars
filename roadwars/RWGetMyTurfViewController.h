//
//  RWGetMyTurfViewController.h
//  roadwars
//
//  Created by Bob Rainis.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWTableViewController.h"
#import "User.h"


@interface RWGetMyTurfViewController : RWTableViewController
@property (strong, nonatomic) NSArray* myTurf;
@property (nonatomic, strong) User *user;
- (IBAction)getAllTurf:(id)sender;
@end

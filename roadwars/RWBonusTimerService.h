//
//  RWBonusTimerService.h
//  roadwars
//
//  Created by Bob Rainis on 11/16/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
#import <UIKit/UIKit.h>


@interface RWBonusTimerService : NSObject {
	id delegate;
	bool running;
	NSInteger seconds_elapsed;
	NSDate *start_date;
	NSTimer *update_interval;
}
@property (nonatomic, strong) id delegate;
@property (nonatomic) bool running;
@property (nonatomic, assign) NSInteger seconds_elapsed;
@property (nonatomic, strong) NSDate *start_date;

-(NSInteger) hours;
-(NSInteger) minutes;
-(NSInteger) seconds;

-(void) setHours:(NSInteger)new_hours;
-(void) setMinutes:(NSInteger)new_minutes;
-(void) setSeconds:(NSInteger)new_seconds;

-(void) setByTimeInterval:(NSTimeInterval)interval;

-(NSString *) stringHours;
-(NSString *) stringMinutes;
-(NSString *) stringSeconds;

-(NSString *) paddedIntString:(NSInteger)value toLength:(NSInteger)length;

-(NSInteger) currentElapsedSeconds;
-(void) handleUpdateInterval:(NSTimer *)update_interval;
	

-(void) stop;
-(void) start;
-(void) startAt:(NSDate *)s_date;
-(void) resetBonusTimer;
-(void) dangerDeductionBonusTimer;
-(void) bustedDeductionBonusTimer;

@end

@protocol TimerDelegate

-(void) timerDidUpdate:(RWBonusTimerService *)timer;

@end


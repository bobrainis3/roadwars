//
//  RotMat.m
//  CarCharter
//
//  Created by lanthe on 4/4/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "RotMat.h"
#include <math.h>


@implementation RotMat

- (id) initWithVec:(Vector)vec
{
	thetax=-1*atan(-1*vec.x/vec.z);
	thetay=-1*atan(-1*vec.y/vec.z);
	return self;
}

- (Vector) operateOnVec:(Vector)vec
{
	Vector newvec;
	newvec.x=cos(thetax)*vec.x-sin(thetax)*vec.z;
	newvec.y=-sin(thetay)*sin(thetax)*vec.x+cos(thetay)*vec.y-sin(thetay)*cos(thetax)*vec.z;
	newvec.z=sin(thetax)*cos(thetay)*vec.x+sin(thetay)*vec.y+cos(thetay)*cos(thetax)*vec.z;
	return newvec;
}

@end

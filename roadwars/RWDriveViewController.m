//
//  RWDriveViewController.m
//  roadwars
//
//  Created by Bob Rainis on 11/8/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
#import <MediaPlayer/MediaPlayer.h>
#import <CoreLocation/CoreLocation.h>
#import <sys/utsname.h>

#import "RWDriveViewController.h"
#import "RWServices.h"
#import "RWAccelerometerService.h"
#import "RWLocationService.h"
#import "RWGeocodingService.h"
#import "RWBonusTimerService.h"
#import "RWAudioService.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "UIView+UIView_.h"
#import "RWContext.h"
#import "CurrentUser.h"
#import <FacebookSDK/FBProfilePictureView.h>
#import "RWAPI.h"
#import "DBFBProfilePictureView.h"
#import "RWAttackPlayViewController.h"
#import "RWReportSpeedLimitViewController.h"
#import "SpeedLimitsReported.h"
#import "RWSilverGradientView.h"
#import "userDailyStat.h"

#define soundFromGame -1
#define kInitializedEarnRate 1000

@interface RWDriveViewController ()

@end

@implementation RWDriveViewController

@synthesize speedLimitLabel = _speedLimitLabel;
@synthesize stateNameLabel = _stateNameLabel;
@synthesize roadNameLabel = _roadNameLabel;
@synthesize townStateNameLabel = _townStateNameLabel;
@synthesize townNameString = _townNameString, townNameHold = _townNameHold;
@synthesize stateNameString = _stateNameString, stateNameHold = _stateNameHold;
@synthesize roadNameString = _roadNameString, roadNameHold = _roadNameHold;
@synthesize drivingSpeedLabel = _drivingSpeedLabel;
@synthesize speedMeterBackground = _speedMeterBackground;
@synthesize speedLimitView = _speedLimitView;
@synthesize bonusTimeLabel = _bonusTimeLabel;
@synthesize bonusTimeChillLabel = _bonusTimeChillLabel;

@synthesize lastBonusMinute=_lastBonusMinute;
@synthesize lastBonusSecond=_lastBonusSecond;
@synthesize accelImage = _accelImage;
@synthesize flagView = _flagView;
@synthesize ownerFlag = _ownerFlag;
@synthesize hudView = _hudView;
@synthesize hudBonusLabel = _hudBonusLabel;
@synthesize hudBottomLabel = _hudBottomLabel;
@synthesize hudTopLabel = _hudTopLabel;
@synthesize hudBottomLabelDescription = _hudBottomLabelDescription;
@synthesize hudTopLabelDescription = _hudTopLabelDescription;
@synthesize hudImage = _hudImage;
@synthesize hudImage2 = _hudImage2;
@synthesize hudInsideView = _hudInsideView;
@synthesize speedLimitInHud = _speedLimitInHud;
@synthesize notDrivingLabelView = _notDrivingLabelView;
@synthesize notDrivingStatsView = _notDrivingStatsView;
@synthesize coinLabel = _coinLabel;
@synthesize turfLabel = _turfLabel;
@synthesize usStateAbbreviations = _usStateAbbreviations;
@synthesize numRoadChanges = _numRoadChanges;
@synthesize numSpeedLimitChanges = _numSpeedLimitChanges;
@synthesize speedMeterView = _speedMeterView;
@synthesize todayLabel = _todayLabel;
@synthesize gbarView = _gbarView;
@synthesize coinAmountLabel = _coinAmountLabel;
@synthesize bonusClockView = _bonusClockView;
@synthesize speedLimitSignView = _speedLimitSignView;
@synthesize todayIncidents = _todayIncidents;
@synthesize todayRoadsCaptured = _todayRoadsCaptured;
@synthesize todayTotalAttack=_todayTotalAttack;
@synthesize weeklyTotalAttack=_weeklyTotalAttack;
@synthesize todayRoadsCapturedLabel = _todayRoadsCapturedLabel;
@synthesize todayIncidentsLabel = _todayIncidentsLabel;
@synthesize todayCoinEarned = _todayCoinEarned;
@synthesize todayCoinEarnedLabel = _todayCoinEarnedLabel;
@synthesize todayTollIn=_todayTollIn;
@synthesize todayTollOut=_todayTollOut;
@synthesize todayTollInLabel=_todayTollInLabel;
@synthesize todayTollOutLabel=_todayTollOutLabel;

@synthesize originalFrameStreetSign = _originalFrameStreetSign;
@synthesize topTitleView = _topTitleView;
@synthesize footerView = _footerView;
@synthesize flagBackgroundView = _flagBackgroundView;
@synthesize streetSignView = _streetSignView;
@synthesize streetSignBackgroundView = _streetSignBackgroundView;
@synthesize roadOwnerFbId = _roadOwnerFbId;
@synthesize roadOwnerLabel = _roadOwnerLabel;
@synthesize roadOwnerView = _roadOwnerView;
@synthesize profilePicture = _profilePicture;
@synthesize gpsDebuggingView = _gpsDebuggingView;
@synthesize countryNameString = _countryNameString;
@synthesize bCanGetBusted = _bCanGetBusted;
@synthesize bCanGetPenalized = _bCanGetPenalized;
@synthesize footerTotalCoinLabel = _footerTotalCoinLabel;
@synthesize footerEarnRateLabel = _footerEarnRateLabel;
@synthesize bBustedViewBackgroundColor = _bBustedViewBackgroundColor;
@synthesize bBustedSpeedMeterColor = _bBustedSpeedMeterColor;
@synthesize lastTimeStamp=_lastTimeStamp;
@synthesize quadriant1View=_quadriant1View, quadriant2View=_quadriant2View, quadriant3View=_quadriant3View, quadriant4View=_quadriant4View;
@synthesize footerCoinLabel=_footerCoinLabel;
@synthesize footerCoinView=_footerCoinView;
@synthesize currentEarnRate=_currentEarnRate;
@synthesize currentEarnRateInGreenZone=_currentEarnRateInGreenZone;
@synthesize accountUpdateTimer=_accountUpdateTimer;
@synthesize lastRefresh=_lastRefresh;
@synthesize todayRoadsLostAttack=_todayRoadsLostAttack;
@synthesize todayRoadsCapturedAttack=_todayRoadsCapturedAttack;
@synthesize weeklyRoadsCapturedAttack=_weeklyRoadsCapturedAttack;
@synthesize todayRoadsCapturedAttackLabel=_todayRoadsCapturedAttackLabel;
@synthesize todayRoadsLost=_todayRoadsLost;
@synthesize todayRoadsLostAttackLabel=_todayRoadsLostAttackLabel;
@synthesize todayRoadsLostLabel=_todayRoadsLostLabel;
@synthesize roadOwner=_roadOwner;
@synthesize roadId=_roadId;
@synthesize bReportedSpeedLimit=_bReportedSpeedLimit;
@synthesize lastSpeedLimitDistanceDifference=_lastSpeedLimitDistanceDifference;
@synthesize lastAttackButton=_lastAttackButton;
@synthesize lastDrivingSpeed=_lastDrivingSpeed;
@synthesize bReportedSpeedLimitWarning=_bReportedSpeedLimitWarning;
@synthesize currentSl=_currentSl;
@synthesize profilePictureHud=_profilePictureHud;
@synthesize todayReportedSpeedLimits=_todayReportedSpeedLimits;
@synthesize serverParameters=_serverParameters;
@synthesize driveSnapshot=_driveSnapshot;
@synthesize drivingStateBeforeConnectivityLoss=_drivingStateBeforeConnectivityLoss;
@synthesize bGotBigBonus=_bGotBigBonus;
@synthesize player=_player;
@synthesize bGotDeductionGoingToBackground=_bGotDeductionGoingToBackground;
@synthesize bAccelerometerEvent=_bAccelerometerEvent;
@synthesize accelIncidentsThisTimePeriod=_accelIncidentsThisTimePeriod;
@synthesize currentMqSpeedLimit=_currentMqSpeedLimit;
@synthesize speedingMessageLabel=_speedingMessageLabel;
@synthesize speedingCostLabel=_speedingCostLabel;
@synthesize speedingCostView=_speedingCostView;
@synthesize speedingInsuranceCostLabel=_speedingInsuranceCostLabel;
@synthesize perMinuteLabel = _perMinuteLabel;
@synthesize mphLabel = _mphLabel;
@synthesize bAcceptedDrivingWarning=_bAcceptedDrivingWarning;
@synthesize backgroundDayView=_backgroundDayView;
@synthesize backgroundNightView=_backgroundNightView;
@synthesize backgroundCloudImageView=_backgroundCloudImageView;
@synthesize backgroundStarsImageView=_backgroundStarsImageView;
@synthesize topTitleGradientImageView=_topTitleGradientImageView;
@synthesize bDayTime=_bDayTime;
@synthesize moreButton=_moreButton;
@synthesize attackButton=_attackButton;
@synthesize dangerGradientView=_dangerGradientView;
@synthesize warningGradientView=_warningGradientView;

BOOL bUseDarkSystemInfo;
BOOL bMQRunningOk;


- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    //NSLog(@"Main driving view loading....");
    
    // first time running app, want to show them Quick Help screens
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    BOOL bViewedAnimation = [prefs integerForKey:@"bViewedAnimation"];
    if (bViewedAnimation == FALSE) {
        
        NSString *url = [[NSBundle mainBundle] pathForResource:@"roadwars_animation" ofType:@"mov"];
        self.player = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:url]];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieFinishedCallback:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.player.moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(hidecontrol)
                                                     name:MPMoviePlayerLoadStateDidChangeNotification
                                                   object:self.player.moviePlayer];
        
        self.player.moviePlayer.controlStyle=MPMovieControlStyleNone;
        //---play roadwars animation
        [self presentMoviePlayerViewControllerAnimated:self.player];
        
        bViewedAnimation = TRUE;
        [prefs setInteger:bViewedAnimation forKey:@"bViewedAnimation"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else
        [self finishRoadWarsStartup];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist"]]];
    
    // log file
    //BOOL b = [[NSUserDefaults standardUserDefaults] boolForKey: DEBUGGING_PREF_KEY];
    //NSLog (@"debug is <%d>",b);
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey: DEBUGGING_PREF_KEY])
        [self redirectConsoleLogToDocumentFolder];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleSwipeGesture:)];
    swipeGesture.numberOfTouchesRequired=2;
    swipeGesture.direction = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGesture];
    
    self.gbarView.backgroundColor = kColorStreetSign;
    
    [self addObserver:kOnSpeedUpdate withSelector:@selector(onSpeedUpdate:)];
    [self addObserver:kOnLocationUpdate withSelector:@selector(onLocationUpdate:)];
    [self addObserver:kOnSpeedlimitUpdate withSelector:@selector(onSpeedlimitUpdate:)];
    [self addObserver:kOnAccelerometerIncident withSelector:@selector(onAccelerometerIncident:)];
    [self addObserver:kOnAccelerometerDetectMovingPhone withSelector:@selector(onAccelerometerDetectMovingPhone:)];
    [self addObserver:kOnBonusTimerUpdate withSelector:@selector(onBonusTimerUpdate:)];
    [self addObserver:kOnCapturedRoad withSelector:@selector(onCapturedRoad:)];
    [self addObserver:kOnIsRoadOwned withSelector:@selector(onIsRoadOwned:)];
    [self addObserver:kOnIsRoadOwnedAccountUpdate withSelector:@selector(onIsRoadOwnedAccountUpdate:)];
    
    [self addObserver:kOnMotionStart withSelector:@selector(onMotionStart:)];
    [self addObserver:kOnMotionStop withSelector:@selector(onMotionStop:)];
    [self addObserver:kOnGoingToBackground withSelector:@selector(onGoingToBackground:)];
    [self addObserver:kOnGoingToForeground withSelector:@selector(onGoingToForeground:)];
    [self addObserver:kOnInternetReachable withSelector:@selector(onInternetReachable:)];
    [self addObserver:kOnInternetNotReachable withSelector:@selector(onInternetNotReachable:)];
    [self addObserver:kOnReportSpeedLimit withSelector:@selector(onReportSpeedLimit:)];
    [self addObserver:kOnWinAttack withSelector:@selector(onWinAttack:)];
    [self addObserver:kOnLoseAttack withSelector:@selector(onLoseAttack:)];
    [self addObserver:kOnLoseAttackMe withSelector:@selector(onLoseAttackMe:)];
    [self addObserver:kOnTollsIn withSelector:@selector(onTollsIn:)];
    [self addObserver:kOnTollsOut withSelector:@selector(onTollsOut:)];
    [self addObserver:kOnInitializeRoadWarsServer withSelector:@selector(onInitializeRoadWarsServer:)];
    [self addObserver:kOnIssueAttack withSelector:@selector(onIssueAttack:)];
    [self addObserver:kOnStopBonusTimer withSelector:@selector(onStopBonusTimer:)];
    [self addObserver:kOnGetSkinny withSelector:@selector(doGetSkinny:)];
    
    // set up us state abbreviations
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"USStateAbbreviations" ofType:@"plist"];
    self.usStateAbbreviations = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
       
    self.bCanGetBusted=self.bCanGetPenalized=YES;
    self.lastBonusMinute=self.lastBonusSecond=-1;
    self.flagView.alpha=.3;
    self.numRoadChanges=[NSNumber numberWithInt:0];
    self.numSpeedLimitChanges=[NSNumber numberWithInt:0];
    self.lastDrivingSpeed=[NSNumber numberWithInt:-1];
    self.navigationController.navigationBar.hidden=YES;
    self.navigationController.tabBarController.tabBar.hidden=YES;
    self.footerView.hidden=YES;
    
    self.bBustedViewBackgroundColor=NO; // use it to switch between red/blue view background when in busted range
    self.bBustedSpeedMeterColor=NO;
    self.bReportedSpeedLimit=NO;
    self.bReportedSpeedLimitWarning=NO;
    self.bAcceptedDrivingWarning=NO;
    
    self.currentSl=nil;
     
    self.lastSpeedLimitDistanceDifference=100000; // br: constant
    self.currentEarnRate=[NSNumber numberWithInt:kInitializedEarnRate];
    self.originalFrameStreetSign=self.streetSignView.frame;
    
    self.serverParameters = [[NSDictionary alloc]init];
    [self onInitializeRoadWarsServer:nil];
   
    self.drivingStateBeforeConnectivityLoss=kInitializeDrivingState;
    self.bGotBigBonus=NO;
    self.bGotDeductionGoingToBackground=NO;
    self.bAccelerometerEvent=NO;
    
    //initially assume daytime...
    self.backgroundNightView.hidden=YES;
    self.backgroundStarsImageView.hidden=YES;
    self.topTitleGradientImageView.hidden=YES;
    self.warningGradientView.hidden=YES;
    self.dangerGradientView.hidden=YES;
    
    self.backgroundDayView.hidden=NO;
    self.bDayTime=YES;
    
    self.drivingSpeedLabel.text = @"0";
    self.speedLimitLabel.text = @"0";
    
    
    // two calls below are in now to test api, need to actually implement
    // get inaccuracies
    [RWAPI getInaccuracies: ^(NSDictionary * inaccuracies) {
        //NSLog (@"results from getInaccuracies <%@>",inaccuracies);
    } failure:^(NSError *error) {
        // sigh... what to do?
    }];
  
    // check version
    [RWAPI checkForUpdate:[NSNumber numberWithFloat:1.0] completion: ^(NSDictionary * versionInfo) {
        NSLog (@"results from checkForUpdate <%@>",versionInfo);
    } failure:^(NSError *error) {
        // sigh... what to do?
    }];

    bMQRunningOk=YES;
    bUseDarkSystemInfo=NO;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
}

-(void)doOneGeocode{
    CLLocation *lastLocation;
    lastLocation=[[RWServices locationService]lastLocation];
    [[RWServices geocodingService] reverseGeocode:lastLocation];
   
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
    self.navigationController.tabBarController.tabBar.hidden=YES;

    // trying to find out if we own the road or not
    [self doDisplayMyInventory];
    [self doNotDrivingModeLabels];
    [self doGetSkinny:nil];
    [self getMyDailyStats];
    [self doOnRoadUpdate];
    
    
}


- (void)dealloc { // best place to unload notifications, release any retained objects, etc
    
    [self removeObserver:kOnSpeedUpdate];
    [self removeObserver:kOnLocationUpdate];
    [self removeObserver:kOnSpeedlimitUpdate];
    [self removeObserver:kOnAccelerometerIncident];
    [self removeObserver:kOnAccelerometerDetectMovingPhone];
    
    [self removeObserver:kOnBonusTimerUpdate];
    [self removeObserver:kOnCapturedRoad];
    [self removeObserver:kOnIsRoadOwned];
    [self removeObserver:kOnIsRoadOwnedAccountUpdate];
    [self removeObserver:kOnMotionStart];
    [self removeObserver:kOnMotionStop];
     
    [self removeObserver:kOnGoingToBackground];
    [self removeObserver:kOnGoingToForeground];
    [self removeObserver:kOnInternetReachable];
    [self removeObserver:kOnInternetNotReachable];
    [self removeObserver:kOnReportSpeedLimit];
    [self removeObserver:kOnWinAttack];
    [self removeObserver:kOnLoseAttack];
    [self removeObserver:kOnLoseAttackMe];
    [self removeObserver:kOnTollsIn];
    [self removeObserver:kOnTollsOut];
    [self removeObserver:kOnInitializeRoadWarsServer];
    [self removeObserver:kOnIssueAttack];
    [self removeObserver:kOnStopBonusTimer];
    [self removeObserver:kOnGetSkinny];

     
    self.speedLimitLabel=nil;
    self.stateNameLabel=nil;
    self.roadNameLabel=nil;
    self.townStateNameLabel=nil;
    self.townNameHold=nil;
    self.townNameString=nil;
    self.stateNameHold=nil;
    self.stateNameString=nil;
    self.roadNameLabel=nil;
    self.roadNameHold=nil;
    self.drivingSpeedLabel=nil;
    self.speedMeterBackground=nil;
    self.speedLimitView=nil;
    
     self.accelImage=nil;
      
    self.hudView=nil;
    self.hudImage=nil;
    self.hudImage2=nil;
    self.hudBottomLabel=nil;
    self.hudTopLabel=nil;
    
    self.notDrivingLabelView=nil;
    self.notDrivingStatsView=nil;
    self.coinLabel=nil;
    self.turfLabel=nil;
    
    self.numRoadChanges=nil;
    self.speedMeterView=nil;
    
    self.todayLabel=nil;
    self.topTitleView=nil;
    self.footerView=nil;
    
    self.serverParameters=nil;
 
}

- (void) doAccountUpdate:(NSTimer*)sender{
    int accountAmountToAdd=self.currentEarnRate.intValue;
   
    if (accountAmountToAdd == kInitializedEarnRate)
        return;
    
     if ([[RWServices bonusTimerService]running]==NO)
         return;
   
    //NSLog (@"doAcountUpdate");
    
    [self doDisplayMyInventory];
    NSNumber *accountUpdateInterval = [self.serverParameters objectForKey:@"accountUpdateInterval"];
    //NSLog (@"accountUpdateInterval <%d>", accountUpdateInterval.intValue);
    
    if (/* self.lastRefresh == nil ||*/ [self.lastRefresh timeIntervalSinceNow] < accountUpdateInterval.intValue ) {
        NSLog (@"doAccountUpdate <%d>",accountAmountToAdd);
        [self doOnAccountUpdate:accountAmountToAdd:accountAmountToAdd: abs(accountUpdateInterval.intValue)];// account and coin every minute
        /*
        if (accountAmountToAdd>=0) {
            self.todayCoinEarned+=accountAmountToAdd;
            self.todayCoinEarnedLabel.text=[NSString stringWithFormat:@"+%d", self.todayCoinEarned];
        }
        else {
            self.todayIncidents+=-(accountAmountToAdd);
            self.todayIncidentsLabel.text=[NSString stringWithFormat:@"-%d", self.todayIncidents];
        }
        */
        self.lastRefresh = [NSDate date];
        
    }
}

// *listen* for bonus timer updates
- (void)onBonusTimerUpdate:(NSNotification *)notif {
   
    RWBonusTimerService *bonusTimer = notif.object;
    NSString *bonusTime, *bonusTimeForSkinny;
    if ([[bonusTimer stringHours]intValue]){
        bonusTime = [NSString stringWithFormat:@"%@:%@:%@", [bonusTimer stringHours], [bonusTimer stringMinutes], [bonusTimer stringSeconds]];
         bonusTimeForSkinny = [NSString stringWithFormat:@"%@ Hour %@ Minutes", [bonusTimer stringHours], [bonusTimer stringMinutes]];
    }
    else{
        bonusTime = [NSString stringWithFormat:@"%@:%@", [bonusTimer stringMinutes], [bonusTimer stringSeconds]];
        bonusTimeForSkinny = [NSString stringWithFormat:@"%@ Minutes", [bonusTimer stringMinutes]];

    }
    
    // Display bonus time
    self.bonusTimeLabel.text= bonusTime;
    
    // do the coins
    int currentSeconds = [[bonusTimer stringSeconds] intValue];
    int currentMinutes = [[bonusTimer stringMinutes] intValue];
    int currentHours = [[bonusTimer stringHours] intValue];
    
    int coinAmountToAddToAccount=self.currentEarnRate.intValue;
    // if starting a new minute, play a sound and report bonus coins to server
    //NSLog (@"current seconds <%d>", currentSeconds);
    //NSLog (@"current minutes <%d>", currentMinutes);
    //NSLog (@"bonus time <%@>",bonusTime);
    
    if (currentSeconds==0){
        if (currentHours==0 && currentMinutes==0){
            self.accelIncidentsThisTimePeriod=0;
            return;

        }

        NSNumber *bigBonusTime = [self.serverParameters objectForKey:@"bigBonusTime"];
        //NSLog (@"bigBonusTime <%d>", bigBonusTime.intValue);
        NSNumber *currentMinutesBonusMultiplier = [self.serverParameters objectForKey:@"currentMinutesBonusMultiplier"];
        //NSLog (@"currentMinutesBonusMultiplier <%d>", currentMinutesBonusMultiplier.intValue);
        
        
        if (self.bGotBigBonus==NO && (currentMinutes % bigBonusTime.intValue == 0)){
            //NSLog (@"Bonus time!");
            self.bGotBigBonus=TRUE;
            coinAmountToAddToAccount=(currentHours*(currentMinutesBonusMultiplier.intValue*60))+(currentMinutes*currentMinutesBonusMultiplier.intValue);
            
            [[RWServices audioService ]playSoundEvent:(currentMinutes % 60 == 0)?kSoundEventBigBonus:kSoundEventSmallBonus:soundFromGame];
            
           // NSString *bonusTimerAmount = [NSString stringWithFormat:@"%d for %@",coinAmountToAddToAccount, bonusTimeForSkinny];
            NSString *bonusTimerAmount = [NSString stringWithFormat:@"%d",coinAmountToAddToAccount];
            NSString *bonusTimeLabel =[NSString stringWithFormat:@"%@",bonusTimeForSkinny];
            NSString *bonusSmoothAmount;
            NSNumber *smoothBonusAmountStart = [self.serverParameters objectForKey:@"smoothBonusAmountStart"];
            int smoothBonusAmount=smoothBonusAmountStart.intValue-(self.accelIncidentsThisTimePeriod*10); // BR: server param
            if (smoothBonusAmount>0){
                //bonusSmoothAmount = [NSString stringWithFormat:@"%d for smooth driving",smoothBonusAmount];
                bonusSmoothAmount = [NSString stringWithFormat:@"%d",smoothBonusAmount];

                coinAmountToAddToAccount += smoothBonusAmount;
               //NSLog (@"bonusAmount from accelerometers <%d>", bonusAmount);
               
            }
            self.accelIncidentsThisTimePeriod=0;

            [self doShowBonusHud:@"Bonus":bonusTimerAmount:bonusSmoothAmount:bonusTimeLabel:@"Smooth":nil];
            
            [RWContext me].numBonusRecieved = [NSNumber numberWithInt:coinAmountToAddToAccount];
            [self doOnAccountUpdate:0:coinAmountToAddToAccount:0];
           // NSLog (@"coinAmountToAddToAccount <%d>", coinAmountToAddToAccount);
            self.todayCoinEarned+=coinAmountToAddToAccount;
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            if (self.currentEarnRate.intValue == kInitializedEarnRate)
                self.todayCoinEarnedLabel.text=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[ NSNumber numberWithInt:self.todayCoinEarned]]];
            
           // self.todayCoinEarnedLabel.text=[NSString stringWithFormat:@"%d", ];
            
            // give an accelerometer bonus also at the hour marks
            if (currentMinutes % 60 == 0)
                [self performSelector:@selector(accelBonus) withObject:nil afterDelay:6];

            
        }
        else {// tiny (one minute) bonus
            if (self.bGotBigBonus==NO)
                [[RWServices audioService ]playSoundEvent:kSoundEventPaulComfortingTic:soundFromGame];
            
        }
    }
    else
        self.bGotBigBonus=NO;
    
    //NSLog(@"onBonusTimerUpdate");


}

- (void) accelBonus{
      
    int bonusAmount=100-(self.accelIncidentsThisTimePeriod*10); // BR: server param
    if (bonusAmount>0){
        NSString *bonusAmountAdded = [NSString stringWithFormat:@"%d Coin",bonusAmount];
        [self doOnAccountUpdate:0:bonusAmount:0];
        //NSLog (@"bonusAmount from accelerometers <%d>", bonusAmount);
        [self doShowHud:bonusAmountAdded:@"Smooth Driving Bonus":@"":[UIImage imageNamed:@"coin-plus.png"]:nil];
    }
    self.accelIncidentsThisTimePeriod=0;
}

- (void) onStopBonusTimer:(NSNotification *)notif {
    if ([[RWServices bonusTimerService]running]==YES)
        [[RWServices bonusTimerService]stop];
}

// *listen* for accelerometer incidents
- (void)onAccelerometerIncident:(NSNotification *)notif {
    //NSLog(@"%@:onAccelerometerIncident - %@", self.class, notif);
    NSNumber *accelerometerActivity = notif.object;
    
    if (self.bAccelerometerEvent)
        return;
    
    if ([accelerometerActivity intValue]==kAccelerometerActivityAccelerateBusted){
        [[RWServices audioService ]playSoundEvent:kSoundEventLoseBonusTime:soundFromGame];
        [self shakescreen];
        self.accelIncidentsThisTimePeriod++;
        
        /*
        [UIView animateWithDuration:1.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGAffineTransform transform = CGAffineTransformMakeScale(.5,.5);
            self.view.transform = transform;
            self.bAccelerometerEvent=YES;
            self.accelIncidentsThisTimePeriod++;
        } completion:^(BOOL finished){
            [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                CGAffineTransform transform = CGAffineTransformMakeScale(1,1);
                self.view.transform = transform;
                
            } completion:^(BOOL finished){
                self.bAccelerometerEvent=NO;
            }];
        }];
        */
        
        // [self doBusted:@"Acceleration":nil];
    }
    else if ([accelerometerActivity intValue]==kAccelerometerActivityBrakeBusted){
        [[RWServices audioService ]playSoundEvent:kSoundEventLoseBonusTime:soundFromGame];
        [self shakescreen];
        self.accelIncidentsThisTimePeriod++;
        
        /*
        [UIView animateWithDuration:1.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGAffineTransform transform = CGAffineTransformMakeScale(1.5,1.5);
            self.view.transform = transform;
            self.bAccelerometerEvent=YES;
             self.accelIncidentsThisTimePeriod++;
        } completion:^(BOOL finished){
            [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                CGAffineTransform transform = CGAffineTransformMakeScale(1,1);
                self.view.transform = transform;
                
            } completion:^(BOOL finished){
                self.bAccelerometerEvent=NO;
                
            }];
        }];
         */
        
        //[self doBusted:@"Braking":nil];
    } else if ([accelerometerActivity intValue]==kAccelerometerActivityLeftTurnBusted){
        [self shakescreen];
         self.accelIncidentsThisTimePeriod++;
        // [self doBusted:@"Swerve Left":nil];
    } else if ([accelerometerActivity intValue]==kAccelerometerActivityRightTurnBusted){
        [self shakescreen];
        // [self doBusted:@"Swerve Right":nil];
         self.accelIncidentsThisTimePeriod++;
    }
}
- (void)onAccelerometerDetectMovingPhone:(NSNotification *)notif {
   // [self doShowHud:@"Put the phone down":@"Do not use phone while driving":@"":[UIImage imageNamed:@"busted-phone.png"]:nil];
}


-(void)shakescreen
{
    [[RWServices audioService ]playSoundEvent:kSoundEventLoseBonusTime:soundFromGame];
    //Shake screen
    CGFloat t = 5.0;
    CGAffineTransform translateRight = CGAffineTransformTranslate(CGAffineTransformIdentity, t, t);
    CGAffineTransform translateLeft = CGAffineTransformTranslate(CGAffineTransformIdentity, -t, -t);
    
    self.view.transform = translateLeft;
    
    [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^
     {
         [UIView setAnimationRepeatCount:10.0];
         self.view.transform = translateRight;
     } completion:^(BOOL finished)
     
     {
         if (finished)
         {
             [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^
              {
                  self.view.transform = CGAffineTransformIdentity;
              }
                              completion:NULL];
         }
     }];
}

// *listen* for a location/road/town update here...
- (void)onLocationUpdate:(NSNotification *)notif {
    //NSLog(@"%@:onLocationUpdate - %@", self.class,notif.object);
    CLPlacemark *placemark = notif.object;
    
    //NSLog(@"placemark -- %@, %@", placemark, placemark.administrativeArea);
    
   // NSLog(@"state abbrev <%@>", self.usStateAbbreviations);
    NSString *stateAbbreviation;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        stateAbbreviation = placemark.administrativeArea;
    }else
        stateAbbreviation = [self.usStateAbbreviations  objectForKey:[placemark.administrativeArea uppercaseString]];
    self.townNameHold=placemark.locality;
    self.stateNameHold=stateAbbreviation;
    self.roadNameHold = placemark.thoroughfare;
    self.countryNameString = placemark.country;
    
    if (self.stateNameString.length==0 && self.townNameString.length==0){ // initially
        self.stateNameString=self.stateNameHold;
        self.townNameString=self.townNameHold;
        self.roadNameString=self.roadNameHold;
        if (self.roadNameString != nil)
            self.roadNameLabel.text= self.roadNameString;
       //NSLog(@"townNameString <%@>, stateNameString <%@>", self.townNameString, self.stateNameString);
        if (self.townNameString != nil && self.stateNameString!=nil)
            self.townStateNameLabel.text=[NSString stringWithFormat:@"%@, %@",self.townNameString,self.stateNameString];
    }
       
    [self doDisplayMyInventory];
  
    
    // trying to find out if we own the road or not
    if (!self.notDrivingStatsView.isHidden){
        [self doOnRoadUpdate];
    }
    
    
}


// *listen* for a driving speed updates
- (void)onSpeedUpdate:(NSNotification *)notif {
    // NSLog(@"%@:onSpeedUpdate - update speed driving %@", self.class, notif);
    //self.gpsDebuggingView.hidden=!self.gpsDebuggingView.hidden;
     
}

// *listen* for a speedlimit from MQ
- (void)onSpeedlimitUpdate:(NSNotification *)notif {
    if (self.bAcceptedDrivingWarning==NO)
        return;
    
    [RWServices startLocationService];
    //NSLog(@"%@:onSpeedlimitUpdate", self.class);
    NSDictionary *json = notif.object;
    // NSLog (@"%@", json);
    
    NSNumber *statusCode = [[json objectForKey:@"info"]objectForKey:@"statuscode"];
    //NSLog (@"statusCode %@", statusCode);
    bMQRunningOk=(statusCode.intValue==0)?YES:NO;
    self.speedLimitLabel.textColor=(statusCode.intValue==0)?[UIColor whiteColor]:[UIColor redColor];
    
    BOOL bRoadChanged = NO;
    
    if (self.serverParameters.count==0){
        NSLog(@"server parameters count == 0");
        return;
    }
    
    
    CLLocation *updatedLocation=[[RWServices locationService]lastLocation];
    NSNumber *drivingSpeed=[NSNumber numberWithDouble:0.0];
    
    if (updatedLocation.speed*KSpeedConverter >= 0)
        drivingSpeed = [NSNumber numberWithDouble:updatedLocation.speed*KSpeedConverter];
    
    NSNumber *minDrivingSpeed = [self.serverParameters objectForKey:@"minSpeed"];
    //NSLog (@"minDrivingSpeed <%d>", minDrivingSpeed.intValue);
    if (drivingSpeed.intValue < minDrivingSpeed.intValue ){
        [self postNotification:kOnStopBonusTimer withObject:nil];
        return;
    }
    else { // coming back from not driving to driving...make sure correct info displayed
        self.roadNameLabel.text = self.roadNameString;
        if (self.townNameString != nil && self.stateNameString!=nil)
            self.townStateNameLabel.text=[NSString stringWithFormat:@"%@, %@",self.townNameString,self.stateNameString];
        
    }
    
    self.drivingSpeedLabel.text = [NSString stringWithFormat:@"%d", drivingSpeed.intValue];
    NSNumber *currentHeading, *previousHeading;
    currentHeading=[NSNumber numberWithInt:0];
    previousHeading=[NSNumber numberWithInt:0];
    if ([RWServices locationService].headingArray.count>1){
        //NSLog (@"<%@>", [RWServices locationService].headingArray);
        currentHeading = [[RWServices locationService].headingArray objectAtIndex:0];
        previousHeading = [[RWServices locationService].headingArray objectAtIndex:[RWServices locationService].headingArray.count-1];
        //NSLog (@"currentHeading <%@> currentHeading1 <%@> previousHeading <%@> count <%d>", currentHeading,[[RWServices locationService].headingArray objectAtIndex:0], previousHeading,[RWServices locationService].headingArray.count);
    }
    
    NSString *lastRoadName = self.roadNameString;
    NSString *lastTownName = self.townNameString;
    NSString *lastStateName = self.stateNameString;
    // NSLog(@"lastroad <%@>", lastRoadName);
    NSString *thisRoadName = self.roadNameHold;
    
    if (lastRoadName.length==0){
        //NSLog (@"lastRoadName.length==0)");
        self.roadNameString=thisRoadName;
    }
    else
        if (![lastRoadName isEqualToString:thisRoadName] ||
            ![lastTownName isEqualToString:self.townNameHold] ||
            ![lastStateName isEqualToString:self.stateNameHold]){
            
            int headingDifference = abs(currentHeading.intValue-previousHeading.intValue);
            
            NSNumber *numStreetChangeNameMin = [self.serverParameters objectForKey:@"numStreetChangeNameMin"];
            //NSLog (@"numStreetChangeNameMin <%d>", numStreetChangeNameMin.intValue);
            NSNumber *numStreetChangeNameMax = [self.serverParameters objectForKey:@"numStreetChangeNameMax"];
            //NSLog (@"numStreetChangeNameMax <%d>", numStreetChangeNameMax.intValue);
            NSNumber *headingDiffChangeRoad = [self.serverParameters objectForKey:@"headingDiffChangeRoad"];
            // NSLog (@"headingDiffChangeRoad <%d>", headingDiffChangeRoad.intValue);
            NSNumber *headingDiffMaybeChangeRoad = [self.serverParameters objectForKey:@"headingDiffMaybeChangeRoad"];
            // NSLog (@"headingDiffMaybeChangeRoad <%d>", headingDiffMaybeChangeRoad.intValue);
            
            int numRoadChangedNeeded = (headingDifference > headingDiffMaybeChangeRoad.intValue)?numStreetChangeNameMin.intValue:numStreetChangeNameMax.intValue;
            if (((headingDifference>headingDiffChangeRoad.intValue)/* && self.numRoadChanges.intValue>0 */) || self.numRoadChanges.intValue>numRoadChangedNeeded){
                // road changed
                //NSLog(@"abs(currentHeading.intValue-previousHeading.intValue) %d", headingDifference);
                //NSLog (@"road changed, old road <%@>, new road <%@>, numRoadChanges <%@>", lastRoadName, thisRoadName, self.numRoadChanges);
                self.numRoadChanges = [NSNumber numberWithInt:0];
                self.ownerFlag.hidden=YES;
                self.roadNameString=thisRoadName;
                self.roadNameLabel.text=thisRoadName;
                self.stateNameString=self.stateNameHold;
                self.townNameString=self.townNameHold;
                // town, state - one label used on storyboard
                if (self.townNameString != nil && self.stateNameString!=nil)
                    self.townStateNameLabel.text=[NSString stringWithFormat:@"%@, %@",self.townNameString,self.stateNameString];
                // NSLog (@"road changed, road <%@> town <%@> state <%@>", self.roadNameString, self.townNameString, self.stateNameString);
                [self doOnRoadUpdate];
                NSNumber *minTimeToDriveOnRoad = [self.serverParameters objectForKey:@"minTimeToDriveOnRoad"];
                [self performSelector:@selector(doCheckIfOwnRoad) withObject:nil afterDelay:minTimeToDriveOnRoad.intValue];
                
                bRoadChanged=YES;
                self.numSpeedLimitChanges = [NSNumber numberWithInt:0];
                self.bReportedSpeedLimit=NO;
                if ([RWServices locationService].headingArray.count>0){
                    [[RWServices locationService].headingArray removeAllObjects];
                    // NSLog (@"Removed all objects from heading array");
                }
            }
            else{
                //NSLog (@"maybe change road <%d>", self.numRoadChanges.intValue);
                //NSLog(@"abs(currentHeading.intValue-previousHeading.intValue) %d", headingDifference);
                self.numRoadChanges = [NSNumber numberWithInt:self.numRoadChanges.intValue+1];
                return;
            }
            
        } else // same road
            self.numRoadChanges = [NSNumber numberWithInt:0];
    
}

- (void) doCheckIfOwnRoad{
    [self doOnAccountUpdate:1 :0 :0];
}

- (void)onCapturedRoad:(NSNotification *)notif {
    //NSLog (@"OnCapturedRoad %@", notif.object);
    
    NSDictionary *result = notif.object;
    if ([[result objectForKey:@"data"]objectForKey:@"capturedFbId"]!=nil){
        [[RWServices audioService ]playSoundEvent:kSoundEventRoadCapturedFromFriend:soundFromGame];
       // NSLog (@"captured from friend");
    }
    else{
        [[RWServices audioService ]playSoundEvent:kSoundEventRoadCaptured:soundFromGame];
        self.ownerFlag.image = [UIImage imageNamed:@"flag-large.png"];
       // NSLog (@"captured new");
    }
    
   // NSDictionary *captured = [[result objectForKey:@"data"]objectForKey:@"captured"];
    
   // [self doShowHud:@"Captured" :[captured objectForKey:@"name"] :[[captured objectForKey:@"city"] objectForKey:@"name"]:[UIImage imageNamed:@"flag.png"]: [[result objectForKey:@"data"]objectForKey:@"capturedFbId"]];
    
    // have flag animate to position on street sign, indicating road is ours
    self.ownerFlag.hidden=NO;
   
    self.ownerFlag.image = [UIImage imageNamed:@"flag-large.png"];
    CGRect originalFrame = self.ownerFlag.frame;
    self.ownerFlag.frame = CGRectMake(self.view.frame.size.width/2.0-37.5, 0, 75, 75);
    // bonus coin animation
    [self.view bringSubviewToFront:self.ownerFlag];
    // CA: hardest thing is converting to the right coordinate system for lining up the coin in HUD
    CGPoint convertedOrigin = [self.view convertPoint:self.view.frame.origin toView:self.ownerFlag.superview];
    self.ownerFlag.frame = CGRectMake(convertedOrigin.x+50, convertedOrigin.y+100, 200, 200);
    self.ownerFlag.alpha = 1.0;
    //NSLog(@"%@::doLocationUpdate - hud origin is %f, %f - converted is %f, %f, and size is %f, %f", self.class, self.hudView.frame.origin.x, self.hudView.frame.origin.y, convertedOrigin.x, convertedOrigin.y, self.hudView.frame.size.width, self.hudView.frame.size.height);
    
    [UIView animateWithDuration:0.6 delay:5.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.ownerFlag.frame = originalFrame;
    } completion:^(BOOL finished){
        //NSLog (@"Finished flag animation:");
        self.profilePicture.hidden=YES;
        [self doDisplayMyInventory];
    }];
    
    self.todayRoadsCapturedLabel.text=[NSString stringWithFormat:@"+%d", ++self.todayRoadsCaptured];
    

}
- (void)onIsRoadOwnedAccountUpdate:(NSNotification *)notif {
    //NSLog (@"onIsRoadOwnedAccountUpdate %@", notif.object);
    NSDictionary *d = notif.object;
    NSString *runnerUpCoverage = [d objectForKey:@"runnerUpCoverageAccount"];
    NSString *coverageAccount = [d objectForKey:@"coverageAccount"];
    NSString *ownerCoverageAccount = [d objectForKey:@"ownerCoverageAccount"];
    NSNumber *tollFbId =[d objectForKey:@"tollFbId"];
    if (tollFbId.intValue!=0)
        self.roadOwnerFbId = tollFbId;
    self.profilePicture.profileID = self.roadOwnerFbId.stringValue;
    //NSLog (@" runnerUp <%@>, coverageAccoubnt <%@>, ownerCoverage <%@>", runnerUpCoverage, coverageAccount, ownerCoverageAccount);
  
    self.ownerFlag.hidden=/*self.flagView.hidden=*/(runnerUpCoverage == nil && coverageAccount== nil && ownerCoverageAccount == nil) ? YES : NO;
    
    self.ownerFlag.image=(runnerUpCoverage==nil && ownerCoverageAccount != nil) ?  nil :  [UIImage imageNamed:@"flag-large.png"];
    
    // self.profilePicture.hidden=(runnerUpCoverage==nil && ownerCoverageAccount != nil) ?  NO :  YES;
    if (runnerUpCoverage==nil && ownerCoverageAccount != nil){
        self.profilePicture.hidden=NO;
        self.roadOwner = (User *)[RWContext getEntityOfType:kModelUser where:[NSPredicate predicateWithFormat:@"self.fbId == %@", self.roadOwnerFbId]];
        //NSLog (@"owner is <%@>", self.roadOwner.name);
        if (self.roadOwner.name == nil){ // not one of my friends
            self.profilePicture.hidden=YES;
            self.ownerFlag.image=[UIImage imageNamed:@"flag-black.png"];
            
            NSNumber *strangerEarnRate = [self.serverParameters objectForKey:@"strangerRoadEarnRate"];
            //NSLog (@"strangerEarnRate <%d>", strangerEarnRate.intValue);
            
            self.currentEarnRate=[NSNumber numberWithInt:strangerEarnRate.intValue];
            //self.flagView.layer.borderColor=kColorHudBackgroundDark.CGColor;
        } else {
            NSNumber *friendRoadEarnRate = [self.serverParameters objectForKey:@"friendRoadEarnRate"];
            //NSLog (@"friendRoadEarnRate <%d>", friendRoadEarnRate.intValue);
            
            self.currentEarnRate=[NSNumber numberWithInt:friendRoadEarnRate.intValue]; // friends
            //self.flagView.layer.borderColor=kColorGold.CGColor;
        }
    }
    else{
        self.profilePicture.hidden=YES;
        NSNumber *yourRoadEarnRate = [self.serverParameters objectForKey:@"yourRoadEarnRate"];
        //NSLog (@"yourRoadEarnRate <%d>", yourRoadEarnRate.intValue);
        
        self.currentEarnRate=[NSNumber numberWithInt:yourRoadEarnRate.intValue]; // mine
       // self.flagView.layer.borderColor=kColorBlueBusted.CGColor;
        self.ownerFlag.image=[UIImage imageNamed:@"flag-large.png"];
    }
    self.currentEarnRateInGreenZone=self.currentEarnRate;
    
    // update coin/turf on drive screen footer
    [self doDisplayMyInventory];
    
}

- (void)onIsRoadOwned:(NSNotification *)notif {
    //NSLog (@"OnIsRoadOwned %@", notif.object);
    NSDictionary *d = notif.object;
    //NSLog(@"[d objectForKey:owne <%@>", [d objectForKey:@"owner"]);
    
    if ([[d objectForKey:@"owner"] isKindOfClass:[NSDictionary class]]) {
        self.roadOwnerFbId = [[d objectForKey:@"owner"]objectForKey:@"fbId"];
        //NSLog(@"self.roadOwnerFbId %@", self.roadOwnerFbId);
        self.profilePicture.profileID = self.roadOwnerFbId.stringValue;
    }
    else
        self.roadOwnerFbId=0;
    
    //NSLog(@"self.profilePicture.profileID %@", self.profilePicture.profileID);
    if (self.roadOwnerFbId.longLongValue==[RWContext me].fbId.longLongValue){
        self.profilePicture.hidden=YES;
        self.ownerFlag.hidden=self.flagView.hidden=NO;
        //self.flagView.layer.borderColor=kColorBlueBusted.CGColor;
        
        self.ownerFlag.image=[UIImage imageNamed:@"flag-large.png"];
        NSNumber *yourRoadEarnRate = [self.serverParameters objectForKey:@"yourRoadEarnRate"];
        //NSLog (@"yourRoadEarnRate <%d>", yourRoadEarnRate.intValue);
        
        self.currentEarnRate=[NSNumber numberWithInt:yourRoadEarnRate.intValue]; // friends
        
    }
    else {
        self.roadOwner = (User *)[RWContext getEntityOfType:kModelUser where:[NSPredicate predicateWithFormat:@"self.fbId == %@", self.roadOwnerFbId]];
        //NSLog (@"owner is <%@>", self.roadOwner.name);
        if (self.roadOwner.name == nil){ // not one of my friends
            self.profilePicture.hidden=YES;
            self.ownerFlag.image=[UIImage imageNamed:@"flag-black.png"];
            NSNumber *strangerRoadEarnRate = [self.serverParameters objectForKey:@"strangerRoadEarnRate"];
            ///NSLog (@"strangerRoadEarnRate <%d>", strangerRoadEarnRate.intValue);
            
            self.currentEarnRate=[NSNumber numberWithInt:strangerRoadEarnRate.intValue];
            //self.flagView.layer.borderColor=kColorHudBackgroundDark.CGColor;
        } else {
            self.profilePicture.hidden=NO;
            //self.flagView.hidden=YES;
            self.ownerFlag.image=nil;
            NSNumber *friendRoadEarnRate = [self.serverParameters objectForKey:@"friendRoadEarnRate"];
            //NSLog (@"friendRoadEarnRate <%d>", friendRoadEarnRate.intValue);
            
            self.currentEarnRate=[NSNumber numberWithInt:friendRoadEarnRate.intValue]; // friends
            //self.flagView.layer.borderColor=kColorGold.CGColor;
            
        }

    }
    //NSLog (@"Leaving onisroadowned");
    
    // update coin/turf on drive screen footer
    [self doDisplayMyInventory];
    
}


- (void)doBusted:(NSString*)topLabelText :(NSString*)bottomLabelText :(NSString*)bottomLabelDescriptionText{
    if (self.bCanGetBusted==NO) return;
    
    //NSLog (@"BUSTED -- 30 minute penalty");
    
    [[RWServices audioService ]playSoundEvent:kSoundEventBusted:soundFromGame];
    //[[RWServices bonusTimerService]resetBonusTimer];
    [[RWServices bonusTimerService]bustedDeductionBonusTimer];

    [self doShowSpeedingHud:topLabelText :bottomLabelText:bottomLabelDescriptionText:[UIImage imageNamed:@"busted-speed.png"]:nil];
    
    self.todayIncidentsLabel.text=[NSString stringWithFormat:@"-%d", self.todayIncidents+=30]; //BR:server param
    self.bCanGetBusted=NO;
    [self performSelector:@selector(doCanGetBusted) withObject:nil afterDelay:15];// BR:server variable
}

- (void)doShowSpeedingHud:(NSString *)topLabelText :(NSString*)bottomLabelText :(NSString*)bottomLabelDescriptionText :(UIImage*)image :(NSNumber*)fbId {
    if ( self.hudView == nil ) {
        self.hudView = [UIView uiViewFromNibName:@"RWSpeedingHudView" owner:self];
        [self.view addSubview:self.hudView];
        [self.view bringSubviewToFront:self.speedMeterView];
    }
    else //only one hud at a time
        return;
   
    self.hudTopLabel.text=topLabelText;
   //self.hudBottomLabel.text=bottomLabelText;
    CLLocation *updatedLocation=[[RWServices locationService]lastLocation];
    NSNumber *drivingSpeed = [NSNumber numberWithDouble:updatedLocation.speed*KSpeedConverter];
    self.hudBottomLabel.text=[NSString stringWithFormat:@"%d",drivingSpeed.intValue];
    //self.hudBottomLabelDescription.text=bottomLabelDescriptionText;
    self.hudBottomLabelDescription.text=@"SLOW DOWN!";
    self.hudImage.image = image;
    //self.speedLimitInHud.text = self.speedLimitLabel.text;
    
    [RWContext addSkinnyMessage:[NSString stringWithFormat:@"%@ %@ %@", topLabelText,bottomLabelText,bottomLabelDescriptionText] appendTime:TRUE];
    
   [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(doHideHud:) userInfo:nil repeats:NO];
}

- (void)doShowBonusHud:(NSString *)bonusLabelText :(NSString *)topLabelText :(NSString*)bottomLabelText :(NSString*)topLabelDescriptionText :(NSString*)bottomLabelDescriptionText :(NSNumber*)fbId {
   
    if ( self.hudView == nil ) {
        self.hudView = [UIView uiViewFromNibName:@"RWBonusHudView" owner:self];
        [self.view addSubview:self.hudView];
        [self.view bringSubviewToFront:self.speedMeterView];
    }
    else //only one hud at a time
        return;
    
    self.hudBonusLabel.text=bonusLabelText;
    self.hudTopLabel.text=topLabelText;
    self.hudBottomLabel.text=bottomLabelText;
    self.hudTopLabelDescription.text=topLabelDescriptionText;
    self.hudBottomLabelDescription.text=bottomLabelDescriptionText;
    
    self.hudImage.image = [UIImage imageNamed:@"gold-coin-bonus.png"];;
    self.hudImage2.image = [UIImage imageNamed:@"silver-coin-bonus.png"];;
    self.roadNameLabel.alpha=self.townStateNameLabel.alpha=0;;
    self.topTitleGradientImageView.hidden=YES;
    [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Bonus %@ %@ safe driving", topLabelText,topLabelDescriptionText] appendTime:TRUE];
    [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Bonus %@ %@", bottomLabelText,bottomLabelDescriptionText] appendTime:TRUE];
   
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(doHideHud:) userInfo:nil repeats:NO];
}

- (void)doShowInitialDrivingWarning {
    if ( self.hudView == nil ) {
        self.hudView = [UIView uiViewFromNibName:@"RWDriveWarningView" owner:self];
        [self.view addSubview:self.hudView];
    }
    else //only one hud at a time
        return;
}

- (void)doShowHud:(NSString *)topLabelText :(NSString*)bottomLabelText :(NSString*)bottomLabelDescriptionText :(UIImage*)image :(NSNumber*)fbId{
    
    if ( self.hudView == nil ) {
        self.hudView = [UIView uiViewFromNibName:@"RWHudView" owner:self];
        [self.view addSubview:self.hudView];
         [self.view bringSubviewToFront:self.speedMeterView];
    }
    else //only one hud at a time
        return;
    
    self.profilePictureHud.hidden=(fbId==nil)?YES:NO;

    self.hudTopLabel.text=topLabelText;
    self.hudBottomLabel.text=bottomLabelText;
    self.hudBottomLabelDescription.text=bottomLabelDescriptionText;
    self.hudImage.image = image;
   
    if (fbId!=nil){
        self.roadOwner = (User *)[RWContext getEntityOfType:kModelUser where:[NSPredicate predicateWithFormat:@"self.fbId == %@", self.roadOwnerFbId]];
        //NSLog (@"owner is <%@>", self.roadOwner.name);
        if (self.roadOwner.name == nil)// not one of my friends
            self.profilePictureHud.profileID=nil;
        else
            self.profilePictureHud.profileID = fbId.stringValue;
    
        [RWContext addSkinnyMessage:[NSString stringWithFormat:@"%@ %@ %@ from %@ %@", topLabelText,bottomLabelText,bottomLabelDescriptionText,self.roadOwner.first, [self.roadOwner.last substringToIndex:1]] appendTime:TRUE];
        
    }else
        [RWContext addSkinnyMessage:[NSString stringWithFormat:@" %@ %@ %@", topLabelText,bottomLabelText,bottomLabelDescriptionText] appendTime:TRUE];
    
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(doHideHud:) userInfo:nil repeats:NO];
}

- (IBAction)doHideHud:(id)sender {
    if ( self.hudView != nil ) {
        [self.hudView removeFromSuperview];
        self.hudView = nil;
    }
    
    self.roadNameLabel.alpha=self.townStateNameLabel.alpha=1;;
    self.topTitleGradientImageView.hidden=NO;
    //self.navigationController.tabBarController.tabBar.hidden=NO;
    self.bAcceptedDrivingWarning=YES;
    
    [RWServices startLocationService];
    
    // trying to find out if we own the road or not
    [self doOnRoadUpdate];
    
    
}

- (IBAction)doHideInitialHud:(id)sender {
    if ( self.hudView != nil ) {
        [self.hudView removeFromSuperview];
        self.hudView = nil;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // first time running app, want to show them Quick Help screens
    BOOL bViewedQuickHelp = [prefs integerForKey:@"bViewedQuickHelp"];
    if (bViewedQuickHelp == FALSE) {
        self.hudView = [UIView uiViewFromNibName:@"RWInitialStartupView" owner:self];
        [self.view addSubview:self.hudView];
        
        bViewedQuickHelp = TRUE;
        [prefs setInteger:bViewedQuickHelp forKey:@"bViewedQuickHelp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
       // self.navigationController.tabBarController.tabBar.hidden=NO;
        self.bAcceptedDrivingWarning=YES;
        if ([[RWServices bonusTimerService]running]==YES)
            [self doMotionStart];
        else
            [RWServices startLocationService];
        
        // trying to find out if we own the road or not
        [self doOnRoadUpdate];
        
    }
    
    
}
- (IBAction)doExitApp:(id)sender {
   
    exit(100);
}

- (IBAction)doTabBarSelect:(UIButton*)sender{
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.tabBarController.tabBar.hidden=YES;
    self.navigationController.tabBarController.selectedIndex=sender.tag;
}

-(void)doDisplayMyInventory {
    
    User *me = [RWContext me];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    if (self.currentEarnRate.intValue == kInitializedEarnRate)
        self.footerTotalCoinLabel.text=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:me.coin]];
    else {
        self.footerTotalCoinLabel.text=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:me.coin]];
        self.footerEarnRateLabel.text=[NSString stringWithFormat:@"%d", self.currentEarnRate.intValue];
    }
    self.footerEarnRateLabel.textColor=(self.currentEarnRate.intValue>0)?[UIColor whiteColor]:[UIColor redColor];

    self.turfLabel.text = [numberFormatter stringFromNumber:me.roads];
}

-(void) doOnAccountUpdate:(int)account :(int)coin :(int)driveTime{
    // NSLog (@"onAccountUpdate account <%d> coin <%d> town <%@> road <%@>",account,coin, self.townNameString, self.roadNameString);
    //   if (account==0 || (account > 0 && self.townNameString.length>0  && self.roadNameString.length>0)){
    
    // create a hash for road to pass to server
    int prime = 31;
    //unsigned roadId;
    unsigned townId;
    unsigned stateProvinceId;
    unsigned countryId = 1;
    
    // get from placemarks
    countryId = prime * countryId + [self.countryNameString hash];
    stateProvinceId =  prime * countryId + [self.stateNameString hash];
    townId =  prime *stateProvinceId + [self.townNameString hash];
    self.roadId =  prime *townId + [self.roadNameString hash];
    
    //NSLog (@"country <%@>, countryID <%d> town <%@>, townID <%d> stateProvince <%@>, stateProvinceID <%d> road <%@>, roadID <%d> ",self.countryNameString, countryId, self.townNameString, townId, self.stateNameString, stateProvinceId, self.roadNameString, self.roadId);
    
    CLLocation *cl = [[RWServices locationService] lastLocation];
    NSNumber *lat =[NSNumber numberWithDouble: cl.coordinate.latitude];
    NSNumber *lon =[NSNumber numberWithDouble: cl.coordinate.longitude];
    
    double accelX = [[RWServices accelerometerService]lastSmoothV].x;
    double accelY = [[RWServices accelerometerService]lastSmoothV].y;
    double accelZ = [[RWServices accelerometerService]lastSmoothV].z;
    
    int isFriend = ([self.profilePicture isHidden])? 0 : 1;
    NSLog(@"isFriend <%d>", isFriend);
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          [NSNumber numberWithInt:coin], @"coin",
                          [NSNumber numberWithInt:driveTime], @"driveTime",
                          [NSNumber numberWithInt:account], @"incrementAccount",
                          [NSNumber numberWithInt:self.roadId], @"accountStreetId",
                          self.roadNameString, @"accountStreetName",
                          [NSNumber numberWithInt:townId], @"accountTownId",
                          self.townNameString, @"accountTownName",
                          [NSNumber numberWithInt:stateProvinceId],@"accountStateProvinceId",
                          self.stateNameString, @"accountStateProvinceName",
                          [NSNumber numberWithInt:countryId], @"accountCountryId",
                          self.countryNameString, @"accountCountryName",
                          lat, @"accountStreetLat",
                          lon, @"accountStreetLng",
                          @"no", @"isNewRoad",
                          self.drivingSpeedLabel.text,@"drivingSpeed",
                          self.speedLimitLabel.text,@"speedLimit",
                          self.speedLimitLabel.text,@"speedLimitMQ",
                          [NSNumber numberWithDouble:accelX],@"accelX",
                           [NSNumber numberWithDouble:accelY],@"accelY",
                           [NSNumber numberWithDouble:accelZ],@"accelZ",
                          [NSNumber numberWithInt:isFriend], @"isFriend",
                          nil];
    //NSLog (@"dict <%@>", dict);
   
    [self postNotification:kOnUserAccountUpdate withObject:dict];
    
    //  }
}

-(void) doOnRoadUpdate{
    //NSLog (@"doOnRoadUpdate town <%@>, road <%@>",self.townNameString,self.roadNameString);
    if (self.townNameString.length>0  && self.roadNameString.length>0){
        
        // create a hash for road to pass to server
        int prime = 31;
        //unsigned roadId;
        unsigned townId;
        unsigned stateProvinceId;
        unsigned countryId = 1;
        
        // get from placemarks
        countryId = prime * countryId + [self.countryNameString hash];
        stateProvinceId =  prime * countryId + [self.stateNameString hash];
        townId =  prime *stateProvinceId + [self.townNameString hash];
        self.roadId =  prime *townId + [self.roadNameString hash];
        
       // NSLog (@"country <%@>, countryID <%d> town <%@>, townID <%d> stateProvince <%@>, stateProvinceID <%d> road <%@>, roadID <%d> ",self.countryNameString, countryId, self.townNameString, townId, self.stateNameString, stateProvinceId, self.roadNameString, self.roadId);
        
        CLLocation *cl = [[RWServices locationService] lastLocation];
        NSNumber *lat =[NSNumber numberWithDouble: cl.coordinate.latitude];
        NSNumber *lon =[NSNumber numberWithDouble: cl.coordinate.longitude];
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              [NSNumber numberWithInt:self.roadId], @"accountStreetId",
                              self.roadNameString, @"accountStreetName",
                              [NSNumber numberWithInt:townId], @"accountTownId",
                              self.townNameString, @"accountTownName",
                              [NSNumber numberWithInt:stateProvinceId],@"accountStateProvinceId",
                              self.stateNameString, @"accountStateProvinceName",
                              [NSNumber numberWithInt:countryId], @"accountCountryId",
                              self.countryNameString, @"accountCountryName",
                              lat, @"accountStreetLat",
                              lon, @"accountStreetLng",
                              @"no", @"isNewRoad",
                              
                              nil];
       // NSLog (@"dict <%@>", dict);
        
        [self postNotification:kOnRoadUpdate withObject:dict];
    }
    
}

- (void)onMotionStart:(NSNotification *)notif {
    if (self.bAcceptedDrivingWarning==NO)
        return;
    
    [self performSelector:@selector(doMotionStart) withObject:nil afterDelay:.25];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    if (bUseDarkSystemInfo){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        return UIStatusBarStyleDefault;
    }
    else{
       [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        return UIStatusBarStyleLightContent;
    }
}



- (void)doMotionStart {
   
    // background image for app
    self.streetSignBackgroundView.backgroundColor = [UIColor blackColor];
    
    self.backgroundDayView.hidden=self.backgroundNightView.hidden=self.backgroundCloudImageView.hidden=self.backgroundStarsImageView.hidden=self.topTitleGradientImageView.hidden= YES;
    //[self.navigationController setNavigationBarHidden:YES];
    //self.navigationController.tabBarController.tabBar.hidden=YES;
    self.gpsDebuggingView.hidden=NO;
    self.notDrivingLabelView.hidden=self.notDrivingStatsView.hidden=self.topTitleView.hidden=self.roadOwnerView.hidden=YES;
    self.speedMeterView.hidden=NO;
    self.speedLimitSignView.hidden=NO;
    self.streetSignView.frame=self.originalFrameStreetSign;
    self.footerCoinLabel.hidden=YES;
    self.footerTotalCoinLabel.hidden=NO;
    self.footerView.hidden=YES;
    self.flagView.alpha=1;
    self.moreButton.enabled=self.attackButton.enabled=NO;
    self.moreButton.alpha=self.attackButton.alpha=.2;
    self.topTitleGradientImageView.hidden=NO;
    
    self.lastRefresh = [NSDate date];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        
        [self preferredStatusBarStyle];
    }
    
    

    // self.footerTotalCoinLabel.text=[NSString stringWithFormat:@"%d", self.currentEarnRate.intValue];
    self.accountUpdateTimer=[NSTimer scheduledTimerWithTimeInterval:1.0
                                                             target:self
                                                           selector:@selector(doAccountUpdate:)
                                                           userInfo:nil
                                                            repeats:YES];
    
    
    [RWServices startAccelerometerService];
}


- (void)onMotionStop:(NSNotification *)notif {
    if (self.bAcceptedDrivingWarning==NO)
        return;
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gravel.png"]];
    //[self.navigationController setNavigationBarHidden:NO];
    //self.navigationController.tabBarController.tabBar.hidden=NO;
    
    if (self.bDayTime){
        self.backgroundNightView.hidden=YES;
        self.backgroundDayView.hidden=NO;
        self.backgroundCloudImageView.hidden=(self.profilePicture.isHidden)?NO:YES;
        self.backgroundStarsImageView.hidden=YES;
    } else {
        self.backgroundDayView.hidden=YES;
        self.backgroundNightView.hidden=NO;;
        self.backgroundCloudImageView.hidden=(self.profilePicture.isHidden)?NO:YES;
        self.backgroundStarsImageView.hidden=NO;
    }
    self.quadriant1View.hidden=self.quadriant2View.hidden=self.quadriant3View.hidden=self.quadriant4View.hidden=YES;
    
    self.gpsDebuggingView.hidden=NO;
    self.speedMeterBackground.backgroundColor = [UIColor lightGrayColor];
    self.notDrivingLabelView.hidden=self.notDrivingStatsView.hidden=self.topTitleView.hidden=self.roadOwnerView.hidden= NO;
    /*self.bonusClockView.hidden=self.flagView.hidden=*/self.speedMeterView.hidden=NO;
    //[self.bonusTimeLabel setFont:[UIFont fontWithName:kFontNameSansBold size:50]];
    //[self.bonusTimeChillLabel setFont:[UIFont fontWithName:kFontNameGrunge size:14]];
    self.speedingCostView.hidden=YES;
    self.speedMeter.image = [UIImage imageNamed:@"meter-plate-green.png"];
    self.bCanGetPenalized=YES;
    self.bonusTimeChillLabel.text=@"Bonus Time Today";
    self.bonusClockView.hidden=NO;
    self.streetSignView.hidden=NO;
    self.speedingMessageLabel.textColor=kColorGreen;
    self.drivingSpeedLabel.textColor=kColorGreen;
    self.mphLabel.textColor=kColorGreen;
    self.flagView.alpha=.3;
    self.footerView.hidden=YES;
    self.moreButton.enabled=self.attackButton.enabled=YES;
    self.moreButton.alpha=self.attackButton.alpha=1;
    self.topTitleGradientImageView.hidden=YES;
    
    //self.speedLimitSignView.hidden=YES;
    self.footerCoinLabel.hidden=YES;
    //self.footerTotalCoinLabel.hidden=NO;
    [self doDisplayMyInventory];
    self.drivingSpeedLabel.text = [NSString stringWithFormat:@"%d", 0];
    self.view.backgroundColor=[UIColor clearColor];
    
    [self.accountUpdateTimer invalidate];
    self.accountUpdateTimer=nil;
    
    [self doNotDrivingModeLabels];
    [self doGetSkinny:nil];
    
    [self getMyDailyStats];
       
    [RWServices stopAccelerometerService];
}

- (void) getMyDailyStats{
    [RWAPI getDailyStats:[RWContext me].fbId
                    date:nil
            isAggregated:NO
              completion:^(NSDictionary *result) {
                  //NSLog (@"result from getDailyStats - %@", result);
                  NSDictionary *stats=[[result objectForKey:@"data"]objectForKey:@"stats"];
                 // NSLog (@"stats from getDailyStats - %@", stats);
                  
                  CurrentUser *me = [RWContext me];
                  //NSLog (@"me <%@> me.dailystats <%@>", me, me.userDailyStat);
                  
                  me.userDailyStat.roadsDownDriving=[stats objectForKey:@"roadsDownDriving"];
                  self.todayRoadsLost=me.userDailyStat.roadsDownDriving.intValue;
                  //self.todayRoadsLostLabel.text = [NSString stringWithFormat:@"-%d",  me.userDailyStat.roadsDownDriving.intValue];
                  
                  me.userDailyStat.roadsUpAttack = [stats objectForKey:@"roadsUpAttack"];
                  self.todayRoadsCapturedAttack= me.userDailyStat.roadsUpAttack.intValue;
                  //self.todayRoadsCapturedAttackLabel.text = [NSString stringWithFormat:@"+%d",  me.userDailyStat.roadsUpAttack.intValue];
                  
                  me.userDailyStat.roadsDownAttack=[stats objectForKey:@"roadsDownAttack"];
                 // NSNumber *totalAttack =[stats objectForKey:@"totalAttack"];
                  //me.userDailyStat.roadsDownAttack= [NSNumber numberWithInt:( totalAttack.intValue-me.userDailyStat.roadsUpAttack.intValue)];
                 
                  self.todayRoadsLostAttack=me.userDailyStat.roadsDownAttack.intValue;
                  //self.todayRoadsLostAttackLabel.text = [NSString stringWithFormat:@"-%d",  me.userDailyStat.roadsDownAttack.intValue];
                  
                  me.userDailyStat.coinUpTolls=[stats objectForKey:@"coinUpTolls"];
                  self.todayTollIn=me.userDailyStat.coinUpTolls.intValue;
                  //self.todayTollInLabel.text = [NSString stringWithFormat:@"+%d",  me.userDailyStat.coinUpTolls.intValue];
                  
                  me.userDailyStat.coinDownTolls=[stats objectForKey:@"coinDownTolls"];;
                  self.todayTollOut=me.userDailyStat.coinDownTolls.intValue;
                  //self.todayTollOutLabel.text = [NSString stringWithFormat:@"-%d",  me.userDailyStat.coinDownTolls.intValue];
                  
                  me.userDailyStat.totalAttack=[stats objectForKey:@"totalAttack"];
                  me.userDailyStat.driveTime=[stats objectForKey:@"driveTime"];
                  
                  me.userDailyStat.coinUpDriving=[stats objectForKey:@"coinUpDriving"];;
                  self.todayCoinEarned= me.userDailyStat.coinUpDriving.intValue;
                  NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                  [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                  
                  self.todayCoinEarnedLabel.text=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[ NSNumber numberWithInt: me.userDailyStat.coinUpDriving.intValue + me.userDailyStat.coinUpTolls.intValue -me.userDailyStat.coinDownTolls.intValue]]];
                  //self.todayCoinEarnedLabel.text = [NSString stringWithFormat:@"%d", me.userDailyStat.coinUpDriving.intValue + me.userDailyStat.coinUpTolls.intValue -me.userDailyStat.coinDownTolls.intValue];
                  
                  me.userDailyStat.roadsUpDriving=[stats objectForKey:@"roadsUpDriving"];
                  self.todayRoadsCaptured = me.userDailyStat.roadsUpDriving.intValue;
                  self.todayRoadsCapturedLabel.text=[NSString stringWithFormat:@"%d",  me.userDailyStat.roadsUpDriving.intValue - me.userDailyStat.roadsDownDriving.intValue + me.userDailyStat.roadsUpAttack.intValue - me.userDailyStat.roadsDownAttack.intValue];
                  
                  [self doNotDrivingModeLabels];
                  //NSLog (@"2.me <%@> me.dailystats <%@>", me, me.userDailyStat);
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [RWContext commit];
                  });
                  
              }
     
                 failure:^(NSError *error) {
                     // do anything on failure?
                     NSLog (@"error from getDailyStats - %@", error);
                 }];
    
}

- (void) doGetSkinny:(NSNotification *)notif {
    [RWAPI getSkinny: ^(NSDictionary * skinny) {
        //NSLog (@"results from getSKinny <%@>",skinny);
        NSArray *skinnyList = [[skinny objectForKey:@"data"] objectForKey:@"skinny"];
        //NSLog(@"skinnylist <%@>",skinnyList);
        NSString *append;
        for (NSDictionary *details in skinnyList){
            NSString *detailSkinny = [details objectForKey:@"details"];
            append=@"";
            NSString *detailSkinnyAppended = [detailSkinny stringByAppendingString:append];
            [RWContext addSkinnyMessage:[NSString stringWithFormat:@"%@", detailSkinnyAppended] appendTime:NO];
            [[NSUserDefaults standardUserDefaults] setObject:[RWContext skinny] forKey:@"skinny" ];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        [RWAPI wipeSkinny];
    } failure:^(NSError *error) {
        // sigh... what to do?
        NSLog (@"skinny result error <%@>", error);
    }];
    
}


- (void) saveTodayData {
    
    
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayCoinEarned forKey:@"todayCoinEarned"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayIncidents forKey:@"todayIncidents"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayRoadsCaptured forKey:@"todayRoadsCaptured"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayRoadsCapturedAttack forKey:@"todayRoadsCapturedAttack"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.weeklyRoadsCapturedAttack forKey:@"weeklyRoadsCapturedAttack"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayDrivingTime forKey:@"todayDrivingTime"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.weeklyDrivingTime forKey:@"weeklyDrivingTime"];
    
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayRoadsLost forKey:@"todayRoadsLost"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayRoadsLostAttack forKey:@"todayRoadsLostAttack"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayTotalAttack forKey:@"todayTotalAttack"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.weeklyTotalAttack forKey:@"weeklyTotalAttack"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayTollIn forKey:@"todayTollIn"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayTollOut forKey:@"todayTollOut"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.todayReportedSpeedLimits forKey:@"todayReportedSpeedLimits"];
    int seconds_elapsed = [[RWServices bonusTimerService]seconds_elapsed];//[[RWServices bonusTimerService]currentElapsedSeconds]+[[RWServices bonusTimerService]seconds_elapsed];
    //NSLog (@"SaveTodayData seconds_lapsed <%d>", seconds_elapsed);
    //NSLog (@"SaveTodayData currentElapsedSeconds <%d>", [[RWServices bonusTimerService]currentElapsedSeconds]);

    [[NSUserDefaults standardUserDefaults] setInteger:seconds_elapsed forKey:@"seconds_elapsed"];
    [[NSUserDefaults standardUserDefaults] setObject:self.serverParameters forKey:@"serverParameters" ];
    [[NSUserDefaults standardUserDefaults] setObject:[RWContext skinny] forKey:@"skinny" ];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // day or night?
    CLLocation *cl = [[RWServices locationService] lastLocation];
    FESSolarCalculator *solarCalculator = [[FESSolarCalculator alloc] initWithDate:[NSDate date] location:cl];
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = @"HH:mm:ss";
    // set the labels
    int sunriseHour = [df stringFromDate:solarCalculator.sunrise].intValue;
    int sunsetHour = [df stringFromDate:solarCalculator.sunset].intValue;
    int nowTime = [df stringFromDate:solarCalculator.startDate].intValue;
    //NSLog (@"Sunrise %@ -- sunset <%@>", [df stringFromDate:solarCalculator.sunrise],[df stringFromDate:solarCalculator.sunset] );
    //NSLog (@"INT - Sunrise <%d> -- sunset <%d> - now <%d>", sunriseHour, sunsetHour, nowTime );
    
    
    if (nowTime>=sunriseHour && nowTime <= sunsetHour){
        if ([self.moreButton isEnabled]){
            self.backgroundNightView.hidden=YES;
            self.backgroundDayView.hidden=self.backgroundCloudImageView.hidden=NO;;
            self.backgroundStarsImageView.hidden=YES;
        }
        self.bDayTime=YES;
    } else {
        if ([self.moreButton isEnabled]){
            self.backgroundDayView.hidden=YES;
            self.backgroundNightView.hidden=self.backgroundCloudImageView.hidden=NO;;
            self.backgroundStarsImageView.hidden=NO;
        }
        self.bDayTime=NO;
        
    }

}

- (void) doCheckIfDayChanged{
    //NSLog (@"Observer - doCheckIfDayChanged");
    
    // did day change
    NSDate *currDate = [NSDate date];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDate *saveDate = [userDefaults objectForKey:@"saveDate"];
    //NSLog (@"saveDate <%@>", saveDate);
    
    if (saveDate != nil) {
        [self getMyDailyStats];
        
        self.serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
        
        // reset skinny list
        [[RWContext skinny]removeAllObjects];
        [[RWContext skinny]addObjectsFromArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"skinny"]];
       // NSLog (@"skinny1 <%@>", [[NSUserDefaults standardUserDefaults] objectForKey:@"skinny"]);
       // NSLog (@"skinny <%@>", [RWContext skinny]);
        
        self.todayReportedSpeedLimits = [[NSUserDefaults standardUserDefaults] integerForKey:@"todayReportedSpeedLimits"];
        //NSLog (@"todaysrpeorted <%d>", self.todayReportedSpeedLimits);
        
        self.todayIncidents =[[NSUserDefaults standardUserDefaults] integerForKey:@"todayIncidents"];
       
        int seconds_elapsed = [[NSUserDefaults standardUserDefaults] integerForKey:@"seconds_elapsed"];
        NSLog (@"seconds_elapsed <%d>", seconds_elapsed);
        
        NSNumber *numTotalAttacksToday = [[NSUserDefaults standardUserDefaults] objectForKey:@"todayTotalAttack"];
        //NSLog (@"numTotalAttacksToday <%d>", numTotalAttacksToday.intValue);
        self.todayTotalAttack=numTotalAttacksToday.intValue;
        
        /*debugging....
        if (seconds_elapsed<1590)
         seconds_elapsed = 590;
        */
        
        [[RWServices bonusTimerService]resetBonusTimer];
        [[RWServices bonusTimerService]stop];
        [[RWServices bonusTimerService]setSeconds:seconds_elapsed];
 
        RWBonusTimerService *bonusTimer = [RWServices bonusTimerService];
        NSString *bonusTime;
        if ([[bonusTimer stringHours]intValue])
            bonusTime = [NSString stringWithFormat:@"%@:%@:%@", [bonusTimer stringHours], [bonusTimer stringMinutes], [bonusTimer stringSeconds]];
        else
            bonusTime = [NSString stringWithFormat:@"%@:%@", [bonusTimer stringMinutes], [bonusTimer stringSeconds]];
        
        // Display bonus time
        self.bonusTimeLabel.text= bonusTime;
        
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSDayCalendarUnit | kCFCalendarUnitWeekday ) fromDate:currDate];
        //NSLog (@"currDate day component %@",components);
        NSInteger currDay = [components day];
        //NSInteger currDayofWeek = [components weekday];
        //NSLog (@"current day of week is %d", currDayofWeek);
        
        components = [[NSCalendar currentCalendar] components:(NSDayCalendarUnit ) fromDate:saveDate];
        // NSLog (@"saveDate day component %@",components);
        NSInteger saveDay = [components day];
        
        if (saveDay!=currDay){
            NSLog (@"saveDay!=currDay, reset all");
            /*
            // delete the debugging logfile - BR: do not delete for now while debugging bonus reset
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *logPath = [documentsDirectory
                                 stringByAppendingPathComponent:@"console.log"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtPath:logPath error:NULL];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey: DEBUGGING_PREF_KEY])
                [self redirectConsoleLogToDocumentFolder];
            */
            // reset today's values
            self.todayCoinEarned=self.todayIncidents=self.todayRoadsCaptured=self.todayRoadsCapturedAttack=self.todayRoadsLost=self.todayRoadsLostAttack=self.todayTollIn=self.todayTollOut=self.todayTotalAttack=self.todayReportedSpeedLimits=0;
            
            // reset bonus timer
            if ([[RWServices bonusTimerService]running]==YES)
                [[RWServices bonusTimerService]stop];
            [[RWServices bonusTimerService]resetBonusTimer];
            
            self.bonusTimeLabel.text = [NSString stringWithFormat:@"%@:%@", [[RWServices bonusTimerService] stringMinutes], [[RWServices bonusTimerService] stringSeconds]];
            
            // reset today's bonus count
            [RWContext me].numBonusRecieved=[NSNumber numberWithInt:0];
            
            // reset attack level
            [RWContext me].attackLevel=[NSNumber numberWithInt:numGames];
            [RWContext commit];
            
            // reset skinny list
            [[RWContext skinny]removeAllObjects];
            
            // save to user defaults
            // Saving current date
            [[NSUserDefaults standardUserDefaults] setObject:currDate forKey:@"saveDate"];
            [self saveTodayData];
            
         }
        
              
        
    } else {
         // Saving current date
        NSLog (@"Save date was null, so initially save date");
        [[NSUserDefaults standardUserDefaults] setObject:currDate forKey:@"saveDate"];
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];

}
- (void) onGoingToForeground:(NSNotification *)notif {
    NSLog (@"Observer - onGoingToForeground");
    [self doCheckIfDayChanged];
    if (self.bGotDeductionGoingToBackground){
        self.bGotDeductionGoingToBackground=NO;
        [[RWServices audioService ]playSoundEvent:kSoundEventLoseBonusTime:soundFromGame];
        [self doShowHud:@"30 MINUTE PENALTY" :@"DON'T USE PHONE WHILE DRIVING" :@"":[UIImage imageNamed:@"busted-phone.png"]: nil]; // BR: server param
        
    }
   
}

- (void) onGoingToBackground:(NSNotification *)notif {
    NSLog (@"Observer - onGoingToBackground");
    if (self.notDrivingStatsView.isHidden){ // means I'm driving
       // [[RWServices bonusTimerService]resetBonusTimer];
        [[RWServices bonusTimerService]bustedDeductionBonusTimer];
        self.bGotDeductionGoingToBackground=TRUE;
    }
    self.bReportedSpeedLimitWarning=NO;
    [self saveTodayData];
    
}
-(void) onInternetReachable:(NSNotification *)notif {
  
    if (self.player==nil){ // animation done playing
        [RWServices stopLocationService];
        [RWServices startLocationService];
    }
        
    self.townStateNameLabel.text = @"Connectivity restored";
  
}
-(void) onInternetNotReachable:(NSNotification *)notif {
    self.drivingStateBeforeConnectivityLoss = ([[RWServices locationService]bDriving])?kDrivingState:kNotDrivingState;
    
  
    if ([[RWServices bonusTimerService]running]==YES)
        [[RWServices bonusTimerService]stop];
   
    self.townStateNameLabel.text = @"Waiting for internet...";
    
}

- (void) doCanGetBusted{
    //NSLog(@"doAllowBusting");
    self.bCanGetBusted=YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
     [super touchesBegan:touches withEvent:event];
        
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(((UITouch *)[touches anyObject]).tapCount == 1){ // use one tap to try to debug connectivity issues
        [self networkType];
        
    }
    else if(((UITouch *)[touches anyObject]).tapCount == 2){
       // NSLog(@"DOUBLE TOUCH");
        [self saveTodayData];
        if (self.notDrivingStatsView.hidden){ // report the speed limit if driving
            
            if (! self.bReportedSpeedLimitWarning){
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Report Speed Limit"
                                                            message:@"This feature should only be used by passengers, for safety. What are you?"
                                                           delegate:self
                                                  cancelButtonTitle:@"Driver"
                                                  otherButtonTitles:@"Passenger",nil];
            [alert show];
            }
            else
                 [self performSegueWithIdentifier:kSegueReportSpeedLimit sender:nil];
          
        }
        else
            [self doAttackPlay:nil];
       
    }
    [super touchesEnded:touches withEvent:event];
}

- (void) doCheckIfMoving:(id)sender {
  //  if (self.notDrivingStatsView.hidden){
        CLLocation *updatedLocation=[[RWServices locationService]lastLocation];
        
       // NSLog (@"updated <%f> self <%f>", updatedLocation.timestamp.timeIntervalSince1970, self.lastTimeStamp.timeIntervalSince1970);
        if (updatedLocation.timestamp.timeIntervalSince1970==self.lastTimeStamp.timeIntervalSince1970){
            //[self postNotification:kOnMotionStop withObject:nil];
            [RWServices stopLocationService];
            [RWServices startLocationService];
        }
        self.lastTimeStamp=updatedLocation.timestamp;
  //  }
    
}

-(void) onWinAttack:(NSNotification *)notif {
    //NSLog (@"onWinAttack");
    self.todayTotalAttack+=1;
    self.weeklyTotalAttack+=1;
    self.weeklyRoadsCapturedAttack+=1;
    self.todayRoadsCapturedAttackLabel.text = [NSString stringWithFormat:@"%d", ++self.todayRoadsCapturedAttack];
    [RWContext me].attackLevel=[NSNumber numberWithInt:[RWContext me].attackLevel.intValue+1];
    [RWContext me].roads=[NSNumber numberWithInt:[RWContext me].roads.intValue+1];
    [RWContext commit];
    //NSLog (@"self.todayRoadsCapturedAttack <%d>", self.todayRoadsCapturedAttack);
    [self saveTodayData];
    
}
// keep track of me losing attack playing game
-(void) onLoseAttackMe:(NSNotification *)notif {
    //NSLog (@"onLoseAttackMe");
    self.todayTotalAttack+=1;
   // NSLog (@"self.todayTotalAttack <%d>", self.todayTotalAttack);
    self.weeklyTotalAttack+=1;
    [self saveTodayData];
}

// lose roads from someone attacking me
-(void) onLoseAttack:(NSNotification *)notif {
    //NSLog (@"onLoseAttack");
    //self.todayRoadsLostAttackLabel.text = [NSString stringWithFormat:@"-%d", ++self.todayRoadsLostAttack];
 //   [self saveTodayData];
}

-(void) onTollsIn:(NSNotification *)notif {
   // NSLog (@"onTollsIn");
   // self.todayTollInLabel.text = [NSString stringWithFormat:@"+%d", ++self.todayTollIn];
    //[self saveTodayData];
}
-(void) onTollsOut:(NSNotification *)notif {
   // NSLog (@"onTollsOut");
   // self.todayTollOutLabel.text = [NSString stringWithFormat:@"-%d", ++self.todayTollOut];
   // [self saveTodayData];
}
-(void) onLoseRoad:(NSNotification *)notif {
   // NSLog (@"onLoseRoad");
   // self.todayRoadsLostLabel.text = [NSString stringWithFormat:@"-%d", ++self.todayRoadsLost];
  //  [self saveTodayData];
}

-(void) onInitializeRoadWarsServer:(NSNotification *)notif {
  //  NSLog (@"onInitializeRoadWarsServer");
    CurrentUser *me = [RWContext me];
    
    [RWAPI initialize: ^(NSDictionary * result) {
       //NSLog (@"results from initialize <%@>",result);
        self.serverParameters = [[result objectForKey:@"data"]objectForKey:@"settings"];
       // NSLog (@"server paramaters <%@>", self.serverParameters);
        self.currentEarnRate = [self.serverParameters objectForKey:@"yourRoadEarnRate"];
        self.currentEarnRateInGreenZone = [self.serverParameters objectForKey:@"yourRoadEarnRate"];
        [[NSUserDefaults standardUserDefaults] setObject:self.serverParameters forKey:@"serverParameters" ];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSDictionary *userData=[[result objectForKey:@"data"]objectForKey:@"user"];
        
        //NSLog (@"Userdata %@", userData);
        
        NSNumber *toll = [[result objectForKey:@"data"]objectForKey:@"toll"];
        if (toll != nil && toll.intValue!=0)
            [self postNotification:kOnTollsOut withObject:result];
        
        me.fbId = [userData objectForKey:@"fbId"];
        me.name = [userData objectForKey:@"name"];
        me.first= [userData objectForKey:@"first"];
        me.last = [userData objectForKey:@"last"];
        me.coin = [userData objectForKey:@"coin"];
        me.dailyTake = [userData objectForKey:@"dailyTake"];
        me.roads = [userData objectForKey:@"streets"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [RWContext commit];
        });
        // for now, give folks some coin if they have none
        NSNumber *welcomeToGameCoinBonus = [self.serverParameters objectForKey:@"welcomeToGameCoinBonus"];
        // NSLog (@"welcomeToGameCoinBonus <%d>", welcomeToGameCoinBonus.intValue);
        
        // NSLog(@"[RWContext me] %@",[RWContext me]);
        if ([RWContext me].coin.intValue<=0){
            int increment=welcomeToGameCoinBonus.intValue;
            
            increment = fabs([RWContext me].coin.doubleValue)+welcomeToGameCoinBonus.intValue;
            [self doOnAccountUpdate:0 :increment :0];
            [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Welcome to RoadWars %d coin bonus - %@", increment, self.townStateNameLabel.text] appendTime:TRUE];
        }
    } failure:^(NSError *error) {
        // sigh... what to do?
        self.serverParameters=[[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    }];
    

}

-(void) onReportSpeedLimit:(NSNotification *)notif {
   // NSLog (@"onReportSpeedLimit");
    
    UIButton *b = notif.object; // button that was pushed, and tag will have value
    CLLocation *updatedLocation=[[RWServices locationService]lastLocation];
   
    self.bReportedSpeedLimit=YES;
    self.speedLimitSignView.layer.borderColor=kColorBlueBusted.CGColor;
    // local commit
    SpeedLimitsReported *sl = (SpeedLimitsReported *)[RWContext newObject:kModelSpeedLimitsReported];
    sl.lat=[NSNumber numberWithFloat: updatedLocation.coordinate.latitude];
    sl.lon=[NSNumber numberWithFloat: updatedLocation.coordinate.longitude];
    sl.roadId=[NSNumber numberWithInt:self.roadId];
    sl.road=self.roadNameString;
    sl.currentDrivingSpeed=[NSNumber numberWithInt:self.drivingSpeedLabel.text.intValue];
    //sl.currentMQSpeed=[NSNumber numberWithInt:self.speedLimitLabel.text.intValue];
    sl.currentMQSpeed=self.currentMqSpeedLimit;
    // NSLog (@"in report - self.currentMqSpeedLimit %@", self.currentMqSpeedLimit);
    sl.currentReportedSpeed=[NSNumber numberWithInt:b.tag];
    
     NSNumber *speedLimitBonusAmount = [self.serverParameters objectForKey:@"speedLimitBonusAmount"];
     NSNumber *speedLimitReportingPerDay = [self.serverParameters objectForKey:@"speedLimitReportingPerDay"];
     NSNumber *speedLimitMaxChange = [self.serverParameters objectForKey:@"speedLimitMaxChange"];
    
    //NSLog (@" bonus amount <%@>, reportsperday <%@>, maxchange <%@> ", speedLimitBonusAmount,speedLimitReportingPerDay,speedLimitMaxChange );
    if (!bMQRunningOk ||  sl.currentReportedSpeed.intValue-sl.currentMQSpeed.intValue <= speedLimitMaxChange.intValue){
        self.speedLimitLabel.text=sl.currentReportedSpeed.stringValue;
        sl.heading=[RWServices locationService].heading;
        [RWContext commit];
        [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Reported %@mph limit on %@ vs. %@mph",sl.currentReportedSpeed,sl.road,sl.currentMQSpeed] appendTime:TRUE];
        if (self.todayReportedSpeedLimits< speedLimitReportingPerDay.intValue){
            [self doOnAccountUpdate:0:speedLimitBonusAmount.intValue:0]; // bonus for reporting speed limit BR: server param
            [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Bonus %@ for reporting speed limit.", speedLimitBonusAmount] appendTime:TRUE];
            self.todayReportedSpeedLimits+=1;
            //NSLog (@"todayReportedSpeedLimits %d",self.todayReportedSpeedLimits);
            [self saveTodayData];
            
        }
        // server commit
        [RWAPI reportInaccuracyLat:updatedLocation.coordinate.latitude lng:updatedLocation.coordinate.longitude streetId:[NSNumber numberWithInt:self.roadId] streetName:self.roadNameString speedLimit:self.speedLimitLabel.text.intValue direction:[RWServices locationService].heading andUserSpeedLimit:b.tag completion: ^(NSDictionary * result) {
            //NSLog (@"results from onReportedSpeedLimit <%@>",result);
            
        } failure:^(NSError *error) {
            // sigh... what to do?
        }];
        

    } else {
        [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Reported %@ speedlimit vs. %@ not allowed on %@ ",sl.currentReportedSpeed,sl.currentMQSpeed,sl.road] appendTime:TRUE];
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Report Speed Limit Error"
                                                        message:@"Speed Limit change would exceed maximum allowed"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
    
}

-(void) doNotDrivingModeLabels{
   // NSLog(@"self.todayCoinEarned %d , self.todayIncidents %d, self.todayTollIn  %d, self.todayTollOut %d", self.todayCoinEarned , self.todayIncidents, self.todayTollIn , self.todayTollOut);
    /*
    self.todayCoinEarnedLabel.alpha=(self.todayCoinEarned==0)?.5:1;
    self.todayIncidentsLabel.alpha=(self.todayIncidents==0)?.5:1;
    self.todayTollInLabel.alpha=(self.todayTollIn==0)?.5:1;
    self.todayTollOutLabel.alpha=(self.todayTollOut==0)?.5:1;
    
    self.todayRoadsCapturedLabel.alpha=(self.todayRoadsCaptured==0)?.5:1;
    self.todayRoadsLostLabel.alpha=(self.todayRoadsLost==0)?.5:1;
    self.todayRoadsCapturedAttackLabel.alpha=(self.todayRoadsCapturedAttack==0)?.5:1;
    self.todayRoadsLostAttackLabel.alpha=(self.todayRoadsLostAttack==0)?.5:1;
     */
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:kSegueAttackToAttackPlay] ) {
        
        RWAttackPlayViewController *attackController = [segue destinationViewController];
        attackController.user = (User *)self.roadOwner;
        attackController.roadId = self.roadId;
        attackController.totalAttacks = self.todayTotalAttack;
        self.navigationController.navigationBar.hidden=NO;
        self.navigationController.tabBarController.tabBar.hidden=YES;
        
    }
    else  if ( [[segue identifier] isEqualToString:kSegueReportSpeedLimit] ) {
        
        RWReportSpeedLimitViewController *reportController = [segue destinationViewController];
        reportController.postedSpeedLimit=[NSNumber numberWithInt:self.speedLimitLabel.text.intValue] ;
        reportController.townName=self.townNameString;
        reportController.roadName=self.roadNameString;
       
    }
    else  if ( [[segue identifier] isEqualToString:kSegueNavToAttack] || [[segue identifier] isEqualToString:kSegueNavToAbout] ) {
        
        self.navigationController.navigationBar.hidden=NO;
        self.navigationController.tabBarController.tabBar.hidden=YES;
        
        
    }
   
}

- (IBAction)doAttackPlay:(UIButton*)sender {
     
    //NSLog (@"doAttackPlay");
    self.lastAttackButton=sender;
    if (self.roadOwner.name != nil/* && self.roadOwner != [RWContext me]*/)
        [self performSegueWithIdentifier:kSegueAttackToAttackPlay sender:sender];
    else
        NSLog (@"Either no one owns road or I own it");
}

- (IBAction)onIssueAttack:(UIButton*)sender {
   // NSLog (@"onIssueAttack");
    [self performSegueWithIdentifier:kSegueAttackToAttackPlay sender: self.lastAttackButton];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		//NSLog(@"user pressed YES");
        self.bReportedSpeedLimitWarning=YES;
        [self performSegueWithIdentifier:kSegueReportSpeedLimit sender:nil];
    }else {
		//NSLog(@"user pressed NO");
        [self.navigationController popToRootViewControllerAnimated:YES];
        
	}
}

- (void)setMidnightTimer {
    NSDateComponents *dc = [[NSCalendar currentCalendar] components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    [dc setDay:dc.day + 1];
    NSDate *midnightDate = [[NSCalendar currentCalendar] dateFromComponents:dc];
    NSLog(@"Now: %@, Tonight Midnight: %@, Hours until midnight: %.1f", [NSDate date], midnightDate, [midnightDate timeIntervalSinceDate:[NSDate date]] / 60.0 / 60.0);
}

- (void) redirectConsoleLogToDocumentFolder // for concole log file
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory
                         stringByAppendingPathComponent:@"console.log"];
    
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}

-(IBAction)handleSwipeGesture:(UISwipeGestureRecognizer*)sender
{
    UIImage *driveSnapshot = [self uikitScreenshot];
       
    CGSize size;
    size.width=320;
    size.height=500;
    UIGraphicsBeginImageContext(size);
    [driveSnapshot drawInRect:CGRectMake(0, 0, size.width, size.height)];
    self.driveSnapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *data;
    NSArray *paths;
    NSString *documentsDirectory,*imagePath ;
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString   stringWithFormat:@"driveSnapshot.png"]];
    data = UIImagePNGRepresentation( self.driveSnapshot);
    [data writeToFile:imagePath atomically:YES];

    [self doShowDebugging];
    return;
    
    
}

- (UIImage *)uikitScreenshot
{
    // Create a graphics context with the target size
    // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
    // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
    CGSize imageSize = [[UIScreen mainScreen] bounds].size;
    if (NULL != UIGraphicsBeginImageContextWithOptions)
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
    else
        UIGraphicsBeginImageContext(imageSize);
	
    CGContextRef context = UIGraphicsGetCurrentContext();
	
    //
    // Iterate over every window from back to front.
    //
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        if (![window respondsToSelector:@selector(screen)] || [window screen] == [UIScreen mainScreen])
        {
            //
            // -renderInContext: renders in the coordinate space of the layer,
            // so we must first apply the layer's geometry to the graphics context.
            //
            CGContextSaveGState(context);
            
            
            //
            // Center the context around the window's anchor point.
            //
            CGContextTranslateCTM(context, [window center].x, [window center].y);
            
            
            //
            // Apply the window's transform about the anchor point.
            //
            CGContextConcatCTM(context, [window transform]);
            
            
            //
            // Offset by the portion of the bounds left of and above the anchor point.
            //
            CGContextTranslateCTM(context,
                                  -[window bounds].size.width * [[window layer] anchorPoint].x,
                                  -[window bounds].size.height * [[window layer] anchorPoint].y);
			
            
            //
            // Render the layer hierarchy to the current context.
            //
            [[window layer] renderInContext:context];
			
            
            //
            // Restore the context.
            //
            CGContextRestoreGState(context);
        }
    }
	
    
    //
    // Retrieve the screenshot image.
    //
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	
    UIGraphicsEndImageContext();
	
    return image;
}



- (void)doShowDebugging{
    if ( self.hudView == nil ) {
        self.hudView = [UIView uiViewFromNibName:@"RWDebuggingView" owner:self];
        [self.view addSubview:self.hudView];
    }
    else //only one hud at a time
        return;
    
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(doHideHud:) userInfo:nil repeats:NO];
}

- (IBAction)doSendEmailReport:(UIButton *)sender {
    if ( self.hudView != nil ) {
        [self.hudView removeFromSuperview];
        self.hudView = nil;
    }
   
    int whichButton;
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton* clickedButton = (UIButton*)sender;
        whichButton=clickedButton.tag;
        
    }
    else
        whichButton=0 ;
    
    NSArray *problemTypeStringArray = [NSArray arrayWithObjects:@"Daily Report", @"Accelerometer",@"Other", nil];
    //NSLog (@"problemType is <%d><%@>", whichButton, [problemTypeStringArray objectAtIndex:whichButton]);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory
                         stringByAppendingPathComponent:@"console.log"];
    
    NSData *fileData = [NSData dataWithContentsOfFile: logPath];
    if (fileData==nil && whichButton!=0){
        [self doShowHud:@"No Log Data to Send":@"":@"":nil:nil];
        return;
    }
    
    NSString* logfileStr = [NSString stringWithUTF8String:[fileData bytes]];

    NSString *driveSnapshot = [documentsDirectory
                         stringByAppendingPathComponent:@"driveSnapshot.png"];
    fileData = [NSData dataWithContentsOfFile: driveSnapshot];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:[problemTypeStringArray objectAtIndex:whichButton]];
        
        struct utsname u;
        uname(&u);
        NSString *nameString = [NSString stringWithFormat:@"%s", u.machine];
        
        NSString *s = [NSString stringWithFormat:@"%@\n\n%@\n%@\n%@\n%@", logfileStr, nameString, [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
        
        [controller setMessageBody:s isHTML:NO];
        
        NSArray *recipients = [NSArray arrayWithObjects:@"support@roadwars.com", nil];
        [controller setToRecipients:recipients];
        
        [controller addAttachmentData:fileData mimeType:@"image/png" fileName:driveSnapshot];
        [self presentModalViewController:controller animated:YES];
        
    }

}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissModalViewControllerAnimated:YES];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory
                         stringByAppendingPathComponent:@"console.log"];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:logPath error:NULL];
    if ([[NSUserDefaults standardUserDefaults] boolForKey: DEBUGGING_PREF_KEY])
        [self redirectConsoleLogToDocumentFolder];

}
- (void) movieFinishedCallback:(NSNotification*) aNotification {
    MPMoviePlayerController *player = [aNotification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    player=nil;
    self.player=nil;
    [self finishRoadWarsStartup];
    
   
}

-(void) finishRoadWarsStartup {
    [self doShowInitialDrivingWarning];
    
    self.tabBarController.selectedIndex=0; // road wars today
    self.navigationController.tabBarController.tabBar.hidden=YES;
    
    self.notDrivingLabelView.hidden=self.notDrivingStatsView.hidden=NO;
    self.streetSignView.hidden=NO;
    self.footerView.hidden=YES;
    self.bonusClockView.hidden=NO;
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(doCheckIfMoving:) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(doCheckIfDayChanged) userInfo:nil repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(doOneGeocode) userInfo:nil repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(saveTodayData) userInfo:nil repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(saveTodayData) userInfo:nil repeats:YES];
    
    self.lastTimeStamp=[[RWServices locationService]lastLocation].timestamp;
    // see if there is a system message
    [RWAPI checkForSystemMessage: ^(NSDictionary * systemMessage) {
        NSNumber *success =  [systemMessage objectForKey:@"success"];
        if (success.intValue==1){
            NSLog (@"display message to user <%@>",[systemMessage objectForKey:@"message"]);
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Road Wars Alert"
                                                            message:[systemMessage objectForKey:@"message"]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else
            NSLog(@"No message for user");
        
    } failure:^(NSError *error) {
        // sigh... what to do?
    }];
    

}
- (void) hidecontrol {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:self.player.moviePlayer];
    [self.player.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
    
}

-(void)networkType {
    
    NSArray *subviews = [[[[UIApplication sharedApplication] valueForKey:@"statusBar"] valueForKey:@"foregroundView"]subviews];
    NSNumber *dataNetworkItemView = nil;
    
    for (id subview in subviews) {
        if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }
    
    
    switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"]integerValue]) {
        case 0:
            NSLog(@"NETWORK->No wifi or cellular");
            break;
            
        case 1:
            NSLog(@"NETWORK->2G");
            break;
            
        case 2:
            NSLog(@"NETWORK->3G");
            break;
            
        case 3:
            NSLog(@"NETWORK->4G");
            break;
            
        case 4:
            NSLog(@"NETWORK->LTE");
            break;
            
        case 5:
            NSLog(@"NETWORK->Wifi");
            break;
            
            
        default:
             NSLog(@"NETWORK->UNKNOWN (oh what to do)");
            break;
    }
}
@end

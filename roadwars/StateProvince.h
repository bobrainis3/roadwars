//
//  StateProvince.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Area.h"

@class Country, Road, Town;

@interface StateProvince : Area

@property (nonatomic, retain) Country *country;
@property (nonatomic, retain) NSSet *towns;
@property (nonatomic, retain) NSSet *roads;
@end

@interface StateProvince (CoreDataGeneratedAccessors)

- (void)addTownsObject:(Town *)value;
- (void)removeTownsObject:(Town *)value;
- (void)addTowns:(NSSet *)values;
- (void)removeTowns:(NSSet *)values;

- (void)addRoadsObject:(Road *)value;
- (void)removeRoadsObject:(Road *)value;
- (void)addRoads:(NSSet *)values;
- (void)removeRoads:(NSSet *)values;

@end

//
//  RWAttackPlayViewController.h
//  roadwars
//
//  Created by Bob Rainis on 1/11/13.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWAttackPlayViewController.h"
#import "RWAPI.h"
#import "User.h"
#import <FacebookSDK/FBProfilePictureView.h>
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWContext.h"
#import "CurrentUser.h"
#import "RWServices.h"
#import "RWAudioService.h"
#import "DBFBProfilePictureView.h"

@interface RWAttackPlayViewController ()

@end

@implementation RWAttackPlayViewController
@synthesize profilePictureMe = _profilePictureMe;
@synthesize meName = _meName;
@synthesize meRoadName = _meRoadName;
@synthesize meRoadExtId = _meRoadExtId;
@synthesize meTownStateName = _meTownStateName;
@synthesize profilePictureAttackee = _profilePictureAttackee;
@synthesize attackeeName = _attackeeName;
@synthesize attackeeRoadName = _attackeeRoadName;
@synthesize attackeeRoadExtId = _attackeeRoadExtId;
@synthesize attackeeTownStateName = _attackeeTownStateName;
@synthesize dice1=_dice1, dice2=_dice2;
@synthesize winnerLabelFriend=_winnerLabelFriend, winnerLabelMe=_winnerLabelMe;
@synthesize user = _user;
@synthesize bIsAnimating=_bIsAnimating;
@synthesize tapToStartView= _tapToStartView;
@synthesize dice1Value=_dice1Value, dice2Value=_dice2Value;
@synthesize versusLabel=_versusLabel;
@synthesize diceTimer=_diceTimer;
@synthesize diceTimer2=_diceTimer2;
@synthesize diceNumber=_diceNumber;
@synthesize attackCost=_attackCost;
@synthesize defendingRoadOwnerAccount=_defendingRoadOwnerAccount;
@synthesize attackingRoadOwnerAccount=_attackingRoadOwnerAccount;
@synthesize diceButton=_diceButton;
@synthesize tapToStopBackgroundView=_tapToStopBackgroundView;
@synthesize roadId=_roadId;
@synthesize randomizedDiceAttack=_randomizedDiceAttack;
@synthesize lastLevel=_lastLevel;
@synthesize bCanAttackAgain=_bCanAttackAgain;
@synthesize outcome=_outcome;
@synthesize currentGame=_currentGame;
@synthesize totalAttacks=_totalAttacks;
@synthesize attackTimer=_attackTimer;
@synthesize coinImageView=_coinImageView;

int costOfAttack;
NSString *gameName;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addObserver:kOnMotionStart withSelector:@selector(onMotionStart:)];
    self.tapToStartLabel.textColor=kColorGold;
     
    if ( self.user != nil )
        [self acquireAttackLock];
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
   
}
-(void) viewDidDisappear:(BOOL)animated{
    [self resignFirstResponder];
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)dealloc { // best place to unload notifications, release any retained objects, etc
    [self removeObserver:kOnMotionStart];
     
}

- (void) doDiceAnimation{
    self.bIsAnimating=YES;
    self.diceButton.hidden=YES;
    self.tapToStartView.hidden=YES;
      
    self.navigationController.navigationBar.hidden=YES;
    self.navigationController.tabBarController.tabBar.hidden=YES;
    
    self.diceTimer= [NSTimer scheduledTimerWithTimeInterval:0
                                                     target:self
                                                   selector:@selector(doDiceAnimationWithDuration:)
                                                   userInfo:[NSNumber numberWithFloat:.5]
                                                    repeats:NO];
    
    
    float interval = 3.0+(arc4random_uniform(10) % 100)/100.0f;
    //NSLog (@"start second dice interval <%f>",interval);

    self.diceTimer= [NSTimer scheduledTimerWithTimeInterval:interval
                                                     target:self
                                                   selector:@selector(doSlowAnimation1)
                                                   userInfo:nil
                                                    repeats:NO];

}
-(void) doSlowAnimation1{
    [self.dice2 stopAnimating];
    [self.diceTimer2 invalidate];
    self.diceTimer2=nil;
    
    if (self.currentGame==kAnimalUpGame || self.currentGame==kAnimalDownGame)
        self.dice2Value=[NSNumber numberWithInteger:1+arc4random_uniform(5)];
    else
        self.dice2Value=[NSNumber numberWithInteger:1+arc4random_uniform(6)];
    
    if (self.currentGame==kUpCardGame || self.currentGame==kDownCardGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"card_0%d",self.dice2Value.intValue]];
    else  if (self.currentGame==kRockPaperScissorGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"rps_0%d",self.dice2Value.intValue]];
    else  if (self.currentGame==kAnimalUpGame || self.currentGame==kAnimalDownGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"animal_0%d",self.dice2Value.intValue]];
    else if (self.currentGame==kChessGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"chess_0%d",self.dice2Value.intValue]];
    else if (self.currentGame==kRomanNumberGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"roman_0%d",self.dice2Value.intValue]];
    else if (self.currentGame==k1to6NumberGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_0%d",self.dice2Value.intValue]];
    else if (self.currentGame==k6to1NumberGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_green_0%d",self.dice2Value.intValue]];
    else if (self.currentGame==kWheelUpGame)
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"wheel_0%d",self.dice2Value.intValue]];
    else
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"w_0%d",self.dice2Value.intValue]];
    
    [self.dice1 stopAnimating];
    
    self.diceButton.hidden=NO;
    self.tapToStartView.hidden=NO;
    self.tapToStopBackgroundView.hidden=NO;
    
    self.tapToStartLabel.textColor=kColorGold;
    self.tapToStartLabel.text=@"Tap To Stop";
    if (self.bIsAnimating){
        NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
        NSNumber *animationSpeedStart = [serverParameters objectForKey:@"animationSpeedStart"];
        ///NSLog (@"animationSpeedStart <%f>", animationSpeedStart.floatValue);
        float durationLevelStart=animationSpeedStart.floatValue; 
        int level=[RWContext me].attackLevel.intValue/numGames;
           
        float duration;
        //NSLog (@"level <%d><%d>",level, [RWContext me].attackLevel.intValue/numGames);
        durationLevelStart -= (level/10.0f/10.0f);
        duration = (level > 0) ? durationLevelStart-(arc4random_uniform(10) % 100)/100.0f : durationLevelStart;
        //NSLog (@"durationStart <%f> duration <%f>",durationLevelStart,duration);
        [self doMyDiceAnimationWithDuration:duration];
        self.diceTimer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                         target:self
                                                       selector:@selector(doSlowAnimation2)
                                                       userInfo:nil
                                                        repeats:NO];
   
    }
    
}
-(void) doSlowAnimation2{
    if (self.bIsAnimating){
        self.tapToStartView.hidden=YES;
        [self.diceTimer2 invalidate];
        self.diceTimer2=nil;
        
        self.tapToStopBackgroundView.hidden=YES;
        [self doMyDiceAnimationWithDuration:.5];
        
        self.diceTimer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                         target:self
                                                       selector:@selector(doStopAnimation)
                                                       userInfo:nil
                                                        repeats:NO];
    }
    
}

- (void) doDiceAnimationWithDuration:(NSTimer*)sender{
  //  float duration=[sender.userInfo floatValue];
    [self doDefenderDiceAnimationWithDuration:.3];
    
}

- (void) doDefenderDiceAnimationWithDuration:(float)duration{
    if (self.bIsAnimating==NO) {
        [self.dice1 stopAnimating];
        return;
    }
    
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSNumber *animationSlowDownAmount = [serverParameters objectForKey:@"animationSlowDownAmount"];
    //NSLog (@"animationSlowDownAmount <%f>", animationSlowDownAmount.floatValue);
    
    if (self.currentGame==kOddUpDiceGame || self.currentGame==kEvenDownDiceGame || self.currentGame==kAnimalUpGame || self.currentGame==kAnimalDownGame || self.currentGame==kRockPaperScissorGame || self.currentGame==kUpCardGame || self.currentGame==kDownCardGame || self.currentGame==kWheelUpGame || self.currentGame==kRomanNumberGame)
        duration += animationSlowDownAmount.floatValue; // slow them down a bit
    
    self.diceTimer2=[NSTimer scheduledTimerWithTimeInterval:duration
                                                     target:self
                                                   selector:@selector(doAnimateCoin:)
                                                   userInfo:self.dice2
                                                    repeats:YES];
    
    self.bIsAnimating=YES;
    
}

- (void) doMyDiceAnimationWithDuration:(float)duration{
    if (self.bIsAnimating==NO) {
        [self.dice1 stopAnimating];
        return;
    }
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSNumber *animationSlowDownAmount = [serverParameters objectForKey:@"gameSlowDownAmount"];
    NSNumber *initialGameSlowdown = [serverParameters objectForKey:@"initialGameSlowdown"];
    NSLog (@"animationSlowDownAmount <%f>", animationSlowDownAmount.floatValue);
    NSLog (@"initialGameSlowdown <%f>", initialGameSlowdown.floatValue);
    
    if ([RWContext me].attackLevel.intValue/numGames==0)
       // duration += (animationSlowDownAmount.floatValue*2);
        duration += (initialGameSlowdown.floatValue*2);
    else if (self.currentGame == kOddUpDiceGame || self.currentGame == kEvenDownDiceGame || self.currentGame == kChessGame || self.currentGame == kAnimalUpGame || self.currentGame == kAnimalDownGame)
        duration += (animationSlowDownAmount.floatValue);
    else if (self.currentGame == kRockPaperScissorGame  || self.currentGame == kUpCardGame || self.currentGame == kDownCardGame)
        duration += animationSlowDownAmount.floatValue;
    else if (self.currentGame == kWheelDownGame  || self.currentGame == kWheelUpGame || self.currentGame == kRomanNumberGame)
        duration += animationSlowDownAmount.floatValue*2;

    
    self.diceTimer2=[NSTimer scheduledTimerWithTimeInterval:duration
                                     target:self
                                    selector:@selector(doAnimateCoin:)
                                   userInfo:self.dice1
                                    repeats:YES];

    self.bIsAnimating=YES;
       
}

-(void) doAnimateCoin:(NSTimer*)sender{
    UIImageView * diceToAnimate = sender.userInfo;
 
    // NSLog(@"level in dice animation <%d>",level);
    if (self.currentGame == kAnimalUpGame ){
        if (++self.diceNumber>5)
            self.diceNumber=1;
    }else if (self.currentGame == kAnimalDownGame){
        if (--self.diceNumber<1)
            self.diceNumber=5;
    } else if (self.currentGame == k1to6DiceGame || self.currentGame == kUpCardGame || self.currentGame == k1to6NumberGame || self.currentGame == kChessGame || self.currentGame == kRomanNumberGame || self.currentGame == kWheelUpGame){
        if (++self.diceNumber>6)
            self.diceNumber=1;
    } else if (self.currentGame == k6to1DiceGame || self.currentGame == kDownCardGame || self.currentGame == kRockPaperScissorGame || self.currentGame == k6to1NumberGame){
        if (--self.diceNumber<1)
            self.diceNumber=6;
    } else if (self.currentGame == kOddUpDiceGame){
        self.diceNumber+=2;
        if (self.diceNumber==7)
            self.diceNumber=2;
        else if (self.diceNumber==8)
            self.diceNumber=1;
    }else if (self.currentGame == kEvenDownDiceGame){
        self.diceNumber-=2;
        if (self.diceNumber<0)
            self.diceNumber=6;
        else if (self.diceNumber==0)
            self.diceNumber=5;
    }
   
    
    if (self.currentGame == kUpCardGame || self.currentGame == kDownCardGame )
        diceToAnimate.image=[UIImage imageNamed:[NSString stringWithFormat:@"card_0%d",self.diceNumber]];
    else if (self.currentGame == kRockPaperScissorGame)
        diceToAnimate.image = [UIImage imageNamed:[NSString stringWithFormat:@"rps_0%d",self.diceNumber]];
    else if (self.currentGame == kAnimalUpGame || self.currentGame == kAnimalDownGame)
        diceToAnimate.image = [UIImage imageNamed:[NSString stringWithFormat:@"animal_0%d",self.diceNumber]];
    else if (self.currentGame == kChessGame)
        diceToAnimate.image = [UIImage imageNamed:[NSString stringWithFormat:@"chess_0%d",self.diceNumber]];
    else if (self.currentGame == kRomanNumberGame)
        diceToAnimate.image = [UIImage imageNamed:[NSString stringWithFormat:@"roman_0%d",self.diceNumber]];
    else if (self.currentGame == k1to6NumberGame)
        diceToAnimate.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_0%d",self.diceNumber]];
    else if (self.currentGame == k6to1NumberGame)
        diceToAnimate.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_green_0%d",self.diceNumber]];
    else if (self.currentGame == kWheelUpGame)
        diceToAnimate.image = [UIImage imageNamed:[NSString stringWithFormat:@"wheel_0%d",self.diceNumber]];

    else
        diceToAnimate.image=[UIImage imageNamed:[NSString stringWithFormat:@"bl_0%d",self.diceNumber]];
    
    
}

- (IBAction)doSpinDice:(id)sender{
    
    if (self.tapToStartView.isHidden)
        return;
    
    self.parentViewController.navigationItem.hidesBackButton = YES;
    
    if (!self.bIsAnimating){
        self.versusLabel.text=@"VS";
        self.tapToStartLabel.text=@"WAIT...";
        self.versusLabel.textColor=[UIColor whiteColor];
        [self doDiceAnimation];
    }
    else
        [self doStopAnimation];
}

-(void) doStopDefenderDiceAnimation{
}
-(void) doStopAnimation{
    
    //NSLog (@"doStopAnimation");
    NSString *gameName; // chess, dice, animals, etc
    [self.dice1 stopAnimating];
    [self.diceTimer invalidate];
    self.diceTimer=nil;
    [self.diceTimer2 invalidate];
    self.diceTimer2=nil;
    
    self.tapToStartView.hidden=YES;
    self.diceButton.hidden=YES;
    self.tapToStopBackgroundView.hidden=YES;
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.tabBarController.tabBar.hidden=YES;
   
    //self.versusLabel.font=[UIFont fontWithName:kFontNameGrunge size:50];
    
    if (self.diceNumber<1) self.diceNumber=1;
    else if (self.currentGame == kAnimalUpGame || self.currentGame == kAnimalDownGame){
        if (self.diceNumber>5) self.diceNumber=5;
    }
    else
        if (self.diceNumber>6) self.diceNumber=6;
    
    self.dice1Value=[NSNumber numberWithInteger:self.diceNumber];
    //NSLog (@" dice1 %d, dice2 %d", self.dice1Value.intValue, self.dice2Value.intValue);
    
    self.outcome=0; // tie=0, win=1, lose=2
    if (self.currentGame==kRockPaperScissorGame){
        gameName=@"rock";
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"rps_0%d",self.dice1Value.intValue]];
        if (self.dice1Value.intValue>3)
            self.dice1Value=[NSNumber numberWithInt:self.dice1Value.intValue-3];
        if (self.dice2Value.intValue>3)
            self.dice2Value=[NSNumber numberWithInt:self.dice2Value.intValue-3];
        if (self.dice2Value.intValue==3){//rock
            if (self.dice1Value.intValue==2)//paper - win
                self.outcome=1;
            else if (self.dice1Value.intValue==1) //scissors - lose
                self.outcome=2;
            
        } else if (self.dice2Value.intValue==2){//paper
            if (self.dice1Value.intValue==1)//scissors - win
                self.outcome=1;
            else if (self.dice1Value.intValue==3) //rock - lose
                self.outcome=2;
            
        } else if (self.dice2Value.intValue==1){//scissors
            if (self.dice1Value.intValue==3)//rock - win
                self.outcome=1;
            else if (self.dice1Value.intValue==2) //paper - lose
                self.outcome=2;
        }
    }
    else {
        if (self.currentGame==kUpCardGame || self.currentGame==kDownCardGame){
            self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"card_0%d",self.dice1Value.intValue]];
            gameName=@"cards";
        }
        else if (self.currentGame==kAnimalUpGame || self.currentGame==kAnimalDownGame){
            self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"animal_0%d",self.dice1Value.intValue]];
            gameName = @"animals";
        }
        else if (self.currentGame==kChessGame){
             self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"chess_0%d",self.dice1Value.intValue]];
            gameName=@"chess";
        }
        else if (self.currentGame==kRomanNumberGame){
                self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"roman_0%d",self.dice1Value.intValue]];
            gameName=@"roman";
        }
        else if (self.currentGame==k1to6NumberGame){
            self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_0%d",self.dice1Value.intValue]];
            gameName=@"numbers";
        }
        else if (self.currentGame==k6to1NumberGame){
            self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_green_0%d",self.dice1Value.intValue]];
            gameName=@"dice";
        }
        else if (self.currentGame==kWheelUpGame){
            self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"wheel_0%d",self.dice1Value.intValue]];
            gameName=@"wheels";
        }
        else{
            self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"bl_0%d",self.dice1Value.intValue]];
            gameName=@"dice";
        }
        
        
        if (self.dice1Value.intValue > self.dice2Value.intValue)
            self.outcome=1;
        else if (self.dice1Value.intValue < self.dice2Value.intValue)
            self.outcome=2;
        
        if (self.currentGame==kChessGame){//bishop==knight
            if ((self.dice1Value.intValue==2|| self.dice1Value.intValue==3) && (self.dice2Value.intValue==2 || self.dice2Value.intValue==3)) //tie
                self.outcome=0;
        }
    }
    
    if (self.outcome==1){
        self.versusLabel.text=@"YOU WIN";
        self.versusLabel.textColor=kColorGreen;
        [[RWServices audioService] playSound:kSoundCaptured2];
        [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Won %@ from %@ %@ (%@ %d coin)", self.attackeeRoadName.text,self.user.first,[self.user.last substringToIndex:1], gameName, costOfAttack] appendTime:TRUE];
        
    }
    else if (self.outcome==2){
        self.versusLabel.text=@"YOU LOSE";
        self.versusLabel.textColor=kColorRed;
        [[RWServices audioService] playSound:kSoundBusted1];
        [RWContext addSkinnyMessage:[NSString stringWithFormat:@"Lost %@ vs. %@ %@ (%@ %d coin)", self.attackeeRoadName.text,self.user.first,[self.user.last substringToIndex:1],gameName,costOfAttack] appendTime:TRUE];
        
    }
    else { // TIE, now just redo withoput asking
        self.versusLabel.textColor=kColorGold;
        //self.versusLabel.font=[UIFont fontWithName:kFontNameGrunge size:30];
        self.versusLabel.text=@"YOU TIE";
        [self doDiceAnimation];
        return;
    }
    
    self.bCanAttackAgain=YES;
    
    if (self.outcome>0)
        [self doDisplayOutcomeView];
    
    self.bIsAnimating=NO;
   // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
   }

- (void)doDisplayOutcomeView{
    
    self.meRoadName.hidden=NO;
    
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.tabBarController.tabBar.hidden=YES;
    self.attackeeRoadName.hidden=self.attackeeTownStateName.hidden=NO;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        if (self.outcome==1){// winner
            self.meRoadName.hidden=self.meTownStateName.hidden=TRUE;
            [self postNotification:kOnWinAttack withObject:nil];
        }
        else { // loser
            //self.attackeeRoadName.hidden=self.attackeeTownStateName.hidden=TRUE;
            
           // self.meRoadName.text=[NSString stringWithFormat:@"%@ coin",self.attackCost.text] ;
           // self.meRoadName.frame=CGRectMake( self.meRoadName.frame.origin.x, self.meRoadName.frame.origin.y+self.view.frame.size.height/2+5, self.meRoadName.frame.size.width, self.meRoadName.frame.size.height);
            
            [self postNotification:kOnLoseAttackMe withObject:nil];
        }
        
    } completion:^(BOOL finished){
        
        //  cost of an attack
        NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
        NSNumber *minimumCostOfAttack = [serverParameters objectForKey:@"minimumCostOfAttack"];
        //NSLog (@"minimumCostOfAttack <%d>", minimumCostOfAttack.intValue);
        
        NSNumber *attackCostMultiplier = [serverParameters objectForKey:@"attackCostMultiplier"];
       // NSLog (@"attackCostMultiplier <%d>", attackCostMultiplier.intValue);
        
        //NSLog(@"(self.defendingRoadOwnerAccount.intValue <%d> self.attackingRoadOwnerAccount.intValue <%d>",self.defendingRoadOwnerAccount.intValue,self.attackingRoadOwnerAccount.intValue);
        costOfAttack = ((self.defendingRoadOwnerAccount.intValue-self.attackingRoadOwnerAccount.intValue)*attackCostMultiplier.intValue);
        if (costOfAttack < minimumCostOfAttack.intValue) costOfAttack=minimumCostOfAttack.intValue+self.totalAttacks;
        
        //NSLog(@"Release- self.meRoadExtId <%@> self.attackeeRoadExtId<%@> ",self.meRoadExtId, self.attackeeRoadExtId);
        //NSLog(@"meFbId <%@> attackeeFbId <%@>", [RWContext me].fbId,self.user.fbId);
        [RWAPI releaseAttackLock:self.meRoadExtId
              defendingRoadExtId:self.attackeeRoadExtId
                      winnerFbId:(self.outcome==1)?[RWContext me].fbId:self.user.fbId
                             won:(self.outcome==1)?YES:NO
                 attackerPayment:[NSNumber numberWithInt:costOfAttack]
                        gameType:[NSNumber numberWithInt:self.currentGame]
                        gameName:gameName
                      completion:^(NSDictionary *result) {
                          //NSLog (@"result from releaseAttackLock - %@", result);
                          [RWContext me].coin=[NSNumber numberWithInt:[RWContext me].coin.intValue-costOfAttack];
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [RWContext commit];
                          });
                          self.tapToStartView.hidden=NO;
                          self.diceButton.hidden=NO;
                          self.attackCost.hidden=YES;
                          self.coinImageView.hidden=YES;
                          self.tapToStartLabel.text=@"ATTACK AGAIN?";
                          [self setTimer];
                          
                      } failure:^(NSError *error) {
                          // do anything on failure?
                          //NSLog (@"error from releaseAttackLock - %@", error);
                      }];

       
    }];
    

}
-(void) setTimer{
    if (self.attackTimer!=nil){
        [self.attackTimer invalidate];
        self.attackTimer=nil;
    }

    self.attackTimer  = [NSTimer scheduledTimerWithTimeInterval:2.5
                                                         target:self
                                                       selector:@selector(doDisolveAndPop)
                                                       userInfo:nil
                                                        repeats:YES];
    
}
-(void)attackAgain{
    self.attackeeRoadName.hidden=self.attackeeTownStateName.hidden=YES;
    self.attackCost.hidden=NO;
    self.coinImageView.hidden=NO;
    [self acquireAttackLock];
}

-(void)doDisolveAndPop{
   
    self.tapToStartView.hidden=YES;
    [UIView animateWithDuration:1.5 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.view.alpha=0;
         
    }completion:^(BOOL finished) {
        [self.attackTimer invalidate];
        self.attackTimer=nil;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		//NSLog(@"user pressed OK");
        [self.navigationController popToRootViewControllerAnimated:NO];
    }else {
		//NSLog(@"user pressed Cancel");
	}
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event{
   // NSLog(@"shake");
    //if (!self.bIsAnimating)
    //    [self doDiceAnimation];
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if(((UITouch *)[touches anyObject]).tapCount == 1){
       // NSLog(@"self.tapToStartLabel.text %@",self.tapToStartLabel.text);
        if ([self.tapToStartLabel.text isEqualToString:@"ATTACK AGAIN?"]){
            [self.attackTimer invalidate];
            self.attackTimer=nil;
            [self attackAgain];
        }
        else
            [ self doSpinDice:nil];
    }
    [super touchesEnded:touches withEvent:event];
}

- (void)onMotionStart:(NSNotification *)notif {
    //NSLog(@"%@:onMotionStart in attack", self.class);
    
      if (self.bIsAnimating)
        [self doStopAnimation];
    
     [self.navigationController popToRootViewControllerAnimated:NO];
   
}
- (void) acquireAttackLock{
   self.attackeeRoadName.hidden=self.attackeeTownStateName.hidden=YES;
    self.dice1.hidden=self.dice2.hidden=self.tapToStopBackgroundView.hidden= YES;
    
    self.versusLabel.text = @"Loading...";
    self.tapToStartView.hidden=YES;
    NSNumber *numTotalAttacksToday = [[NSUserDefaults standardUserDefaults] objectForKey:@"todayTotalAttack"];
    self.totalAttacks= numTotalAttacksToday.intValue;
    //NSLog (@"self.totalAttacks=%d",self.totalAttacks);
    
    if (self.user==nil || self.user.fbId==nil){
        NSLog (@"Something not right <%@>", self.user);
        self.versusLabel.text = @"Problem with attack, try again";
       // [self performSelector:@selector(popToRootViewController) withObject:nil afterDelay:.25];
        //[self.navigationController popToRootViewControllerAnimated:NO];
        return;
        
    }
    //NSLog (@"acquireLock <%@>",self.user);
    self.view.alpha=1;
    // NSLog(@" self.meRoadExtId <%@> self.attackeeRoadExtId<%@> ",self.meRoadExtId, self.attackeeRoadExtId);
    self.dice1.hidden=self.dice2.hidden=NO;
    self.diceButton.hidden=NO;
    
    self.profilePictureAttackee.profileID = self.user.fbId.stringValue;
    self.profilePictureMe.profileID = [RWContext me].fbId.stringValue;
    [self setTitle:[NSString stringWithFormat:@"Attack %@ %@.",self.user.first,[self.user.last substringToIndex:1]]];
    self.diceNumber=0;
    self.tapToStopBackgroundView.hidden=YES;
    self.meRoadName.hidden=YES;
    self.diceButton.hidden=YES;
    self.bCanAttackAgain=NO;
    
    int initialDefenderDice=1;
    
    int level=[RWContext me].attackLevel.intValue/numGames; // how many
    if (level ==0){
        if ([RWContext me].attackLevel.intValue%numGames==0) self.currentGame=k1to6DiceGame;
        else if ([RWContext me].attackLevel.intValue%numGames==1) self.currentGame=kUpCardGame;
        else if ([RWContext me].attackLevel.intValue%numGames==2){ self.currentGame=k6to1DiceGame;initialDefenderDice=6;}
        else if ([RWContext me].attackLevel.intValue%numGames==3) self.currentGame=kRockPaperScissorGame;
        else if ([RWContext me].attackLevel.intValue%numGames==4){ self.currentGame=kEvenDownDiceGame;initialDefenderDice=6;}
        else if ([RWContext me].attackLevel.intValue%numGames==5) self.currentGame=kAnimalUpGame;
        else if ([RWContext me].attackLevel.intValue%numGames==6) self.currentGame=kOddUpDiceGame;
        else if ([RWContext me].attackLevel.intValue%numGames==7){ self.currentGame=kDownCardGame;initialDefenderDice=6;}
        else if ([RWContext me].attackLevel.intValue%numGames==8){ self.currentGame=k1to6NumberGame;}
        else if ([RWContext me].attackLevel.intValue%numGames==9){ self.currentGame=kRomanNumberGame;}
        else if ([RWContext me].attackLevel.intValue%numGames==10){ self.currentGame=k6to1NumberGame;initialDefenderDice=6;}
        else if ([RWContext me].attackLevel.intValue%numGames==11){ self.currentGame=kChessGame;}
        else if ([RWContext me].attackLevel.intValue%numGames==12){ self.currentGame=kAnimalDownGame;initialDefenderDice=5;}
        else if ([RWContext me].attackLevel.intValue%numGames==13){ self.currentGame=kWheelUpGame;}
        
        [RWContext me].attackRandomizedDiceOrder=[NSNumber numberWithInt:self.currentGame];
        [RWContext commit];
        
    }
    else {
        int random = [RWContext me].attackRandomizedDiceOrder.intValue;
        while (random == [RWContext me].attackRandomizedDiceOrder.intValue) // try to get a new random game diff from one before
            random = arc4random_uniform(numGames+1);
        
        if (random < 1) random=1;
        else if (random > numGames) random = numGames;
        
        self.randomizedDiceAttack=random;
        [RWContext me].attackRandomizedDiceOrder=[NSNumber numberWithInt:random];
        [RWContext commit];
        self.currentGame = self.randomizedDiceAttack;
        initialDefenderDice=1;
        if (self.currentGame==k6to1DiceGame || self.currentGame==kEvenDownDiceGame || self.currentGame==kDownCardGame || self.currentGame==k6to1NumberGame )
            initialDefenderDice=6;
        else if (self.currentGame==kAnimalDownGame)
            initialDefenderDice=5;
        
        
    }
    //NSLog (@"current game <%d>", self.currentGame);
    
    //NSLog (@"2.Level <%d> lastlevel <%d> randomdice <%d>", level, self.lastLevel, self.randomizedDiceAttack);
    
    if (self.currentGame==kUpCardGame || self.currentGame==kDownCardGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"card_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"card_0%d",initialDefenderDice]];
        gameName=@"cards";
    } else if (self.currentGame==kRockPaperScissorGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"rps_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"rps_0%d",initialDefenderDice]];
         gameName=@"rock";
    }else if (self.currentGame==kAnimalUpGame || self.currentGame==kAnimalDownGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"animal_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"animal_0%d",initialDefenderDice]];
         gameName=@"animals";
    }else if (self.currentGame==kChessGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"chess_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"chess_0%d",initialDefenderDice]];
         gameName=@"chess";
    }else if (self.currentGame==kRomanNumberGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"roman_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"roman_0%d",initialDefenderDice]];
         gameName=@"roman";
    }else if (self.currentGame==k1to6NumberGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_0%d",initialDefenderDice]];
         gameName=@"numbers";
    }else if (self.currentGame==k6to1NumberGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_green_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"clay_green_0%d",initialDefenderDice]];
        gameName=@"numbers";
    }else if (self.currentGame==kWheelUpGame){
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"wheel_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"wheel_0%d",1]];
        gameName=@"wheels";
    }else{
        self.dice1.image = [UIImage imageNamed:[NSString stringWithFormat:@"bl_0%d",1]];
        self.dice2.image = [UIImage imageNamed:[NSString stringWithFormat:@"w_0%d",initialDefenderDice]];
        gameName=@"dice";
    }
    
    
    //hide the view when the download starts
    self.profilePictureAttackee.startHandler = ^(DBFBProfilePictureView* view){
        view.layer.opacity = 0.0f;
    };
    //show the view when the download completes, or show the empty image
    self.profilePictureAttackee.completionHandler = ^(DBFBProfilePictureView* view, NSError* error){
        if(error) {
            view.showEmptyImage = YES;
            //view.profileID = nil;
            NSLog(@"Loading Defender profile picture failed with error: %@", error);
        }
        [UIView animateWithDuration:0.1f animations:^{
            view.layer.opacity = 1.0f;
        }];
    };
    
    //hide the view when the download starts
    self.profilePictureMe.startHandler = ^(DBFBProfilePictureView* view){
        view.layer.opacity = 0.0f;
    };
    //show the view when the download completes, or show the empty image
    self.profilePictureMe.completionHandler = ^(DBFBProfilePictureView* view, NSError* error){
        if(error) {
            view.showEmptyImage = YES;
            //view.profileID = nil;
            NSLog(@"Loading ME profile picture failed with error: %@", error);
        }
        [UIView animateWithDuration:0.1f animations:^{
            view.layer.opacity = 1.0f;
        }];
    };
    
    
    

    [RWAPI acquireAttackLock:self.user.fbId defendingRoadExtId:[NSNumber numberWithInt: self.roadId] gameType:[NSNumber numberWithInt:self.currentGame] gameName:gameName
                  completion:^(NSDictionary *result) {
                      NSLog (@"result from acquireAttackLock - %@", result);
                      NSNumber *isLockAcquired=[[result objectForKey:@"data"]objectForKey:@"isLockAcquired"];
                      
                      if (isLockAcquired.intValue==YES){
                          
                          if (self.user == nil || self.user.first == nil){
                              NSLog(@"Something wrong with user...contact support, going to try another attack. %@", self.user);
                              self.versusLabel.text=@"Unable to attack...";
                              [self.navigationController popToRootViewControllerAnimated:NO];
                          }
                          self.versusLabel.text = @"VS";
                          self.tapToStartView.hidden=NO;
                          
                          self.meTownStateName.text=[[result objectForKey:@"data"]objectForKey:@"attackingRoadCityName"];
                          self.meRoadName.text=[[result objectForKey:@"data"]objectForKey:@"attackingRoadName"];
                          self.meRoadExtId=[[result objectForKey:@"data"]objectForKey:@"attackingRoadExtId"];
                          
                          self.attackeeTownStateName.text=[[result objectForKey:@"data"]objectForKey:@"defendingRoadCityName"];
                          self.attackeeRoadName.text=[NSString stringWithFormat:@"%@, %@",[[result objectForKey:@"data"]objectForKey:@"defendingRoadName"],[[result objectForKey:@"data"]objectForKey:@"defendingRoadCityName"]]  ;
                          self.attackeeRoadExtId=[[result objectForKey:@"data"]objectForKey:@"defendingRoadExtId"];
                          self.defendingRoadOwnerAccount=[[result objectForKey:@"data"]objectForKey:@"defendingRoadOwnerAccount"];
                          self.attackingRoadOwnerAccount=[[result objectForKey:@"data"]objectForKey:@"attackerAccount"];
                          //NSLog(@"defendingRoadOwnerAccount %d",self.defendingRoadOwnerAccount.intValue);
                          //NSLog(@"attackingRoadOwnerAccount %d",self.attackingRoadOwnerAccount.intValue);
                          
                          NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
                          NSNumber *attackCostMultiplier = [serverParameters objectForKey:@"attackCostMultiplier"];
                          // NSLog (@"attackCostMultiplier <%d>", attackCostMultiplier.intValue);
                          NSNumber *minimumCostOfAttack = [serverParameters objectForKey:@"minimumCostOfAttack"];
                          //NSLog (@"minimumCostOfAttack <%d>", minimumCostOfAttack.intValue);
                          
                          costOfAttack = ((self.defendingRoadOwnerAccount.intValue-self.attackingRoadOwnerAccount.intValue)*attackCostMultiplier.intValue);
                          
                          //NSLog(@"(self.defendingRoadOwnerAccount.intValue <%d> self.attackingRoadOwnerAccount.intValue <%d>",self.defendingRoadOwnerAccount.intValue,self.attackingRoadOwnerAccount.intValue);
                          
                          if (costOfAttack <= 0) { // something wrong, should not have this
                              NSLog(@"Something wrong with this road...contact support, going to try another attack.");
                              [self acquireAttackLock];
                              return ;
                              
                          } else {
                              
                              if (costOfAttack < minimumCostOfAttack.intValue) costOfAttack=minimumCostOfAttack.intValue+self.totalAttacks;
                              self.attackCost.textColor=kColorGold;
                              self.tapToStartLabel.text=[NSString stringWithFormat:@" Start %d Coin",costOfAttack];
                              if (costOfAttack >  [RWContext me].coin.intValue) { // cannot go negative coin, ask to buy
                                  UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Cannot Attack"
                                                                                  message:@"Not enough coin to attack, go out driving."
                                                                                 delegate:self
                                                                        cancelButtonTitle:@"OK"
                                                                        otherButtonTitles:nil];
                                  [alert show];
                                  
                              }
                          }
                      }
                      else{
                          /*
                           UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Cannot Attack"
                           message:[result objectForKey:@"message"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                          [alert show];
                           */
                          NSLog(@"Something wrong with this attack... <%@>", [result objectForKey:@"message"]);
                          self.versusLabel.text=@"Unable to attack...";
                          [NSTimer scheduledTimerWithTimeInterval:5
                                                           target:self
                                                         selector:@selector(popToRootViewController)
                                                         userInfo:nil
                                                          repeats:NO];
                                                    
                      }
                      //NSLog(@"numAttacksWon <%@>",[RWContext me].attackLevel);
                  } failure:^(NSError *error) {
                      // do anything on failure?
                      NSLog (@"error from acquireAttackLock - %@", error);
                  }];
    

    
}
-(void) popToRootViewController{
    self.navigationController.tabBarController.selectedIndex=1;
}
@end

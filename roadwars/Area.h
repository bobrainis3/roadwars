//
//  Area.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Rider;

@interface Area : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * abbreviation;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * lng;
@property (nonatomic, retain) NSNumber * ownerAccount;
@property (nonatomic, retain) NSNumber * runnerUpAccount;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSDate * lastUpdated;
@property (nonatomic, retain) NSNumber * dailyTake;
@property (nonatomic, retain) NSNumber * extId;
@property (nonatomic, retain) NSNumber * lifetimeTake;
@property (nonatomic, retain) Rider *owner;
@property (nonatomic, retain) Rider *runnerUp;
@property (nonatomic, retain) NSSet *coverages;
@end

@interface Area (CoreDataGeneratedAccessors)

- (void)addCoveragesObject:(NSManagedObject *)value;
- (void)removeCoveragesObject:(NSManagedObject *)value;
- (void)addCoverages:(NSSet *)values;
- (void)removeCoverages:(NSSet *)values;

@end

//
//  RWAppDelegate.h
//  roadwars
//
//  Created by Christian Allen on 11/5/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

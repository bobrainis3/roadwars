//
//  RWAboutViewController
//  roadwars
//
//  Created by Bob Rainis 1/07/13.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWViewController.h"

@interface RWAboutViewController : RWViewController

@property (nonatomic, weak) IBOutlet UILabel *versionLabel;
- (IBAction)doFacebook:(id)sender;
- (IBAction)doTwitter:(id)sender;


@end

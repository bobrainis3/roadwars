//
//  RWLocationService.m
//  roadwars
//
//  Created by Bob Rainis on 11/7/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
#import "RWServices.h"
#import "RWLocationService.h"
#import "RWGeocodingService.h"
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"

@implementation RWLocationService

@synthesize locationManager = _locationManager;
@synthesize lastLocation = _lastLocation;
@synthesize previousLocation = _previousLocation;
@synthesize bDriving = _bDriving;
@synthesize heading = _heading;
@synthesize headingArray = _headingArray;
@synthesize bDoneStartOrStop=_bDoneStartOrStop;

- (id) init {
	self = [super init];
	if (self != nil) {
      	self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self; // send loc updates to myself
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
        NSNumber *locationUpdateInMeters = [serverParameters objectForKey:@"locationUpdateInMeters"];
        
        // just to be safe...
        if (locationUpdateInMeters.intValue < 1)
            locationUpdateInMeters = [NSNumber numberWithInt:10];
        
        NSLog(@"locationUpdateInMeters <%@><%f>", locationUpdateInMeters,locationUpdateInMeters.doubleValue);

        self.locationManager.distanceFilter = locationUpdateInMeters.doubleValue;//kCLDistanceFilterNone;                                                                               
        self.locationManager.pausesLocationUpdatesAutomatically=YES;
        [self.locationManager setActivityType:CLActivityTypeAutomotiveNavigation];
        self.bDriving=NO;
        self.heading=[NSNumber numberWithInt:1]; // initialize with 1 for simulator to work when reporting speed limits
        self.headingArray=[[NSMutableArray alloc]init];
    
        [self addObserver:kOnDoneMotionStart withSelector:@selector(onDoneMotionStart)];
        [self addObserver:kOnDoneMotionStop withSelector:@selector(onDoneMotionStop)];
        self.bDoneStartOrStop=YES;
        
	}
	return self;
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    //NSLog(@"Location Update <%@>",locations);
    
    //CLLocation *newLocation=manager.location;
    CLLocation *newLocation = [locations lastObject];

    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
      
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0){NSLog (@"horizonatal accracy failure"); return;}
   
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    //NSLog (@"abs(howRecent) %d", abs(howRecent));
    if (abs(howRecent) < 5) {  // If the event is recent, do something with it.
       
        // Store updatedLocation, determine speed, and if necessary, notify MasterNavigationController
        // is state of driving has change, and notify UI that speed has changed
        // this is a notification that the server should update views
        double speedMph = newLocation.speed*KSpeedConverter;
        NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
        NSNumber *minDrivingSpeed = [serverParameters objectForKey:@"minSpeed"];
        float trueHeading=[self bearingBetweenStartLocation:self.lastLocation andEndLocation:newLocation];
        self.lastLocation = newLocation;
        
        //if (self.bDoneStartOrStop){
            if (speedMph>minDrivingSpeed.intValue){
                if (!self.bDriving){
                    [self postNotification:kOnMotionStart withObject:nil];
                    self.bDriving=YES;
                    self.bDoneStartOrStop=NO;
                }
            }
            else {
                if (self.bDriving){
                    [self postNotification:kOnMotionStop withObject:nil];
                    self.bDriving=NO;
                    self.bDoneStartOrStop=NO;
                }
            //    return;
            }
        //}
        if (isnan(trueHeading)){
            //NSLog(@"isnan %f self.lastLocation<%@> newLocation <%@>", trueHeading, self.lastLocation, newLocation);
            return;
        } else {
            [self.headingArray insertObject:[NSNumber numberWithDouble:trueHeading]  atIndex:0];
            if (self.headingArray.count==headingBufferSize)
                [self.headingArray removeObjectAtIndex:headingBufferSize-1];
        }
        [self postNotification:kOnSpeedUpdate withObject:self.lastLocation];
        
        //NSLog (@"going to reverseGeocode");
        // call geocoding service with this updated location
        
        [[RWServices geocodingService] reverseGeocode:newLocation];
        //NSLog(@"%@::didUpdateToLocation - sanity check - are lastLocation and oldLocation the same?: %@ vs %@", self.class, lastLocation, newLocation);
        
    }
    else
        NSLog (@"Doing nothing in getLocation");

}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateHeading:(CLHeading *)newHeading {
    //NSLog (@"true heading <%f>",newHeading.trueHeading);
    //self.heading=[NSNumber numberWithFloat:newHeading.trueHeading];
       
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager{
        [self postNotification:kOnMotionStop withObject:nil];
}
- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager{
        [self postNotification:kOnMotionStart withObject:nil];
}
// CA: not sure yet what we'll do with these.  what kind of things produce errors here?
- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error {
    NSLog (@"location manager error %@", error);
	//[self.delegate locationError:error];  // let's not implement anything until we know what we'll do for errors
}

-(float) bearingBetweenStartLocation:(CLLocation *)startLocation andEndLocation:(CLLocation *)endLocation{
    CLLocation *northPoint = [[CLLocation alloc] initWithLatitude:(startLocation.coordinate.latitude)+.01 longitude:endLocation.coordinate.longitude];
    float magA = [northPoint distanceFromLocation:startLocation];
    float magB = [endLocation distanceFromLocation:startLocation];
    CLLocation *startLat = [[CLLocation alloc] initWithLatitude:startLocation.coordinate.latitude longitude:0];
    CLLocation *endLat = [[CLLocation alloc] initWithLatitude:endLocation.coordinate.latitude longitude:0];
    float aDotB = magA*[endLat distanceFromLocation:startLat];
    return RADIANS_TO_DEGREES(acosf(aDotB/(magA*magB)));
}


- (void)dealloc {
    [self removeObserver:kOnDoneMotionStart];
    [self removeObserver:kOnDoneMotionStop];
	self.locationManager=nil;
    self.lastLocation=nil;
    self.previousLocation=nil;
}

-(void) onDoneMotionStart{
    self.bDoneStartOrStop=YES;
    
}
-(void) onDoneMotionStop{
    self.bDoneStartOrStop=YES;
}

@end

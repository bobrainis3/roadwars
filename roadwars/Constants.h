//
//  Constants.h
//  RW
//
//  Created by Christian Allen on 9/18/12.
//  Copyright (c) 2013 Pemberton Software All rights reserved.
//

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// colors
#define kColorCobblestoneLabel UIColorFromRGB(0xdddddd)
#define kColorCobblestoneShadow UIColorFromRGB(0x444444)
#define kColorGold UIColorFromRGB(0xfba919)
#define kColorPurple UIColorFromRGB(0xaa22ee)
#define kColorBlue UIColorFromRGB(0x4169E1)
#define kColorDarkBlue UIColorFromRGB(0x00008B)
#define kColorBlueBusted UIColorFromRGB(0x3333FF)	
#define kColorPink UIColorFromRGB(0xee1199)
#define kColorGreen UIColorFromRGB(0x22dd22)
#define kColorYellow UIColorFromRGB(0xffee11)
#define kColorRed UIColorFromRGB(0xee3322)
#define kColorGray UIColorFromRGB(0xF5F5F5)
#define kColorSpeedMeterGreen kColorGreen
#define kColorStreetSign kColorGray
#define kColorStreetSignMeterNotOwn kColorGray
#define kColorStreetSignMeterOwn kColorDarkBlue
#define kColorBonusMeter kColorGray
#define kColorSilverGradientBorder UIColorFromRGB(0xf7f7f7)
#define kColorSilverGradientTop UIColorFromRGB(0xf0f1f2)
#define kColorSilverGradientBottom UIColorFromRGB(0xc0c1c2)
#define kColorHudBackgroundDark UIColorFromRGB(0x171717)
#define kColorTabBar UIColorFromRGB(0x161616)
#define kColorGrungeButtonTopGradient UIColorFromRGB(0x2f2f2f)
#define kColorGrungeButtonBottomGradient UIColorFromRGB(0x1d1d1d)
#define kColorGrungeButtonTopBorder UIColorFromRGB(0x3b3b3b)
#define kColorDaytimeViewTop UIColorFromRGB(0x0052cc)
#define kColorDaytimeViewBottom UIColorFromRGB(0x50aaeb)
#define kColorNighttimeViewTop UIColorFromRGB( 0x0a213a)
#define kColorNighttimeViewMiddle UIColorFromRGB( 0x231f63)
#define kColorNighttimeViewBottom UIColorFromRGB(0x735539)

#define kColorYellowViewTop UIColorFromRGB(0xffa100)
#define kColorYellowViewBottom UIColorFromRGB(0xff7400)
#define kColorRedViewTop UIColorFromRGB(0xfa4218)
#define kColorRedViewBottom UIColorFromRGB(0xda0908)



// fonts
#define kFontNameSans @"PT Sans"
#define kFontNameSansBold @"PTSans-Bold"
#define kFontNameGrunge @"HVDPoster"

// notifications
#define kOnMotionStart @"onMotionStart"
#define kOnMotionStop @"onMotionStop"
#define kOnMotionStartDrive @"onMotionStartDrive"
#define kOnMotionStopDrive @"onMotionStopDrive"

#define kOnSpeedUpdate @"onSpeedUpdate"
#define kOnLocationUpdate @"onLocationUpdate"
#define kOnSpeedlimitUpdate @"onSpeedlimitUpdate"
#define kOnStreetNameUpdate @"onStreetNameUpdate"
#define kOnAccelerometerIncident @"onAccelerometerIncident"
#define kOnAccelerometerDetectMovingPhone @"onAccelerometerDetectMovingPhone"
#define kOnBonusTimerUpdate @"onBonusTimerUpdate"
#define kOnFacebookLogin @"onFacebookLogin" // actual login through fb sdk complete
#define kOnFacebookLogout @"onFacebookLogout" // actual login through fb sdk complete
#define kOnFacebookLoginFailure @"onFacebookLoginFailure" // actual login through fb fails (user fails login or auth)
#define kOnFacebookMeRequest @"onFacebookMeRequest" // starting a /graph/me request via fb sdk
#define kOnFacebookMeRequestComplete @"onFacebookMeRequestComplete" // finishing a /graph/me request
#define kOnFacebookFriendsRequest @"onFacebookFriendsRequest" // starting a /graph/friends request via fb sdk
#define kOnFacebookFriendsRequestComplete @"onFacebookFriendsRequestComplete" // finishing a /graph/friends request
#define kOnFacebookFriendsRequestInviteComplete @"onFacebookFriendsRequestInviteComplete" // finishing a /graph/friends request for invite

#define kOnCurrentUserFbConnect @"onCurrentUserFbConnect" // user info updated due to fb connect (first time)
#define kOnCurrentUserFbSwitch @"onCurrentUserFbSwitch" // user info update w new fbid, but different than current
#define kOnCurrentUserFbDisconnect @"onCurrentUserFbDisconnect" // user info "disconnects" with FB (not implemented)
#define kOnCapturedRoad @"onCapturedRoad"
#define kOnIsRoadOwnedAccountUpdate @"onIsRoadOwnedAccountUpdate"
#define kOnIsRoadOwned @"onIsRoadOwned"
#define kOnUserAccountUpdate @"onUserAccountUpdate" // update user account
#define kOnRoadUpdate @"onRoadUpdate" 
#define kOnGoingToBackground @"onGoingToBackground"
#define kOnGoingToForeground @"onGoingToForeground"
#define kOnInternetNotReachable @"onInternetNotReachable"
#define kOnInternetReachable @"onInternetReachable"
#define kOnReportSpeedLimit @"onReportSpeedLimit"
#define kOnWinAttack @"onWinAttack"
#define kOnLoseAttackMe @"onLoseAttackMe" // keep track of me losing an attack in game
#define kOnLoseAttack @"onLoseAttack" // lose a road from being attacked
#define kOnTollsIn @"OnTollsIn"
#define kOnTollsOut @"OnTollsOut"
#define kOnInitializeRoadWarsServer @"OnInitializeRoadWarsServer"
#define kOnIssueAttack @"OnIssueAttack"
#define kOnDoneMotionStart @"OnDoneMotionStart"
#define kOnDoneMotionStop @"OnDoneMotionStop"
#define kOnDoneQuickHelp @"onDoneQuickHelp"
#define kOnStopBonusTimer @"onStopBonusTimer"
#define kOnGetSkinny @"onGetSkinny"


// storyboards
#define kStoryboardMain @"MainStoryboard"

// segues
#define kSegueHitlistToProfile @"segueHitlistToProfile"
#define kSegueHitlistToActivity @"segueHitlistToActivity"
#define kSegueDrivingToProfileMe @"segueDrivingToProfileMe"
#define kSegueNavToAbout @"segueNavToAbout"
#define kSegueNavToOptions @"segueNavToOptions"
#define kSegueNavToDriveSimulation @"segueNavToDriveSimulation"
#define kSegueAboutToFeedback @"segueNavToFeedback"
#define kSegueOptionsToSoundBoard @"segueOptionsToSoundBoard"
#define kSegueNavToAttack @"segueNavToAttack"
#define kSegueAttackToSettings @"segueAttackToSettings"
#define kSegueAttackToAttackPlay @"segueAttackToAttackPlay"
#define kSegueAttackToShowBonus @"segueAttackToShowBonus"
#define kSegueReportSpeedLimit @"segueReportSpeedLimit"
#define kSegueShowDriveScreenImages @"segueShowDriveScreenImages"
#define kSegueAllModelImages @"segueAllModelImages"
#define kSegueQuickHelpImages @"segueQuickHelpImages"
#define kSegueMyTurf @"segueMyTurf"
#define kSegueSkinny @"segueSkinny"
#define kSegueTurfMap @"segueTurfMap"
#define kSegueAllTurfMap @"segueAllTurfMap"

#define kSegueMoreToReviews @"segueMoreToReviews"
#define kSegueMoreToInvite @"segueMoreToInvite"
#define kSegueMoreToFeedback @"segueMoreToFeedback"
#define kSegueMoreToSoundOptions @"segueMoreToSoundOptions"
#define kSegueMoreToAbout @"segueMoreToAbout"
#define kSegueMoreToRateApp @"segueMoreToRateApp"


// view controller identifiers
#define kViewControllerAbout @"viewControllerAbout"
#define kViewControllerDriveSimulator @"viewControllerDriveSimulator"
#define kViewControllerDriving @"viewControllerDriving"
#define kViewControllerOptions @"viewControllerOptions"
#define kViewControllerFeedback @"viewControllerFeedback"
#define kViewControllerAttack @"viewControllerAttack"
#define kViewControllerProfileMe @"viewControllerProfileMe"


// server constants
//#define kServerHost @"www.roadwarsgame.com"
#define kServerHost @"api.roadwars.com"

#define kServerSecret @"puffTheMagicIguana"
#define kServerDeviceType @"ios"
#define kServerApiPath @"/api"
#define kserverSessionDuration 1800 // 30 minutes

// bonus amounts
#define kBonusPerMinuteRound1 5
#define kBonusEndRound1 50
#define kBonusPerMinuteRound2 10
#define kBonusEndRound2 100
#define kBonusPerMinuteRound3 15
#define kBonusEndRound3 200
#define kBonusPerMinuteRound4 25
#define kBonusEndRound4 400
#define kBonusPerMinuteRound5 35
#define kBonusEndRound5 800
#define kBonusPerMinuteRound6 50
#define kBonusEndRound6 2000

//  meters/second returned from location services to mph
#define KSpeedConverter 2.2369362920544 

// MapQuest URL for getting speedlimit
#define kMapQuestHost @"open.mapquestapi.com/guidance/v1/route"
#define kMapQuestPath @"http://open.mapquestapi.com/guidance/v1/route?key=Fmjtd%7Cluubn16bn5%2Caa%3Do5-90a59r"

// constant for distance between each tic in meter (3 of them), which will be +-10 mph tic marks
#define kMphRange 30

// location heading buffer size, this might change to server param
#define headingBufferSize 6 //BR:server parm

//  debugging log toggle switch between console/logfile (default)
#define DEBUGGING_PREF_KEY @"enabledDebuggingToLog"

//  models On/OFF
#define MODELS_PREF_KEY @"enabledModels"

// accelerometer activity
typedef enum {
    kAccelerometerActivityNone = 0,
	kAccelerometerActivityRenorm,
	kAccelerometerActivityAccelerateGreen,
	kAccelerometerActivityAccelerateYellow,
	kAccelerometerActivityAccelerateRed,
    kAccelerometerActivityAccelerateBusted,
	kAccelerometerActivityBrakeGreen,
	kAccelerometerActivityBrakeYellow,
	kAccelerometerActivityBrakeRed,
    kAccelerometerActivityBrakeBusted,
	kAccelerometerActivityLeftTurnGreen,
	kAccelerometerActivityLeftTurnYellow,
	kAccelerometerActivityLeftTurnRed,
    kAccelerometerActivityLeftTurnBusted,
	kAccelerometerActivityRightTurnGreen,
	kAccelerometerActivityRightTurnYellow,
	kAccelerometerActivityRightTurnRed,
    kAccelerometerActivityRightTurnBusted
} AccelerometerActivity;


#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

#define k1to6DiceGame 1
#define k6to1DiceGame 2
#define kOddUpDiceGame 3
#define kEvenDownDiceGame 4
#define kUpCardGame 5
#define kDownCardGame 6
#define kRockPaperScissorGame 7
#define k1to6NumberGame 8
#define k6to1NumberGame 9
#define kRomanNumberGame 10
#define kChessGame 11
#define kAnimalUpGame 12
#define kAnimalDownGame 13
#define kWheelUpGame 14
#define kWheelDownGame 15

#define numGames 14

#define kDrivingState 0
#define kNotDrivingState 1
#define kInitializeDrivingState -1

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)




//
//  RWTellFriendsViewController.m
//  roadwars
//
//  Created by Bob Rainis.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWTellFriendsViewController.h"
#import "RWServices.h"
#import "RWFacebookService.h"
#import "RWAPI.h"
#import <FacebookSDK/FBSession.h>
#import <FacebookSDK/FBWebDialogs.h>
#import "Constants.h"
#import "NSObject+ObserverShortcuts.h"
#import "RWTableViewCell.h"

@interface RWTellFriendsViewController ()

@end

@implementation RWTellFriendsViewController
@synthesize connectButton=_connectButton;
@synthesize friendsArray=_friendsArray;
@synthesize allTableData=_allTableData;
@synthesize filteredTableData=_filteredTableData;
@synthesize letters=_letters;
@synthesize searchBar=_searchBar;
@synthesize isFiltered=_isFiltered;

- (id)initWithCoder:(NSCoder *)decoder {
    if ( self = [super initWithCoder:decoder] ) {
        if (self.friendsArray==nil)
            self.friendsArray = [[NSMutableArray alloc] init];
        [[RWServices facebookService] openSession];
        
    }
    return self;
}



- (void)viewDidLoad{
    [super viewDidLoad];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    [self addObserver:kOnGoingToForeground withSelector:@selector(onGoingToForeground:)];
    [self addObserver:kOnFacebookFriendsRequestInviteComplete withSelector:@selector(onFacebookFriendsInvite:)];
    self.allTableData = [[NSMutableArray alloc] init];
    [self.allTableData addObject:@"A"];
    [self.allTableData addObject:@"B"];
    [self.allTableData addObject:@"C"];
    [self.allTableData addObject:@"D"];
    [self.allTableData addObject:@"E"];
    [self.allTableData addObject:@"F"];
    [self.allTableData addObject:@"G"];
    [self.allTableData addObject:@"H"];
    [self.allTableData addObject:@"I"];
    [self.allTableData addObject:@"J"];
    [self.allTableData addObject:@"K"];
    [self.allTableData addObject:@"L"];
    [self.allTableData addObject:@"M"];
    [self.allTableData addObject:@"N"];
    [self.allTableData addObject:@"O"];
    [self.allTableData addObject:@"P"];
    [self.allTableData addObject:@"Q"];
    [self.allTableData addObject:@"R"];
    [self.allTableData addObject:@"S"];
    [self.allTableData addObject:@"T"];
    [self.allTableData addObject:@"U"];
    [self.allTableData addObject:@"V"];
    [self.allTableData addObject:@"W"];
    [self.allTableData addObject:@"X"];
    [self.allTableData addObject:@"Y"];
    [self.allTableData addObject:@"Z"];
    
    [[RWServices facebookService] getFriends:YES];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self.searchBar.subviews objectAtIndex:0] setAlpha:0.0];
    [[UISearchBar appearance]setSearchFieldBackgroundImage:[UIImage imageNamed:@"row44single.png"] forState:UIControlStateNormal];
   
    }

- (void)dealloc {
    [self removeObserver:kOnFacebookFriendsRequestInviteComplete];
    [self removeObserver:kOnGoingToForeground];
    self.friendsArray=nil;
    self.tableView.delegate=nil;
 }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 24;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
   // if(searching)
    //    return nil;
    
       return self.allTableData;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // Each letter is a section title
    NSString* key = [self.letters objectAtIndex:section];
    return key;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of letters in our letter array. Each letter represents a section.
    return self.letters.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Returns the number of items in the array associated with the letter for this section.
    NSString* letter = [self.letters objectAtIndex:section];
    NSArray* arrayForLetter = (NSArray*)[self.filteredTableData objectForKey:letter];
    return arrayForLetter.count;
}



- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
   // if(searching)
    //    return -1;
    
    return index;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RWTableViewCell *cell = nil;
    
    cell = [self dequeueOrNew:kTableCellFriends];
    [cell setBackgroundImageForRow:indexPath.row ofTotalRows:self.friendsArray.count];
    // Grab the object for the specified row and section
    NSString* letter = [self.letters objectAtIndex:indexPath.section];
    NSArray* arrayForLetter = (NSArray*)[self.filteredTableData objectForKey:letter];
    NSArray* friend = (NSArray*)[arrayForLetter objectAtIndex:indexPath.row];
    
   // NSLog(@"friend again <%@>", friend);
    
    cell.titleLabel.text =  [friend objectAtIndex:0];
    cell.leftProfileView.profileID = [ friend objectAtIndex:1];
    
    // [self configureCell:cell withObject:[self.friendsArray objectAtIndex:indexPath.row]];
    
    if ( cell == nil ) NSLog(@"RWTellFriendsViewController::cellForRow... - returning a null cell :(");
    return cell;
}

- (void)configureCell:(RWTableViewCell *)cell withObject:(NSObject *)obj {
    NSDictionary *friend = (NSDictionary *)obj;
   // NSLog (@"friend is <%@>", friend);
       
    cell.leftProfileView.profileID = [friend objectForKey:@"id"];
    cell.titleLabel.text = [friend objectForKey:@"name"];
   }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RWTableViewCell *cell = (RWTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    //NSLog (@"cell.leftProfileView.profileID %@", cell.leftProfileView.profileID);
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     // 2. Optionally provide a 'to' param to direct the request at
                                     cell.leftProfileView.profileID, @"to", 
                                     nil];
    NSDictionary *serverParameters = [[NSUserDefaults standardUserDefaults] objectForKey:@"serverParameters"];
    NSString *message = [serverParameters objectForKey:@"facebookInviteMessage"];
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:message
                                                    title:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                     // NSLog (@"resultUrl <%@>", resultURL);
                                                      if (error) {
                                                          // Case A: Error launching the dialog or sending request.
                                                          NSLog(@"Error sending request.");
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // Case B: User clicked the "x" icon
                                                              //NSLog(@"User cancelled request, but hitting X.");
                                                          } else {
                                                              if (![resultURL query])
                                                              {
                                                                   //NSLog(@"User cancelled request, but hitting CANCEL.");
                                                                  return;
                                                              }
                                                              //NSLog(@"Request Sent.");
                                                              // check version
                                                              [RWAPI facebookInvite:[NSNumber numberWithLongLong:cell.leftProfileView.profileID.longLongValue] completion: ^(NSDictionary * faceBookInviteResult) {
                                                                  NSLog (@"results from facebookInvite <%@>",faceBookInviteResult);
                                                                  UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"RoadWars Invite"
                                                                                                                  message:[faceBookInviteResult objectForKey:@"message"]
                                                                                                                 delegate:self
                                                                                                        cancelButtonTitle:@"OK"
                                                                                                        otherButtonTitles:nil];
                                                                  [alert show];
                                                                  
                                                                  
                                                              } failure:^(NSError *error) {
                                                                  // sigh... what to do?
                                                              }];
                                                              
                                                          }
                                                      }}];
    
}

- (IBAction)doDisconnectFacebook {
    // NSLog(@"%@:doDisconnectFacebook - clicked", self.class);
    if (FBSession.activeSession.isOpen){
       // NSLog (@"FBSessionStateOpen going to close");
        [self.connectButton setTitle:@"Connect to Facebook" forState:UIControlStateNormal];
        [FBSession.activeSession closeAndClearTokenInformation];
        // self.friendsArray=nil;
        self.filteredTableData=nil;
        [self.searchBar resignFirstResponder];
        [self.tableView reloadData];
    }
    else{
       // NSLog (@"FBSessionStateClosed going to open");
        [self.connectButton setTitle:@"Disconnect from Facebook" forState:UIControlStateNormal];
        [[RWServices facebookService] openSession];
        [self updateTableData:nil];
        
    }
}
- (void)onFacebookFriendsInvite:(NSNotification *)notif {
    if ( notif != nil && notif.object != nil ) {
        NSMutableArray *friends = (NSMutableArray *)notif.object;
        //NSLog (@"Invite Friends <%@>", friends);
        self.friendsArray=friends;
        
        // sort friend's list by name
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                       ascending:YES] ;
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
        
        // new and shiny sorted array
        self.friendsArray = (NSMutableArray*)[friends sortedArrayUsingDescriptors:sortDescriptors];
        [self updateTableData:@""];
    }
}
- (void)onGoingToForeground:(NSNotification *)notif {
    //[[RWServices facebookService] getFriends:YES];
    [self performSelector:@selector(updateFacebook) withObject:nil afterDelay:2];
    
}
- (void)updateFacebook{
    
    if (FBSession.activeSession.isOpen){
        [self updateTableData:nil];
        [self.searchBar becomeFirstResponder];
    }
    
}
-(void)updateTableData:(NSString*)searchString
{
    self.filteredTableData = [[NSMutableDictionary alloc] init];
    
    for (NSDictionary* friend in self.friendsArray)
    {
       // NSLog(@"name is <%@>",friend);
        
        bool isMatch = false;
        if(searchString.length == 0)
        {
            // If our search string is empty, everything is a match
            isMatch = true;
        }
        else
        {
            // If we have a search string, check to see if it matches the food's name or description
            NSRange nameRange = [[friend objectForKey:@"name"] rangeOfString:searchString options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound )
                isMatch = true;
        }
        
        // If we have a match...
        if(isMatch)
        {
            // Find the first letter of the food's name. This will be its gropu
            NSString* firstLetter = [[friend objectForKey:@"name"] substringToIndex:1];
            //NSLog (@"first letter is <%@>", firstLetter);
            // Check to see if we already have an array for this group
            NSMutableArray* arrayForLetter = (NSMutableArray*)[self.filteredTableData objectForKey:firstLetter];
            if(arrayForLetter == nil)
            {
                // If we don't, create one, and add it to our dictionary
                arrayForLetter = [[NSMutableArray alloc] init];
                [self.filteredTableData setValue:arrayForLetter forKey:firstLetter];
            }
            
            // Finally, add the food to this group's array
            NSMutableArray *m = [[NSMutableArray alloc]initWithObjects:[friend objectForKey:@"name"],[friend objectForKey:@"id"], nil];
            
            [arrayForLetter addObject:m];
           ;
        }
    }
    
   //NSLog(@"self.filteredTableData <%@>", self.filteredTableData);
    // Make a copy of our dictionary's keys, and sort them
    self.letters = [[self.filteredTableData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    //NSLog(@"self.letters <%@>", self.letters);
    
    // Finally, refresh the table
    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    //NSLog(@"search bar working");
    if (FBSession.activeSession.isOpen)
        [self updateTableData:text];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //NSLog(@"search canceled");
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    
    if (FBSession.activeSession.isOpen)
        [self updateTableData:nil];
}

@end

//
//  RotMat.h
//  CarCharter
//
//  Created by lanthe on 4/4/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef struct _Vector
{
	double x;
	double y;
	double z;
} Vector;


@interface RotMat : NSObject {

	double thetax;
	double thetay;
}

- (id) initWithVec:(Vector)vec;
- (Vector) operateOnVec:(Vector)vec;


@end

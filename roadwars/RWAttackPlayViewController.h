//
//  RWAttackPlayViewController.h
//  roadwars
//
//  Created by Bob Rainis 1/11/13.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWViewController.h"

@class User;
@class FBProfilePictureView;
@class DBFBProfilePictureView;

@interface RWAttackPlayViewController : RWViewController <UITextViewDelegate, UIAlertViewDelegate>
@property (nonatomic, weak) IBOutlet DBFBProfilePictureView *profilePictureMe;
@property (nonatomic, weak) IBOutlet UILabel *meName;
@property (nonatomic, weak) IBOutlet UILabel *meRoadName;
@property (nonatomic, strong) NSNumber *meRoadExtId;
@property (nonatomic, weak) IBOutlet UILabel *meTownStateName;
@property (nonatomic, weak) IBOutlet DBFBProfilePictureView *profilePictureAttackee;
@property (nonatomic, weak) IBOutlet UILabel *attackeeName;
@property (nonatomic, weak) IBOutlet UILabel *attackeeRoadName;
@property (nonatomic, strong) NSNumber *attackeeRoadExtId;
@property (nonatomic, strong) NSNumber *defendingRoadOwnerAccount;
@property (nonatomic, strong) NSNumber *attackingRoadOwnerAccount;
@property (nonatomic, weak) IBOutlet UILabel *attackCost;
@property (nonatomic, weak) IBOutlet UIImageView *coinImageView;

@property (nonatomic, weak) IBOutlet UILabel *attackeeTownStateName;
@property (nonatomic, weak) IBOutlet UIImageView *dice1, *dice2;
@property (nonatomic, weak) IBOutlet UILabel *winnerLabelMe;
@property (nonatomic, weak) IBOutlet UILabel *winnerLabelFriend;
@property (nonatomic, weak) IBOutlet UIView *tapToStartView;
@property (nonatomic, weak) IBOutlet UIView *tapToStopBackgroundView;
@property (nonatomic, weak) IBOutlet UILabel *tapToStartLabel;
@property (nonatomic, weak) IBOutlet UILabel *versusLabel;
@property (nonatomic, weak) IBOutlet UIButton *diceButton;

@property (nonatomic, strong) NSTimer *diceTimer;
@property (nonatomic, strong) NSTimer *diceTimer2;


@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSNumber *dice1Value, *dice2Value;

@property(nonatomic, assign) BOOL bIsAnimating;
@property(nonatomic, assign) int diceNumber;
@property(nonatomic, assign) int totalAttacks;
@property(nonatomic, assign) int lastLevel;
@property(nonatomic, assign) int randomizedDiceAttack;
@property(nonatomic, assign) int outcome;
@property(nonatomic, assign) BOOL bCanAttackAgain;;

@property (nonatomic) unsigned roadId;
@property (nonatomic) int currentGame;
@property (nonatomic, strong) NSTimer *attackTimer;

- (IBAction)doSpinDice:(id)sender;
//-(void) formatNumbers: (User *)user :(User *)me;

@end

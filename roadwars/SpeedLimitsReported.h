//
//  SpeedLimitsReported.h
//  roadwars
//
//  Created by Admin on 11/20/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SpeedLimitsReported : NSManagedObject

@property (nonatomic, retain) NSNumber * currentDrivingSpeed;
@property (nonatomic, retain) NSNumber * currentMQSpeed;
@property (nonatomic, retain) NSNumber * currentReportedSpeed;
@property (nonatomic, retain) NSDate * dateStamp;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSString * road;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * town;
@property (nonatomic, retain) NSNumber * heading;
@property (nonatomic, retain) NSNumber * roadId;

@end

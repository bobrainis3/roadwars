//
//  RWMapAnnotations.h
//  roadwars
//
//  Created by Bob Rainis on 7/1/13.
//  Copyright (c) 2013 BLADE LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

//note conforming to <MKAnnotation> protocol
//this requires a coordinate, title, and subtitle.
//tag has been added in addition
@interface MapAnnotations : NSObject <MKAnnotation> {
    
	CLLocationCoordinate2D coordinate;
	NSUInteger tag;
	NSString *title;
	NSString *subtitle;
}

//coordinate property must be readonly
@property(nonatomic,readonly) CLLocationCoordinate2D coordinate;
@property(nonatomic) NSUInteger tag;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;

//programmer provided init function to create the annotation objects
-(id)initWithCoordinate:(CLLocationCoordinate2D)c withTag:(NSUInteger)t withTitle:(NSString *)tl withSubtitle:	(NSString *)s;
@end
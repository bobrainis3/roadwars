//
//  RWHitlist.m
//  roadwars
//
//  Created by Christian Allen on 11/17/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWHitlist.h"
#import "RWContext.h"
#import <CoreData/CoreData.h>

@implementation RWHitlist

- (id)init {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kModelUser];
    
    // BR: sort hitlist by number of roads.
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"roads" ascending:NO];
    NSSortDescriptor *sort2 = [[NSSortDescriptor alloc] initWithKey:@"coin" ascending:NO];
  	
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sort, sort2, nil]];
    [fetchRequest setFetchBatchSize:20];
    
   // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"fbId > 0"]; // whether you or others, fbId > 0...
   // [fetchRequest setPredicate:predicate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"coin > 0 || roads > 0"];
    [fetchRequest setPredicate:predicate];
    
    if ((self = [super initWithFetchRequest:fetchRequest managedObjectContext:[RWContext managedObjectContext] sectionNameKeyPath:nil cacheName:nil])) {

    }
    return self;
}

@end

//
//  RWGrungeLabel.m
//  RW
//
//  Created by Bob Rainis on 9/28/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWGrungeLabel.h"
#import "Constants.h"

@implementation RWGrungeLabel

- (void)awakeFromNib {  // BR: simply allows us to use the Grunge font in interface builder
    [super awakeFromNib];
    self.font = [UIFont fontWithName:kFontNameGrunge size:self.font.pointSize];
   
}

@end

//
//  RWTableViewCell.m
//  RW
//
//  Created by Christian Allen on 9/17/12.
//  Copyright (c) 2012 bladenet LLC All rights reserved.
//

#import "RWTableViewCell.h"
#import "RWBigButton.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation RWTableViewCell

@synthesize leftImageView = _leftImageView;
@synthesize titleLabel = _titleLabel;
@synthesize descriptionLabel = _descriptionLabel;
@synthesize rightImageView = _rightImageView;
@synthesize rightLabel = _rightLabel;
@synthesize rightButton = _rightButton;
@synthesize rightButtonImageView = _rightButtonImageView;
@synthesize rightDescriptionLabel = _rightDescriptionLabel;
@synthesize leftProfileView = _leftProfileView;

- (id)initWithNibName:(NSString *)nibNameOrNil
{
    if ( nibNameOrNil == nil ) nibNameOrNil = kTableCellTurf; // the default xib
    self = [[[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil] objectAtIndex:0];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.titleLabel.text = @"";
        self.descriptionLabel.text = @"";
        self.rightLabel.text = @"";
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44single.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44single.png"]];
        
        //hide the view when the download starts
        self.leftProfileView.startHandler = ^(DBFBProfilePictureView* view){
            view.layer.opacity = 0.0f;
        };
        //show the view when the download completes, or show the empty image
        self.leftProfileView.completionHandler = ^(DBFBProfilePictureView* view, NSError* error){
            if(error) {
                view.showEmptyImage = YES;
                //view.profileID = nil;
                //NSLog(@"Loading profile picture failed with error: %@", error);
            }
            [UIView animateWithDuration:0.1f animations:^{
                view.layer.opacity = 1.0f;
            }];
        };

    }
    return self;
}

- (void)setBackgroundImageForRow:(NSUInteger)row ofTotalRows:(NSUInteger)totalRows {
    //NSLog(@"%@:setBackgroundImageForRow - height is %f", self.class, self.frame.size.height);
    if ( self.frame.size.height == kTableCellHeightInfo ) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row175single.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row175single.png"]];
    } else if ( totalRows < 2 ) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44single.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44single.png"]];
    } else if ( row == 0 ) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44top.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44top.png"]];
    } else if ( row == (totalRows - 1) ) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row125bottom.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row125bottom.png"]];
    } else if ( row % 3 == 2 ) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44r1.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44r1.png"]];
    } else if ( row % 3 == 0 ) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44r2.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44r2.png"]];
    } else {
        self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44r3.png"]];
        self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"row44r3.png"]];
    }
}

@end

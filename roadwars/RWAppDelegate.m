//
//  RWAppDelegate.m
//  roadwars
//
//  Created by Christian Allen on 11/5/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWAppDelegate.h"
#import "RWContext.h"
#import "Constants.h"
#import "RWServices.h"
#import "RWBonusTimerService.h"
#import <FacebookSDK/FacebookSDK.h>
#import <CoreData/CoreData.h>

@implementation RWAppDelegate

- (void)customizeAppearance { // very annoyed that this has to be done here for app-wide appearance changes...
    // custom navigation bars
    UIImage *navBackground = [[UIImage imageNamed:@"top-blank-single.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 320, 44)];
    [[UINavigationBar appearance] setBackgroundImage:navBackground forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor],
      UITextAttributeTextColor,
      [UIFont fontWithName:kFontNameGrunge size:28.0],
      UITextAttributeFont,
      nil]
     ];
    // BR: needed to do this for the title NOT getting truncated (ATT... PRO... etc).
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        CGFloat verticalOffset = 5;
        [[UINavigationBar appearance] setTitleVerticalPositionAdjustment:verticalOffset forBarMetrics:UIBarMetricsDefault];
    }
    // background image for app
    //self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gravel.png"]];

    [FBProfilePictureView class];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self customizeAppearance];
    
       
    // don't let the screen go idle
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
   
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSNotificationCenter defaultCenter] postNotificationName:kOnGoingToBackground object:nil];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:kOnGoingToForeground object:nil];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark Facebook stuff

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBSession.activeSession handleOpenURL:url];
}


@end

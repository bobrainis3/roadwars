//
//  RWNavigationController.h
//  roadwars
//
//  Created by Christian Allen on 12/17/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWNavigationController : UINavigationController

@property (nonatomic, strong) UIActivityIndicatorView *activityView;

- (void)setNavigationItems:(UINavigationItem *)navigationItem forController:(UIViewController *)controller;
- (void)showActivity;
- (void)hideActivity;

@end

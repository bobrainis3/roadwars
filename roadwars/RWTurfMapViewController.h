//
//  RWTurfMapViewController.h
//  roadwars
//
//  Created by Bob Rainis
//

#import "RWViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface RWTurfMapViewController : RWViewController <MKMapViewDelegate>

@property (nonatomic, strong)  NSMutableArray *allTurf;

@property (strong, nonatomic) IBOutlet MKMapView* mapView;
@property (strong, nonatomic) NSMutableArray *turfAnnotations;

@end

//
//  RWSettingBoardViewController.m
//  roadwars
//
#import <MediaPlayer/MediaPlayer.h>
#import "RWSettingsViewController.h"
#import "RWTableViewCellSoundBoard.h"
#import "Constants.h"


@interface RWSettingsViewController ()

@property (nonatomic, strong) NSArray *settings;

@end

@implementation RWSettingsViewController
@synthesize settings=_settings;
@synthesize player = _player;

- (id)initWithCoder:(NSCoder *)decoder {
    if ( self = [super initWithCoder:decoder] ) {
        self.settings = [NSArray arrayWithObjects:@"Frequently Asked Questions",@"Rate Road Wars in App Store", @"Invite Friends to Play", @"Send Feedback", @"Sound Options", @"About Road Wars", @"Startup Animation",nil];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //NSLog(@"self.settings.count <%d>", self.settings.count);
    return self.settings.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"indexPath <%@>",indexPath);
    
    RWTableViewCell *cell = nil;
    
    cell = [self dequeueOrNew:kTableCellSettings];
    [cell setBackgroundImageForRow:indexPath.row ofTotalRows:self.settings.count];
    cell.titleLabel.text=[self.settings objectAtIndex:indexPath.row];
    
    if ( cell == nil ) NSLog(@"RWSettingsViewController::cellForRow... - returning a null cell :(");
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item==0)
        [self performSegueWithIdentifier:kSegueMoreToReviews sender:nil];
    else if (indexPath.item==1)
        [self doReviews];
        //[self performSegueWithIdentifier:kSegueMoreToRateApp sender:nil];
    else if (indexPath.item==2)
        [self performSegueWithIdentifier:kSegueMoreToInvite sender:nil];
    else if (indexPath.item==3)
        [self performSegueWithIdentifier:kSegueMoreToFeedback sender:nil];
    else if (indexPath.item==4)
        [self performSegueWithIdentifier:kSegueMoreToSoundOptions sender:nil];
    else if (indexPath.item==5)
        [self performSegueWithIdentifier:kSegueMoreToAbout sender:nil];
    else if (indexPath.item==6)
        [self doShowStartUp];
    
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    //change background color of selected cell
    RWTableViewCell *cell = (RWTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor blueColor];
    
}
- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // Reset Color.
    RWTableViewCell *cell = (RWTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
}
- (IBAction)doReviews{
    NSString *appURL = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@?type=Purple+Software",
                        @"590821094"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appURL]];
/*
    // Initialize Product View Controller
    SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
    // Configure View Controller
    [storeProductViewController setDelegate:self];
    [storeProductViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier : @"590821094"} completionBlock:^(BOOL result, NSError *error) {
        if (error) {
            NSLog(@"Error %@ with User Info %@.", error, [error userInfo]);
        } else {
            // Present Store Product View Controller
            CATransition *transition = [CATransition animation];
            transition.duration = 0.4;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            [self.view.window.layer addAnimation:transition forKey:nil];
            [self presentViewController:storeProductViewController animated:NO completion:nil];
        }
    }];
*/
    
}


- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)doShowStartUp{
    NSString *url = [[NSBundle mainBundle] pathForResource:@"roadwars_animation" ofType:@"mov"];
    self.player = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:url]];
    self.player.moviePlayer.controlStyle=MPMovieControlStyleNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hidecontrol)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:self.player.moviePlayer];
    //---play roadwars animation
    [self presentMoviePlayerViewControllerAnimated:self.player];
}

- (void) hidecontrol {
    [[NSNotificationCenter defaultCenter] removeObserver:self     name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:self.player.moviePlayer];
    [self.player.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
    
}


@end

//
//  RWTableViewCellSoundBoard.h
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWTableViewCell.h"

@interface RWTableViewCellSoundBoard : RWTableViewCell

@property (nonatomic, weak) IBOutlet UIButton *muteButton;
@property (nonatomic, weak) IBOutlet UIButton *aButton;
@property (nonatomic, weak) IBOutlet UIButton *bButton;
@property (nonatomic, weak) IBOutlet UIButton *cButton;

@end

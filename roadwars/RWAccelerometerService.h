//
//  RWAccelerometerService.h
//  roadwars
//
//  Created by Bob Rainis on 11/14/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//
#import <CoreMotion/CoreMotion.h>
#import "RotMat.h"

#define WINDOW_SIZE 100
#define	SMOOTH_WINDOW_SIZE	30
#define ACTIVITY_THRESHOLD  .0000013
#define kSmoothTimeout 60


@interface RWAccelerometerService : NSObject {
    // accelerometer related variables
    NSNumber* activityThreshold;
    int windowIndex;
	int sWindowIndex;
	UIAccelerationValue xEHistory[WINDOW_SIZE];
	UIAccelerationValue yEHistory[WINDOW_SIZE];
	UIAccelerationValue zEHistory[WINDOW_SIZE];
	double xSmooth[SMOOTH_WINDOW_SIZE];
	double ySmooth[SMOOTH_WINDOW_SIZE];
	double zSmooth[SMOOTH_WINDOW_SIZE];
	Vector meanV;
	Vector smoothV;
	Vector rotV;
	Vector gravV;
	double stdX;
	int timeout;
    NSNumber *accelerometerThreshold;
    int numberTimesThinkPhoneMoving;
    

}
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong)  NSOperationQueue *motionManagerQueue;
@property (nonatomic, assign)  Vector lastSmoothV;
- (void)startAccelerometerDetect;
- (void)stopAccelerometerDetect;
@end
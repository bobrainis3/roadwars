//
//  Road.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "Road.h"
#import "Country.h"
#import "StateProvince.h"
#import "Town.h"


@implementation Road

@dynamic town;
@dynamic stateProvince;
@dynamic country;

@end

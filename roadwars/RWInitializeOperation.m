//
//  RWInitializeOperation.m
//  RW
//
//  Created by Christian Allen on 9/25/12.
//  Copyright (c) 2012 bladenet LLC. All rights reserved.
//

#import "RWInitializeOperation.h"

@implementation RWInitializeOperation

#pragma mark NSOperation Overrides
- (void)main {
    @autoreleasepool {
        @try {
            NSDictionary *jsonResponse = [self refreshSession];
            if ( self.isCancelled ) return;
            if ( jsonResponse != nil && [jsonResponse isKindOfClass:[NSDictionary class]] ) {
                self.result = jsonResponse;
            }
        } @catch (NSException *exception) {
            NSLog(@"RWInitializeAPIOperation triggered exception: %@", exception);
            self.error = [NSError errorWithDomain:exception.name code:400 userInfo:exception.userInfo];
        }
    }
}

@end

//
//  Country.m
//  roadwars
//
//  Created by Christian Allen on 11/21/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "Country.h"
#import "Road.h"
#import "StateProvince.h"
#import "Town.h"


@implementation Country

@dynamic stateProvinces;
@dynamic towns;
@dynamic roads;

@end

//
//  RWDriveViewController.h
//  roadwars
//
//  Created by Bob Rainis on 11/8/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
#import "RWViewController.h"
#import "FESSolarCalculator.h"

@class FBProfilePictureView;
@class DBFBProfilePictureView;
@class User;
@class SpeedLimitsReported;

@interface RWDriveViewController : RWViewController <UIAlertViewDelegate, MFMailComposeViewControllerDelegate>

@property(strong,nonatomic) IBOutlet UILabel *speedLimitLabel;
@property(strong,nonatomic) IBOutlet UILabel *roadNameLabel;
@property(strong,nonatomic) IBOutlet UILabel *townStateNameLabel;
@property(strong,nonatomic) NSString *stateNameString, *stateNameHold;
@property(strong,nonatomic) NSString *townNameString, *townNameHold;
@property(strong,nonatomic) NSString *roadNameString, *roadNameHold ;
@property(strong,nonatomic) NSString *countryNameString ;

@property(strong,nonatomic) IBOutlet UILabel *stateNameLabel;
@property(strong,nonatomic) IBOutlet UILabel *drivingSpeedLabel;
@property(strong,nonatomic) IBOutlet UIView  *speedMeterBackground;
@property(strong,nonatomic) IBOutlet UIView  *speedLimitView;
@property(weak,nonatomic) IBOutlet UILabel *bonusTimeLabel;
@property(weak,nonatomic) IBOutlet UILabel *bonusTimeChillLabel;
@property(strong,nonatomic) IBOutlet UIView *gbarView;


@property (nonatomic,strong)  IBOutlet UIView *flagView;
@property (nonatomic,strong)  IBOutlet UIView *speedLimitSignView;


@property (nonatomic,strong)  IBOutlet UIImageView *ownerFlag;
@property (nonatomic) int lastBonusMinute;
@property (nonatomic) int lastBonusSecond;

@property (nonatomic) int todayRoadsCaptured;
@property (nonatomic) int todayRoadsLost;
@property (nonatomic) int todayRoadsCapturedAttack;
@property (nonatomic) int weeklyRoadsCapturedAttack;
@property (nonatomic) int todayRoadsLostAttack;
@property (nonatomic) int todayTotalAttack;
@property (nonatomic) int weeklyTotalAttack;
@property (nonatomic) int todayIncidents;
@property (nonatomic) int todayCoinEarned;
@property (nonatomic) int todayTollIn;
@property (nonatomic) int todayTollOut;
@property (nonatomic) int todayReportedSpeedLimits;
@property (nonatomic) int todayDrivingTime;
@property (nonatomic) int weeklyDrivingTime;
@property (nonatomic) int accelIncidentsThisTimePeriod;

@property (nonatomic) unsigned roadId;

@property(strong,nonatomic) IBOutlet UILabel  *todayRoadsCapturedLabel;
@property(strong,nonatomic) IBOutlet UILabel  *todayRoadsLostLabel;
@property(strong,nonatomic) IBOutlet UILabel  *todayRoadsCapturedAttackLabel;
@property(strong,nonatomic) IBOutlet UILabel  *todayRoadsLostAttackLabel;

@property(strong,nonatomic) IBOutlet UILabel  *todayIncidentsLabel;
@property(strong,nonatomic) IBOutlet UILabel  *todayCoinEarnedLabel;
@property(strong,nonatomic) IBOutlet UILabel  *todayTollInLabel;
@property(strong,nonatomic) IBOutlet UILabel  *todayTollOutLabel;


// TODO: BR temporary for debugging accelerometer
@property(strong,nonatomic) IBOutlet UIImageView  *accelImage;

@property (nonatomic, strong) IBOutlet UIView *hudView;
@property (nonatomic, strong) IBOutlet UIView *bonusHudView;
@property (nonatomic, strong) IBOutlet UIView *hudInsideView;
@property (strong,nonatomic) IBOutlet UILabel *hudBonusLabel;
@property (strong,nonatomic) IBOutlet UILabel *hudTopLabel;
@property (strong,nonatomic) IBOutlet UILabel *hudTopLabelDescription;
@property (strong,nonatomic) IBOutlet UILabel *hudBottomLabel;
@property (strong,nonatomic) IBOutlet UILabel *hudBottomLabelDescription;
@property (nonatomic,strong)  IBOutlet UIImageView *hudImage;
@property (nonatomic,strong)  IBOutlet UIImageView *hudImage2;
@property (nonatomic,strong)  IBOutlet UILabel *speedLimitInHud;

@property (nonatomic,strong)  IBOutlet UIView *notDrivingLabelView;
@property (nonatomic,strong)  IBOutlet UIView *notDrivingStatsView;
@property (nonatomic, strong) IBOutlet UILabel *coinLabel;
@property (nonatomic, strong) IBOutlet UILabel *turfLabel;
@property (nonatomic, strong) IBOutlet UILabel *footerTotalCoinLabel;
@property (nonatomic, strong) IBOutlet UILabel *footerEarnRateLabel;
@property (nonatomic, strong) IBOutlet UIView *speedMeterView;
@property (nonatomic, strong) IBOutlet UILabel *todayLabel;
@property (nonatomic, strong) IBOutlet UIImageView *speedMeter;
@property (nonatomic, strong) IBOutlet UIView *bonusClockView;
@property (nonatomic, strong) IBOutlet UIView *topTitleView;
@property (nonatomic, strong) IBOutlet UIView *footerView;
@property (nonatomic, strong) IBOutlet UIView *flagBackgroundView;
@property (nonatomic, strong) IBOutlet UIView *streetSignView;
@property (nonatomic, strong) IBOutlet UIView *streetSignBackgroundView;
@property (nonatomic, strong) IBOutlet UIView *roadOwnerView;
@property (nonatomic, strong) IBOutlet UILabel *roadOwnerLabel;
@property (nonatomic, weak) IBOutlet UIView *footerCoinView;
@property (nonatomic, weak) IBOutlet UILabel *footerCoinLabel;

@property (nonatomic, strong) NSNumber *numRoadChanges;
@property (nonatomic, strong) NSNumber *numSpeedLimitChanges;

@property (nonatomic, assign) CGRect originalFrameStreetSign;
// us states abbreviations
@property (nonatomic, strong) NSDictionary * usStateAbbreviations;

@property (nonatomic, strong) IBOutlet UILabel * coinAmountLabel;
@property (nonatomic, strong) NSNumber *roadOwnerFbId;

@property (nonatomic, weak) IBOutlet DBFBProfilePictureView *profilePicture;
@property (nonatomic, weak) IBOutlet UIView *gpsDebuggingView;
@property (nonatomic, assign) BOOL bCanGetBusted;
@property (nonatomic, assign) BOOL bCanGetPenalized;
@property (nonatomic, assign) BOOL bBustedViewBackgroundColor;
@property (nonatomic, assign) BOOL bBustedSpeedMeterColor;
@property (nonatomic, assign) BOOL bReportedSpeedLimit;
@property (nonatomic, assign) BOOL bReportedSpeedLimitWarning;
@property (nonatomic, assign) BOOL bGotBigBonus;
@property (nonatomic, assign) BOOL bGotDeductionGoingToBackground;
@property (nonatomic, assign) BOOL bAccelerometerEvent;
@property (nonatomic, assign) BOOL bAcceptedDrivingWarning;
@property (nonatomic, assign) BOOL bDayTime;

@property (nonatomic, strong) NSDate *lastTimeStamp;

@property (nonatomic, weak) IBOutlet UIView *quadriant1View;
@property (nonatomic, weak) IBOutlet UIView *quadriant2View;
@property (nonatomic, weak) IBOutlet UIView *quadriant3View;
@property (nonatomic, weak) IBOutlet UIView *quadriant4View;
@property (nonatomic, strong) NSNumber *currentEarnRate;
@property (nonatomic, strong) NSNumber *currentEarnRateInGreenZone;
@property (nonatomic, strong) NSTimer *accountUpdateTimer;
@property (nonatomic, strong) NSDate *lastRefresh;
@property (nonatomic, strong) User *roadOwner;
@property (nonatomic) double lastSpeedLimitDistanceDifference;
@property (nonatomic, strong) UIButton *lastAttackButton;
@property (nonatomic, strong) NSNumber *lastDrivingSpeed;
@property (nonatomic, strong) SpeedLimitsReported *currentSl;
@property (nonatomic, weak) IBOutlet DBFBProfilePictureView *profilePictureHud;
@property (nonatomic, strong) UIImage *driveSnapshot;
@property (nonatomic, assign) int drivingStateBeforeConnectivityLoss;
@property (nonatomic, strong) NSDictionary *serverParameters;
@property (nonatomic, strong) MPMoviePlayerViewController *player;
@property (nonatomic, strong) NSNumber *currentMqSpeedLimit;
@property (nonatomic, weak) IBOutlet UILabel *speedingMessageLabel;
@property (nonatomic, weak) IBOutlet UILabel *speedingCostLabel;
@property (nonatomic, weak) IBOutlet UILabel *speedingInsuranceCostLabel;
@property (nonatomic, weak) IBOutlet UIView *speedingCostView;

@property (nonatomic, weak) IBOutlet UILabel *mphLabel;
@property (nonatomic, weak) IBOutlet UILabel *perMinuteLabel;
@property (nonatomic, weak) IBOutlet UIView *backgroundDayView;
@property (nonatomic, weak) IBOutlet UIView *backgroundNightView;
@property (nonatomic, weak) IBOutlet UIView *backgroundCloudImageView;
@property (nonatomic, weak) IBOutlet UIView *backgroundStarsImageView;
@property (nonatomic, weak) IBOutlet UIView *topTitleGradientImageView;
@property (nonatomic, weak) IBOutlet UIView *dangerGradientView;
@property (nonatomic, weak) IBOutlet UIView *warningGradientView;


@property (nonatomic, weak) IBOutlet UIButton *moreButton;
@property (nonatomic, weak) IBOutlet UIButton *attackButton;



- (void)onLocationUpdate:(NSNotification *)notif;
- (void)onSpeedUpdate:(NSNotification *)notif;
- (void)onSpeedlimitUpdate:(NSNotification *)notif;
- (void)onAccelerometerIncident:(NSNotification *)notif;
- (void)onStopBonusTimer:(NSNotification *)notif;
- (void)onBonusTimerUpdate:(NSNotification *)notif;
- (void)onCapturedRoad:(NSNotification *)notif;
- (void)onIsRoadOwned:(NSNotification *)notif;
-(void) onInternetReachable:(NSNotification *)notif;
-(void) onInternetNotReachable:(NSNotification *)notif;

- (void)doBusted:(NSString*)topLabelText :(NSString*)bottomLabelText :(NSString*)bottomLabelDescriptionText;
- (void)doShowHud:(NSString*)topLabelText :(NSString*)bottomLabelText :(NSString*)bottomLabelDescriptionText :(UIImage*)image :(NSNumber*)fbId;
- (void)doShowSpeedingHud:(NSString *)topLabelText :(NSString*)bottomLabelText :(NSString*)bottomLabelDescriptionText :(UIImage*)image :(NSNumber*)fbId;
- (void)doShowBonusHud:(NSString *)bonusLabelText :(NSString *)topLabelText :(NSString*)bottomLabelText :(NSString*)topLabelDescriptionText :(NSString*)bottomLabelDescriptionText :(NSNumber*)fbId;
- (IBAction)doHideHud:(id)sender;
- (IBAction)doHideInitialHud:(id)sender;
- (IBAction)doExitApp:(id)sender;

- (IBAction)doTabBarSelect:(UIButton*)sender;



@end

//
//  RWFacebookService.m
//  roadwars
//
//  Created by Christian Allen on 11/17/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWFacebookService.h"
#import "CurrentUser.h"
#import "NSObject+ObserverShortcuts.h"
#import "Constants.h"
#import "RWContext.h"
#import <FacebookSDK/FBGraphUser.h>
#import <FacebookSDK/FBSession.h>
#import <FacebookSDK/FBRequest.h>

@implementation RWFacebookService

@synthesize facebook = _facebook;

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: 
           // NSLog(@"%@::sessionStateChanged - FBSessionStateOpen", self.class);
            
            //if ([[[RWContext me] fbId] longLongValue] == 0)
                [self getMeAndFriends:YES];
            //else
            //    [self getFriends:NO];
            
            break;
        case FBSessionStateClosed:
           // NSLog(@"%@::sessionStateChanged - FBSessionStateClosed", self.class);
           // [FBSession.activeSession closeAndClearTokenInformation];
            [FBSession.activeSession close];
            [self postNotification:kOnFacebookLogout withObject:nil];
            
            break;
        case FBSessionStateClosedLoginFailed:

           // NSLog(@"%@::sessionStateChanged - FBSessionStateClosedLoginFailed", self.class);
            [FBSession.activeSession closeAndClearTokenInformation];
            [self postNotification:kOnFacebookLoginFailure withObject:nil];
            
            break;
        default:
            break;
    }
    
    if (error) {
        NSLog(@"%@::sessionStateChanged - error: %@", self.class, error);
    }
}

- (void)openSession {
    NSArray *permissions = [NSArray arrayWithObjects:@"email", nil];

    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
     }];
}

/* BR - needs to be debugged
- (void)openSession {
    BOOL hasLoggedInBefore = ([[[RWContext me] fbId] intValue] > 0 ? YES : NO);
    NSLog(@"%@::openSession for fbId %@", self.class, [[RWContext me] fbId]);
    [FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:!hasLoggedInBefore completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         NSLog(@"%@::openSession - FB returned with state of %d", self.class, state);
         if ( state != FBSessionStateOpen && hasLoggedInBefore ) {
             NSLog(@"%@::openSession - FB returned without a good session, so trying again w UI", self.class);
             [FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler:
              ^(FBSession *session2,
                FBSessionState state2, NSError *error2) {
                  [self sessionStateChanged:session2 state:state2 error:error2];
              }];
         } else {
             NSLog(@"%@::openSession - FB returned with a good session, didn't even have to use the UI", self.class);
             [self sessionStateChanged:session state:state error:error];
         }
     }];
}
*/

- (void)getMeAndFriends:(BOOL)shouldGetFriendsToo { // calls getFriends in succession...
    FBRequest* meRequest = [FBRequest requestForMe];
    [self postNotification:kOnFacebookMeRequest withObject:nil];
    [meRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  id result,
                                                  NSError *error) {
        if ( error == nil ) {
            NSDictionary<FBGraphUser> *me = (NSDictionary<FBGraphUser> *)result;
            //NSLog (@"result <%@>", me);
            NSDictionary *userData = (NSDictionary *)result;
            NSString *gender = userData[@"gender"];
            NSString *emailAddress = userData[@"email"];
            //NSLog (@"gender <%@>, email <%@>", gender, emailAddress);
            [RWContext setCurrentUserFbId:me.id name:me.name first:me.first_name last:me.last_name gender:gender email:emailAddress];
            [self postNotification:kOnFacebookMeRequestComplete withObject:me];
            if ( shouldGetFriendsToo ) {
                [self getFriends:NO];
            }
        }
        else {
            NSLog (@"getMeAndFriends request error <%@>", error);
            [FBSession.activeSession closeAndClearTokenInformation];
        }

    }];
    
}

- (void)getFriends:(BOOL)forInvite {
    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
    [self postNotification:kOnFacebookFriendsRequest withObject:nil];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        if ( error == nil ) {
            NSArray* friends = [result objectForKey:@"data"];
        //    NSLog (@"friends <%@>", friends);
            //NSLog(@"%@:getFriends - got %d friends", self.class, friends.count);
          [self postNotification:(forInvite)?kOnFacebookFriendsRequestInviteComplete:kOnFacebookFriendsRequestComplete withObject:friends];
        }
        else {
            NSLog (@"getFriends request error <%@>", error);
            [FBSession.activeSession closeAndClearTokenInformation];
        }
    }];
}

@end

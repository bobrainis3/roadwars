//
//  RWNavigationController.m
//  roadwars
//
//  Created by Christian Allen on 12/17/12.
//  Copyright (c) 2013 Pemberton Software. All rights reserved.
//

#import "RWNavigationController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@interface RWNavigationController ()

@end

@implementation RWNavigationController

@synthesize activityView = _activityView;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)setNavigationItems:(UINavigationItem *)navigationItem forController:(UIViewController *)controller {

    // add our custom back button
    if ( controller != [[self viewControllers] objectAtIndex:0] ) { // if not the root view controller...
        UIView *leftButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 35, 30)];

        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        leftButton.frame = CGRectMake(0, 0, 30, 30);
       
        [leftButton addTarget:self action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
        [leftButtonView addSubview:leftButton];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
        [navigationItem setLeftBarButtonItem:backButton];
    }
    navigationItem.hidesBackButton = YES;
    
    //NSString *sysVer = [[UIDevice currentDevice] systemVersion];
    //NSLog(@"sysVer is <%@>", sysVer);

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {

        [[UITabBar appearance] setTintColor:kColorGold];
        //[[UITabBar appearance] setBarTintColor:kColorTabBar];
        [self preferredStatusBarStyle];
    }
}


- (void)showActivity {
    if ( self.activityView == nil ) {
        self.activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activityView.frame = CGRectMake(215, 32, 20, 20);
        self.activityView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.view addSubview:self.activityView];
    }
    [self.activityView startAnimating];
    self.activityView.hidden = NO;
}

- (void)hideActivity {
    if ( self.activityView ) {
        self.activityView.hidden = YES;
        [self.activityView stopAnimating];
    }
}

@end
